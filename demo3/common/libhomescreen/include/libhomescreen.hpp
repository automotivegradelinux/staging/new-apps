/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBHOMESCREEN_H
#define LIBHOMESCREEN_H
#include <vector>
#include <map>
#include <string>
#include <functional>
#include <json-c/json.h>
#include <systemd/sd-event.h>
extern "C"
{
#include <afb/afb-wsj1.h>
#include <afb/afb-ws-client.h>
}

class LibHomeScreen
{
public:
    LibHomeScreen();
    ~LibHomeScreen();

    LibHomeScreen(const LibHomeScreen &) = delete;
    LibHomeScreen &operator=(const LibHomeScreen &) = delete;

    using handler_func = std::function<void(json_object*)>;

    enum EventType {
	Event_ShowWindow = 1,
        Event_TapShortcut = 1,
        Event_OnScreenMessage,
        Event_OnScreenReply,
        Event_AllocateRestriction,
        Event_ReleaseRestriction
    };

    static const std::vector<std::string> api_list;
    static const std::vector<std::string> event_list;

    /* Method */
    int init(const int port, const std::string& token);

    int tapShortcut(const char* application_name);
    int onScreenMessage(const char* display_message);
    int onScreenReply(const char* reply_message);

    void set_event_handler(enum EventType et, handler_func f);

    void registerCallback(
        void (*event_cb)(const std::string& event, struct json_object* event_contents),
        void (*reply_cb)(struct json_object* reply_contents),
        void (*hangup_cb)(void) = nullptr);

    int call(const std::string& verb, struct json_object* arg);
    int call(const char* verb, struct json_object* arg);
    int subscribe(const std::string& event_name);
    int unsubscribe(const std::string& event_name);
    int allocateRestriction(const char* area);
    int releaseRestriction(const char* area);
    int showWindow(const char* id, json_object* json);

private:
    int initialize_websocket();

    void (*onEvent)(const std::string& event, struct json_object* event_contents);
    void (*onReply)(struct json_object* reply);
    void (*onHangup)(void);

    struct afb_wsj1* sp_websock;
    struct afb_wsj1_itf minterface;
    sd_event* mploop;
    std::string muri;

    int mport = 2000;
    std::string mtoken = "hs";

    std::map<EventType, handler_func> handlers;

public:
    /* Don't use/ Internal only */
    void on_hangup(void *closure, struct afb_wsj1 *wsj);
    void on_call(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg);
    void on_event(void *closure, const char *event, struct afb_wsj1_msg *msg);
    void on_reply(void *closure, struct afb_wsj1_msg *msg);
};

#endif /* LIBHOMESCREEN_H */

==========
 Overview
==========
<br>This is the templete that is useful for implementation of Qt application
<br>by using HomeScreen, WindowManager and SoundManager.


===========
 Structure
===========
<br> - main.cpp
<br>     The templete for implementation main function for Qt application.

<br> - wmhandler.cpp
<br> - wmhandler.h
<br>     These file is implemented "slot"
<br>     that is needed by Qt application that uses WindowManager.
<br>     These file should be modified according to Qt application.

<br> - smhandler.cpp
<br> - smhandler.h
<br>     These file is implemented "signal" and "slot"
<br>     that is needed by Qt application that uses SoundManager.
<br>     These file should be modified according to Qt application.


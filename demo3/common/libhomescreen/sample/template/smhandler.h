/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SIGNALER_H
#define SIGNALER_H

 #include <QObject>
 #include <QVariant>
 #include <QJsonDocument>
 #include <QtCore/QJsonObject>
 #include <libsoundmanager/libsoundmanager.hpp>
 #include <QString>
 #include <string>


class LibSMWrapper : public QObject
{
    Q_OBJECT
public: // method
    explicit LibSMWrapper(QObject *parent = nullptr);
    LibSMWrapper(const int port, const QString& token, QObject *parent = nullptr);
    ~LibSMWrapper();

    void wrapper_registerCallback(
        void (*event_func)(const std::string& event, struct json_object* event_contents), 
        void (*reply_func)(struct json_object* reply_contents)
    );
    void subscribe(const QString event_name);
    void unsubscribe(const QString event_name);
    void run_eventloop();

    void emit_event(const QString &event, const QJsonObject &msg);
    void emit_reply(const QJsonObject &msg);
public slots:
    int call(const QString &verb, const QString &arg);
    void print(const QString &str);
signals:
    void smEvent(const QVariant &event, const QVariant &msg);
    void smReply(const QVariant &msg);

private:
    LibSoundmanager* libsm;
};


#endif /*SIGNALER_H*/
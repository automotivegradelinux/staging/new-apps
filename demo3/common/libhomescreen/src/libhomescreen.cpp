/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdarg.h>
#include <sys/socket.h>
#include <iostream>
#include <algorithm>
#include <errno.h>
#include <cassert>
#include <cctype>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <libhomescreen.hpp>
#include "hmi-debug.h"

using namespace std;

static bool has_verb(const string& verb);
static const char API[] = "homescreen";

const std::vector<std::string> LibHomeScreen::api_list {
	std::string("ping"), // debug do not use
//	std::string("tap_shortcut"), // HomeScreen Application only
	std::string("on_screen_message"),
	std::string("on_screen_reply"),
	std::string("subscribe"),
	std::string("unsubscribe"),
	std::string("showWindow"),
	std::string("allocateRestriction"),
	std::string("releaseRestriction")
};

const std::vector<std::string> LibHomeScreen::event_list {
//	std::string("tap_shortcut"),
	std::string("showWindow"),
	std::string("on_screen_message"),
	std::string("on_screen_reply"),
	std::string("allocateRestriction"),
	std::string("releaseRestriction"),
	std::string("none")
};


/**
 * websocket
 */

static void _on_hangup_static(void *closure, struct afb_wsj1 *wsj)
{
	static_cast<LibHomeScreen*>(closure)->on_hangup(NULL,wsj);
}

static void _on_call_static(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg)
{
	/* LibHomeScreen is not called from other process */
}

static void _on_event_static(void* closure, const char* event, struct afb_wsj1_msg *msg)
{
	static_cast<LibHomeScreen*>(closure)->on_event(NULL,event,msg);
}

static void _on_reply_static(void *closure, struct afb_wsj1_msg *msg)
{
	static_cast<LibHomeScreen*>(closure)->on_reply(NULL,msg);
}


/**
 * constructor
 */
LibHomeScreen::LibHomeScreen()
{
}

/**
 * destructor
 */
LibHomeScreen::~LibHomeScreen()
{
	if(mploop)
	{
		sd_event_unref(mploop);
	}
	if(sp_websock != NULL)
	{
		afb_wsj1_unref(sp_websock);
	}
}

/**
 * This function is initializer
 *
 * #### Parameters
 * - port  [in] : This argument should be specified to the port number to be used for websocket
 * - token [in] : This argument should be specified to the token to be used for websocket
 *
 * #### Return
 * Nothing
 *
 * #### Note
 * Use this constructor
 *
 */
int LibHomeScreen::init(const int port, const string& token)
{
	int ret = 0;
	if(port > 0 && token.size() > 0)
	{
		mport = port;
		mtoken = token;
	}
	else
	{
		HMI_ERROR("libhomescreen","port and token should be > 0, Initial port and token uses.");
	}

	ret = initialize_websocket();
	if(ret != 0 )
	{
		HMI_ERROR("libhomescreen","Failed to initialize websocket");
	}
	else{
		HMI_DEBUG("libhomescreen","Initialized");
	}

	return ret;
}

/**
 * This function register callback function for reply/event message from home screen
 *
 * #### Parameters
 * - event_cb [in] : This argument should be specified to the callback for subscribed event
 * - reply_cb [in] : This argument should be specified to the reply callback for call function
 *
 * #### Return
 * Nothing
 *
 * #### Note
 * Event callback is invoked by home screen for event you subscribed.
 * If you would like to get event, please call subscribe function before/after this function
 */
void LibHomeScreen::registerCallback(
	void (*event_cb)(const std::string& event, struct json_object* event_contents),
	void (*reply_cb)(struct json_object* reply_contents),
	void (*hangup_cb)(void))
{
	onEvent = event_cb;
	onReply = reply_cb;
	onHangup = hangup_cb;
}

int LibHomeScreen::initialize_websocket()
{
	mploop = NULL;
	onEvent = nullptr;
	onReply = nullptr;
	int ret = sd_event_default(&mploop);
	if(ret < 0)
	{
		HMI_ERROR("libhomescreen","Failed to create event loop");
		goto END;
	}

	/* Initialize interface from websocket */
	minterface.on_hangup = _on_hangup_static;
	minterface.on_call = _on_call_static;
	minterface.on_event = _on_event_static;
	muri += "ws://localhost:" + to_string(mport) + "/api?token=" + mtoken; /*To be modified*/
	sp_websock = afb_ws_client_connect_wsj1(mploop, muri.c_str(), &minterface, this);
	if(sp_websock == NULL)
	{
		HMI_ERROR("libhomescreen","Failed to create websocket connection");
		goto END;
	}

	/* creates the evsrc */
	//ret = sd_event_add_io(mploop,&mevent_src, sp_websock->fd, EPOLLIN, event_callback, NULL);

	return 0;
END:
	if(mploop)
	{
		sd_event_unref(mploop);
	}
	return -1;
}

/**
 * Sending ShortCut Icon tapped event
 *
 * When HomeScreen shortcut area is tapped, sending a event
 *
 * #### Parameters
 * - application_name [in] : Tapped application name (label)
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 */
int LibHomeScreen::tapShortcut(const char* application_name)
{
	if(!sp_websock)
	{
		return -1;
	}

	struct json_object* obj = json_object_new_object();
	struct json_object* val = json_object_new_string("normal");
	json_object_object_add(obj, "area", val);
	
	return showWindow(application_name, obj);
}

/**
 * Sending onScreen message event
 *
 * Sending OnScreen message event to HomeScreen from applications
 *
 * #### Parameters
 * - display_message [in] : message for display
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 */
int LibHomeScreen::onScreenMessage(const char* display_message)
{
	if(!sp_websock)
	{
		return -1;
	}

	struct json_object* j_obj = json_object_new_object();
	struct json_object* val = json_object_new_string(display_message);
	json_object_object_add(j_obj, "display_message", val);
	return this->call("on_screen_message", j_obj);
}

/**
 * Sending onScreen reply event
 *
 * Sending OnScreen reply event to applications from HomeScreen
 *
 * #### Parameters
 * - reply_message [in] : message for reply
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 */
int LibHomeScreen::onScreenReply(const char* reply_message)
{
	if(!sp_websock)
	{
		return -1;
	}

	struct json_object* j_obj = json_object_new_object();
	struct json_object* val = json_object_new_string(reply_message);
	json_object_object_add(j_obj, "reply_message", val);
	return this->call("on_screen_reply", j_obj);
}

/**
 * Setting Event Handler
 *
 * Setting event handler for Homescreen
 *
 * #### Parameters
 * - et [in] : event name
 * - f [in] : event handler
 *
 * #### Return
 * Nothing
 *
 * #### Note
 * Don't release json_object by json_object_put in handler_func.
 * The resource is released by libafbwsc library.
 */
void LibHomeScreen::set_event_handler(enum EventType et, handler_func f)
{
	if (et >= 1 && et <= 5) {
		switch (et) {
/*			case Event_TapShortcut:
				this->subscribe(LibHomeScreen::event_list[0]);
				break;
*/
			case Event_ShowWindow:
				this->subscribe(LibHomeScreen::event_list[0]);
				break;
			case Event_OnScreenMessage:
				this->subscribe(LibHomeScreen::event_list[1]);
				break;
			case Event_OnScreenReply:
				this->subscribe(LibHomeScreen::event_list[2]);
				break;
			case Event_AllocateRestriction:
				this->subscribe(LibHomeScreen::event_list[3]);
				break;
			case Event_ReleaseRestriction:
				this->subscribe(LibHomeScreen::event_list[4]);
				break;
		}

		this->handlers[et] = std::move(f);
	}
}

/**
 * This function calls the API of HomeScreen via WebSocket
 *
 * #### Parameters
 * - verb [in] : This argument should be specified to the API name (e.g. "tap_shortcut")
 * - arg  [in] : This argument should be specified to the argument of API. And this argument expects JSON object
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 * #### Note
 * To call HomeScreen's APIs, the application should set its function name, arguments to JSON format.
 *
 */
int LibHomeScreen::call(const string& verb, struct json_object* arg)
{
	int ret;
	if(!sp_websock)
	{
		return -1;
	}
	if (!has_verb(verb))
	{
		HMI_ERROR("libhomescreen","verb doesn't exit");
		return -1;
	}
	ret = afb_wsj1_call_j(sp_websock, API, verb.c_str(), arg, _on_reply_static, this);
	if (ret < 0) {
		HMI_ERROR("libhomescreen","Failed to call verb:%s",verb.c_str());
	}
	return ret;
}

/**
 * This function calls the API of HomeScreen via WebSocket
 * This function is overload function of "call"
 *
 * #### Parameters
 * - verb [in] : This argument should be specified to the API name (e.g. "tap_shortcut")
 * - arg  [in] : This argument should be specified to the argument of API. And this argument expects JSON object
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 * #### Note
 * To call HomeScreen's APIs, the application should set its function name, arguments to JSON format.
 *
 */
int LibHomeScreen::call(const char* verb, struct json_object* arg)
{
	int ret;
	if(!sp_websock)
	{
		return -1;
	}
	if (!has_verb(string(verb)))
	{
		HMI_ERROR("libhomescreen","verb doesn't exit");
		return -1;
	}
	ret = afb_wsj1_call_j(sp_websock, API, verb, arg, _on_reply_static, this);
	if (ret < 0) {
		HMI_ERROR("libhomescreen","Failed to call verb:%s",verb);
	}
	return ret;
}

/**
 * Register callback function for each event
 *
 * #### Parameters
 * - event_name [in] : This argument should be specified to the event name
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 * #### Note
 * This function enables to get an event to your callback function.
 *
 */
int LibHomeScreen::subscribe(const string& event_name)
{
	if(!sp_websock)
	{
		return -1;
	}
	struct json_object* j_obj = json_object_new_object();
	json_object_object_add(j_obj, "event", json_object_new_string(event_name.c_str()));

	int ret = afb_wsj1_call_j(sp_websock, API, "subscribe", j_obj, _on_reply_static, this);
	if (ret < 0) {
		HMI_ERROR("libhomescreen","Failed to call verb");
	}
	return ret;
}

/**
 * Unregister callback function for each event
 *
 * #### Parameters
 * - event_name [in] : This argument should be specified to the event name
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 * #### Note
 * This function disables to get an event to your callback function.
 *
 */
int LibHomeScreen::unsubscribe(const string& event_name)
{
	if(!sp_websock)
	{
		return -1;
	}
	struct json_object* j_obj = json_object_new_object();
	json_object_object_add(j_obj, "event", json_object_new_string(event_name.c_str()));

	int ret = afb_wsj1_call_j(sp_websock, API, "unsubscribe", j_obj, _on_reply_static, this);
	if (ret < 0) {
		HMI_ERROR("libhomescreen","Failed to call verb");
	}
	return ret;
}

/**
 * Sending show window event
 * 
 * Call HomeScreen Service's showWindow verb to request display id's screen.
 *
 * #### Parameters
 * - id [in] : This argument should be specified to the application's id.
 * - json [in] : This argument should be specified to the json parameters.
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 */
int LibHomeScreen::showWindow(const char* application_name, json_object* json)
{
    if(!sp_websock)
	{
		return -1;
	}

	struct json_object* j_obj = json_object_new_object();
	struct json_object* val = json_object_new_string(application_name);
	json_object_object_add(j_obj, "application_name", val);
	// j_json = json;
	if (json == nullptr) {
		struct json_object* j_json = json_object_new_object();
		struct json_object* value = json_object_new_string("normal");
		json_object_object_add(j_json, "area", value);
		json_object_object_add(j_obj, "parameter", j_json);
	}
	else {
		json_object_object_add(j_obj, "parameter", json);
	}
	

	return this->call("showWindow", j_obj);
}

/**
 * Sending allocate restriction event 
 * 
 * Call HomeScreen Service's requestRestriction verb to request restriction screen.
 *
 * #### Parameters
 * - area [in] : restriction display area id.
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 */
int LibHomeScreen::allocateRestriction(const char* area)
{
    if(!sp_websock)
	{
		return -1;
	}

	struct json_object* j_obj = json_object_new_object();
	struct json_object* a_obj = json_object_new_object();
	
	struct json_object* val = json_object_new_string(area);
	
	json_object_object_add(j_obj, "area", val);	
	
	json_object_object_add(a_obj, "args", j_obj);
	
	return this->call("allocateRestriction", a_obj);
}

/**
 * Sending release restriction event 
 * 
 * Call HomeScreen Service's releaseRestriction verb to release restriction screen.
 *
 * #### Parameters
 * - area [in] : restriction display area id.
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 */
int LibHomeScreen::releaseRestriction(const char* area)
{
    if(!sp_websock)
	{
		return -1;
	}

	struct json_object* j_obj = json_object_new_object();	
	struct json_object* a_obj = json_object_new_object();
	
	struct json_object* val = json_object_new_string(area);
	
	json_object_object_add(j_obj, "area", val);
	
	json_object_object_add(a_obj, "args", j_obj);
	
	return this->call("releaseRestriction", a_obj);
}


/************* Callback Function *************/

void LibHomeScreen::on_hangup(void *closure, struct afb_wsj1 *wsj)
{
	HMI_DEBUG("libhomescreen","called");
	if(onHangup != nullptr)
	{
		onHangup();
	}
}

void LibHomeScreen::on_call(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg)
{
}

/*
* event is like "homescreen/tap_shortcut"
* msg is like {"event":"homescreen\/tap_shortcut","data":{"application_name":"hoge"},"jtype":"afb-event"}
* so you can get
	event name : struct json_object obj = json_object_object_get(msg,"event")
*/
void LibHomeScreen::on_event(void *closure, const char *event, struct afb_wsj1_msg *msg)
{
	HMI_DEBUG("libhomescreen","event: (%s) msg: (%s).", event, afb_wsj1_msg_object_s(msg));

	if (strstr(event, API) == NULL) {
		return;
	}

	struct json_object* ev_contents = afb_wsj1_msg_object_j(msg);
	struct json_object *json_data;
	if(!json_object_object_get_ex(ev_contents, "data", &json_data)) {
		HMI_ERROR("libhomescreen", "got ev_contents error.");
		return;
	}

	if(onEvent != nullptr)
	{
		const string ev(event);
		onEvent(ev, ev_contents);
	}

	const char* event_type = nullptr;
	struct json_object *json_event_type;
	if(json_object_object_get_ex(json_data, "type", &json_event_type)) {
		event_type = json_object_get_string(json_event_type);
	}
	else {
		HMI_WARNING("libhomescreen","event_type is null.");
		return;
	}

	if (strcasecmp(event_type, LibHomeScreen::event_list[0].c_str()) == 0) {
		auto i = this->handlers.find(Event_TapShortcut);
		if ( i != this->handlers.end() ) {
			i->second(json_data);
		}
	}
	else if (strcasecmp(event_type, LibHomeScreen::event_list[1].c_str()) == 0) {
		auto i = this->handlers.find(Event_OnScreenMessage);
		if ( i != this->handlers.end() ) {
			i->second(json_data);
		}
	}
	else if (strcasecmp(event_type, LibHomeScreen::event_list[2].c_str()) == 0) {
		auto i = this->handlers.find(Event_OnScreenReply);
		if ( i != this->handlers.end() ) {
			i->second(json_data);
		}
	}
}

/**
 * msg is like ({"response":{"verb":"subscribe","error":0},"jtype":"afb-reply","request":{"status":"success","info":"homescreen binder subscribe event name [on_screen_message]"}})
 * msg is like ({"response":{"verb":"tap_shortcut","error":0},"jtype":"afb-reply","request":{"status":"success","info":"afb_event_push event [tap_shortcut]"}})
 */
void LibHomeScreen::on_reply(void *closure, struct afb_wsj1_msg *msg)
{
	HMI_DEBUG("libhomescreen","msg: (%s)", afb_wsj1_msg_object_s(msg));
	if(onReply != nullptr)
	{
		struct json_object* reply = afb_wsj1_msg_object_j(msg);
		onReply(reply);
	}
}

static bool has_verb(const string& verb)
{
	HMI_DEBUG("libhomescreen","verb is %s", verb.c_str());
	if(find(LibHomeScreen::api_list.begin(), LibHomeScreen::api_list.end(), verb) != LibHomeScreen::api_list.end())
		return true;
	else
		return false;
}

/*
 * Copyright (c) 2018 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HOMESCREEN_CLIENT_H
#define HOMESCREEN_CLIENT_H

#include <string>
#include <unordered_map>
#include "hs-helper.h"


class HS_Client {
public:
    HS_Client(struct afb_req request, const char* id) : HS_Client(request, std::string(id)){}
    HS_Client(struct afb_req request, std::string id);
    HS_Client(HS_Client&) = delete;
    HS_Client &operator=(HS_Client&) = delete;
    ~HS_Client();

    int tap_shortcut(const char* appname);
    int showWindow(struct afb_req request, const char* appname);
    int on_screen_message (struct afb_req request, const char* message);
    int on_screen_reply (struct afb_req request, const char* message);
    int subscribe(struct afb_req request, const char* event);
    int unsubscribe(struct afb_req request, const char* event);
    int allocateRestriction(struct afb_req request, const char* area);
    int releaseRestriction(struct afb_req request, const char* area);

private:
    bool checkEvent(const char* event);

private:
    std::string my_id;
    struct afb_event my_event;
    std::unordered_map<std::string, int> event_list;

};

#endif // HOMESCREEN_CLIENT_H
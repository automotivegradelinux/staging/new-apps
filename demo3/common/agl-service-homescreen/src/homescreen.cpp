/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <memory>
#include <algorithm>
#include "hs-helper.h"
#include "hmi-debug.h"
#include "hs-clientmanager.h"

#define EVENT_SUBSCRIBE_ERROR_CODE 100

const char _error[] = "error";
const char _application_name[] = "application_name";
const char _display_message[] = "display_message";
const char _reply_message[] = "reply_message";
const char _args[] = "args";
const char _parameter[] = "parameter";

static HS_ClientManager* g_client_manager = HS_ClientManager::instance();

static std::unordered_map<std::string, HS_Client*> g_client_map;

/*
********** Method of HomeScreen Service (API) **********
*/

static void pingSample(struct afb_req request)
{
   static int pingcount = 0;
   afb_req_success_f(request, json_object_new_int(pingcount), "Ping count = %d", pingcount);
   HMI_NOTICE("homescreen-service","Verbosity macro at level notice invoked at ping invocation count = %d", pingcount);
   pingcount++;
}

/**
 * tap_shortcut notify for homescreen
 * When Shortcut area is tapped,  notify these applciations
 *
 * #### Parameters
 * Request key
 * - application_name   : application name
 *
 * #### Return
 * None
 *
 */
static void tap_shortcut (struct afb_req request)
{
    HMI_NOTICE("homescreen-service","called.");

    int ret = 0;
    const char* value = afb_req_value(request, _application_name);
    if (value) {
      HMI_NOTICE("homescreen-service","request params = %s.", value);
      // first step get appid from appname, next step change appname to appid
      std::string appid(value);
      std::transform(appid.begin(), appid.end(), appid.begin(), ::tolower);
      HS_Client* client = g_client_manager->find(appid);
      if(client != nullptr) {
        if(client->tap_shortcut(value) != 0) {
          afb_req_fail_f(request, "afb_event_push failed", "called %s.", __FUNCTION__);
          return;
        }
      }
      else {
          // app is not started, do nothing
      }
    } else {
      afb_req_fail_f(request, "failed", "called %s, Unknown palameter", __FUNCTION__);
      return;
    }

  // response to HomeScreen
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
      _error,  ret);
    afb_req_success(request, res, "afb_event_push event [tap_shortcut]");
}

/**
 * showWindow event
 *
 * #### Parameters
 *  - id  : app's id
 *  - parameter : parameter from app to new window
 *
 * #### Return
 * None
 *
 */
static void showWindow(struct afb_req request)
{
    HMI_NOTICE("homescreen-service","called.");

    int ret = 0;
    const char* value = afb_req_value(request, _application_name);
    if (value) {
      HMI_NOTICE("homescreen-service","request params = %s.", value);
      // first step get appid from appname, next step change appname to appid
      std::string appid(value);
      std::transform(appid.begin(), appid.end(), appid.begin(), ::tolower);
      HS_Client* client = g_client_manager->find(appid);
      if(client != nullptr) {
        if(client->showWindow(request, value) != 0) {
          afb_req_fail_f(request, "afb_event_push failed", "called %s.", __FUNCTION__);
          return;
        }
      }
      else {
          // app is not started, do nothing
      }
    } else {
      afb_req_fail_f(request, "failed", "called %s, Unknown palameter", __FUNCTION__);
      return;
    }

  // response to HomeScreen
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
      _error,  ret);
    afb_req_success(request, res, "afb_event_push event [showWindow]");
}

/**
 * HomeScreen OnScreen message
 *
 * #### Parameters
 * Request key
 * - display_message   : message for display
 *
 * #### Return
 * None
 *
 */
static void on_screen_message (struct afb_req request)
{
    HMI_NOTICE("homescreen-service","called.");

    int ret = 0;
    const char* value = afb_req_value(request, _display_message);
    if (value) {

      HMI_NOTICE("homescreen-service","request params = %s.", value);
      for(auto m : g_client_manager->getAllClient()) {
        if(m->on_screen_message(request, value) != 0) {
          afb_req_fail_f(request, "afb_event_push failed", "called %s.", __FUNCTION__);
          return;
        }
      }
    } else {
      afb_req_fail_f(request, "failed", "called %s, Unknown palameter", __FUNCTION__);
      return;
    }

  // response to HomeScreen
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
      _error,  ret);
    afb_req_success(request, res, "afb_event_push event [on_screen_message]");
}

/**
 * HomeScreen OnScreen Reply
 *
 * #### Parameters
 * Request key
 * - reply_message   : message for reply
 *
 * #### Return
 * None
 *
 */
static void on_screen_reply (struct afb_req request)
{
    HMI_NOTICE("homescreen-service","called.");

    int ret = 0;
    const char* value = afb_req_value(request, _reply_message);
    if (value) {

      HMI_NOTICE("homescreen-service","request params = %s.", value);
      for(auto m : g_client_manager->getAllClient()) {
        if(m->on_screen_reply(request, value) != 0) {
          afb_req_fail_f(request, "afb_event_push failed", "called %s.", __FUNCTION__);
          return;
        }
      }
    } else {
      afb_req_fail_f(request, "failed", "called %s, Unknown palameter", __FUNCTION__);
      return;
    }

  // response to HomeScreen
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
      _error,  ret);
    afb_req_success(request, res, "afb_event_push event [on_screen_reply]");
}

/**
 * Subscribe event
 *
 * #### Parameters
 *  - event  : Event name. Event list is written in libhomescreen.cpp
 *
 * #### Return
 * None
 *
 */
static void subscribe(struct afb_req request)
{
    const char *value = afb_req_value(request, "event");
    HMI_NOTICE("homescreen-service","value is %s", value);
    int ret = 0;
    if(value) {
        std::string appid(afb_req_get_application_id(request));
        std::transform(appid.begin(), appid.end(), appid.begin(), ::tolower);
        if(g_client_manager->getClient(request, appid)->subscribe(request, value) != 0) {
          afb_req_fail_f(request, "afb_req_subscribe failed", "called %s.", __FUNCTION__);
          return;
        }
    }
    else {
        HMI_NOTICE("homescreen-service","Please input event name");
        ret = EVENT_SUBSCRIBE_ERROR_CODE;
    }
    /*create response json object*/
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
        _error, ret);
    afb_req_success_f(request, res, "homescreen binder subscribe event name [%s]", value);
}

/**
 * Unsubscribe event
 *
 * #### Parameters
 *  - event  : Event name. Event list is written in libhomescreen.cpp
 *
 * #### Return
 * None
 *
 */
static void unsubscribe(struct afb_req request)
{
    const char *value = afb_req_value(request, "event");
    HMI_NOTICE("homescreen-service","value is %s", value);
    int ret = 0;
    if(value) {
        std::string appid(afb_req_get_application_id(request));
        std::transform(appid.begin(), appid.end(), appid.begin(), ::tolower);
        HS_Client* client = g_client_manager->find(appid);
        if(client != nullptr) {
            if(client->unsubscribe(request, value) != 0) {
                afb_req_fail_f(request, "afb_req_unsubscribe failed", "called %s.", __FUNCTION__);
                return;
            }
        }
        else {
            HMI_NOTICE("homescreen-service","not find app's client, unsubscribe failed");
            ret = EVENT_SUBSCRIBE_ERROR_CODE;
        }
    }
    else{
        HMI_NOTICE("homescreen-service","Please input event name");
        ret = EVENT_SUBSCRIBE_ERROR_CODE;
    }
    /*create response json object*/
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
        _error, ret);
    afb_req_success_f(request, res, "homescreen binder unsubscribe event name [%s]", value);
}

/**
 * allocateRestriction event
 *
 * #### Parameters
 *  - value  : the json contents to Restriction App.
 *             {"area":"area id"}
 *
 * #### Return
 * None
 *
 */
static void allocateRestriction(struct afb_req request)
{
    HMI_NOTICE("homescreen-service","called.");

    int ret = 0;
    const char* value = afb_req_value(request, _args);
    if (value) {
      HMI_NOTICE("homescreen-service","request args = %s.", value);
      HS_Client* client = g_client_manager->find(std::string("restriction"));
      if(client != nullptr) {
        if(client->allocateRestriction(request, value) != 0) {
          afb_req_fail_f(request, "afb_event_push failed", "called %s.", __FUNCTION__);
          return;
        }
      }
      else {
          // app is not started, do nothing
      }
    } else {
      afb_req_fail_f(request, "failed", "called %s, Unknown palameter", __FUNCTION__);
      return;
    }

    // response to Application
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
      _error,  ret);
    afb_req_success(request, res, "afb_event_push event [allocateRestriction]");
}

/**
 * releaseRestriction event
 *
 * #### Parameters
 *  - value  : the json contents to Restriction App.
 *             {"area":"area id"}
 *
 * #### Return
 * None
 *
 */
static void releaseRestriction(struct afb_req request)
{
    HMI_NOTICE("homescreen-service","called.");

    int ret = 0;
    const char* value = afb_req_value(request, _args);
    if (value) {
      HMI_NOTICE("homescreen-service","request args = %s.", value);
      HS_Client* client = g_client_manager->find(std::string("restriction"));
      if(client != nullptr) {
        if(client->releaseRestriction(request, value) != 0) {
          afb_req_fail_f(request, "afb_event_push failed", "called %s.", __FUNCTION__);
          return;
        }
      }
      else {
          // app is not started, do nothing
      }
    } else {
      afb_req_fail_f(request, "failed", "called %s, Unknown palameter", __FUNCTION__);
      return;
    }

    // response to Application
    struct json_object *res = json_object_new_object();
    hs_add_object_to_json_object_func(res, __FUNCTION__, 2,
      _error,  ret);
    afb_req_success(request, res, "afb_event_push event [releaseRestriction]");
}

/*
 * array of the verbs exported to afb-daemon
 */
static const struct afb_verb_v2 verbs[]= {
    /* VERB'S NAME                    FUNCTION TO CALL                  authorisation     some info         SESSION MANAGEMENT                                    */
    { .verb = "ping",                 .callback = pingSample,           .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE     },
    { .verb = "tap_shortcut",         .callback = tap_shortcut,         .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE     },
    { .verb = "showWindow",           .callback = showWindow,           .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE,    },
    { .verb = "on_screen_message",    .callback = on_screen_message,    .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE     },
    { .verb = "on_screen_reply",      .callback = on_screen_reply,      .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE     },
    { .verb = "subscribe",            .callback = subscribe,            .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE     },
    { .verb = "unsubscribe",          .callback = unsubscribe,          .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE     },
    { .verb = "allocateRestriction",  .callback = allocateRestriction,  .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE,    },
    { .verb = "releaseRestriction",   .callback = releaseRestriction,   .auth = NULL,     .info = NULL,     .session = AFB_SESSION_NONE,    },
    {NULL } /* marker for end of the array */
};

/**
 * homescreen binding preinit function
 *
 * #### Parameters
 *  - null
 *
 * #### Return
 * None
 *
 */
static int preinit()
{
   HMI_NOTICE("homescreen-service","binding preinit (was register)");
   return 0;
}

/**
 * homescreen binding init function
 *
 * #### Parameters
 *  - null
 *
 * #### Return
 * None
 *
 */
static int init()
{
    HMI_NOTICE("homescreen-service","binding init");

    g_client_manager->init();

    return 0;
}

/**
 * homescreen binding event function
 *
 * #### Parameters
 *  - event  : event name
 *  - object : event json object
 *
 * #### Return
 * None
 *
 */
static void onevent(const char *event, struct json_object *object)
{
   HMI_NOTICE("homescreen-service","on_event %s", event);
}

const struct afb_binding_v2 afbBindingV2 = {
    .api = "homescreen",
    .specification = NULL,
    .info = NULL,
    .verbs = verbs,
    .preinit = preinit,
    .init = init,
    .onevent = onevent
};

/*
 * Copyright (c) 2018 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hs-clientmanager.h"
#include "hmi-debug.h"

HS_ClientManager* HS_ClientManager::me = nullptr;

static void cbRemoveClientCtxt(void *data)
{
    HS_ClientManager::instance()->removeClientCtxt(data);
}

/**
 * HS_ClientManager construction function
 *
 * #### Parameters
 *  - Nothing
 *
 * #### Return
 * None
 *
 */
HS_ClientManager::HS_ClientManager()
{
}

/**
 * get instance
 *
 * #### Parameters
 *  - Nothing
 *
 * #### Return
 * HS_ClientManager instance pointer
 *
 */
HS_ClientManager* HS_ClientManager::instance(void)
{
    if(me == nullptr)
        me = new HS_ClientManager();

    return me;
}

/**
 * HS_ClientManager init function
 *
 * #### Parameters
 *  - Nothing
 *
 * #### Return
 * init result
 *
 */
int HS_ClientManager::init(void)
{
    HMI_NOTICE("homescreen-service","called.");
    // TODO : connect to windowmanger
    // get applist from appfw
}

/**
 * find HS_Client in client_list
 *
 * #### Parameters
 *  - appid: app's id
 *
 * #### Return
 * found HS_Client pointer
 *
 */
HS_Client* HS_ClientManager::find(std::string appid)
{
    std::lock_guard<std::mutex> lock(this->mtx);
    HS_Client* p = nullptr;
    auto ip = client_list.find(appid);
    if(ip != client_list.end()) {
        p = client_list[appid];
    }
    return p;
}

/**
 * get HS_Client
 *
 * #### Parameters
 *  - appid: app's id
 *
 * #### Return
 * found HS_Client pointer
 *
 */
HS_Client* HS_ClientManager::getClient(afb_req req, std::string appid)
{
    std::lock_guard<std::mutex> lock(this->mtx);
    HS_Client* p = nullptr;
    auto ip = client_list.find(appid);
    if(ip != client_list.end()) {
        p = client_list[appid];
    }
    else {
        appid2ctxt[appid] = createClientCtxt(req, appid);
        p = addClient(req, appid);
    }
    return p;
}

/**
 * get HS_Client pointers set
 *
 * #### Parameters
 *  - Nothing
 *
 * #### Return
 * HS_Client pointers set
 *
 */
std::vector<HS_Client*> HS_ClientManager::getAllClient(void)
{
    std::lock_guard<std::mutex> lock(this->mtx);
    std::vector<HS_Client*> v;
    for(auto a : client_list)
        v.push_back(a.second);
    return v;
}

/**
 * create client's afb_req_context
 *
 * #### Parameters
 *  - appid: app's id
 *
 * #### Return
 * HS_ClientCtxt pointer
 *
 */
HS_ClientCtxt* HS_ClientManager::createClientCtxt(afb_req req, std::string appid)
{
    HS_ClientCtxt *ctxt = (HS_ClientCtxt *)afb_req_context_get(req);
    if (!ctxt)
    {
        HMI_NOTICE("homescreen-service", "create new session for %s", appid.c_str());
        HS_ClientCtxt *ctxt = new HS_ClientCtxt(appid.c_str());
        afb_req_session_set_LOA(req, 1);
        afb_req_context_set(req, ctxt, cbRemoveClientCtxt);
    }
    return ctxt;
}

/**
 * add Client
 *
 * #### Parameters
 *  - ctxt: app's id
 *
 * #### Return
 * HS_Client pointer
 *
 */
HS_Client* HS_ClientManager::addClient(afb_req req, std::string appid)
{
    return (client_list[appid] = new HS_Client(req, appid));
}

/**
 * remove Client
 *
 * #### Parameters
 *  - appid: app's id
 *
 * #### Return
 * None
 *
 */
void HS_ClientManager::removeClient(std::string appid)
{
    delete client_list[appid];
    client_list.erase(appid);
}

/**
 * remove Client from list
 *
 * #### Parameters
 *  - data: HS_ClientCtxt pointer
 *
 * #### Return
 * None
 *
 */
void HS_ClientManager::removeClientCtxt(void *data)
{
    HS_ClientCtxt *ctxt = (HS_ClientCtxt *)data;
    if(ctxt == nullptr)
    {
        HMI_ERROR("homescreen-service", "data is nullptr");
        return;
    }

    HMI_NOTICE("homescreen-service", "remove app %s", ctxt->id.c_str());
    std::lock_guard<std::mutex> lock(this->mtx);
    removeClient(ctxt->id);
    delete appid2ctxt[ctxt->id];
    appid2ctxt.erase(ctxt->id);
}

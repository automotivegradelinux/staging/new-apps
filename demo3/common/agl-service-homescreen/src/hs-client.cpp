/*
 * Copyright (c) 2018 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hs-client.h"
#include "hs-helper.h"
#include "hmi-debug.h"

static const char _type[] = "type";

/**
 * HS_Client construction function
 *
 * #### Parameters
 *  - id: app's id
 *
 * #### Return
 * None
 *
 */
HS_Client::HS_Client(struct afb_req request, std::string id) : my_id(id)
{
    HMI_NOTICE("homescreen-service","called.");
    my_event = afb_daemon_make_event(id.c_str());
}

/**
 * HS_Client destruction function
 *
 * #### Parameters
 *  - null
 *
 * #### Return
 * None
 *
 */
HS_Client::~HS_Client()
{
    HMI_NOTICE("homescreen-service","called.");
    afb_event_unref(my_event);
}

/**
 * push tap_shortcut event
 *
 * #### Parameters
 *  - appname: app's name.
 *
 * #### Return
 * result
 *
 */
int HS_Client::tap_shortcut(const char* appname)
{
    if(!checkEvent(__FUNCTION__))
        return 0;

    HMI_NOTICE("homescreen-service","%s application_name = %s.", __FUNCTION__, appname);
    struct json_object* push_obj = json_object_new_object();
    hs_add_object_to_json_object_str( push_obj, 4, _application_name, appname,
    _type, __FUNCTION__);
    afb_event_push(my_event, push_obj);
    return 0;
}

/**
 * push showWindow event
 *
 * #### Parameters
 *  - appname: app's name.
 *
 * #### Return
 * result
 *
 */
int HS_Client::showWindow(struct afb_req request, const char* appname)
{
    if(!checkEvent(__FUNCTION__))
        return 0;

    HMI_NOTICE("homescreen-service","%s application_name = %s.", __FUNCTION__, appname);
    struct json_object* push_obj = json_object_new_object();
    hs_add_object_to_json_object_str( push_obj, 4, _application_name, appname,
    _type, __FUNCTION__);
    const char *param = afb_req_value(request, _parameter);
    json_object_object_add(push_obj, _parameter, json_tokener_parse(param));
    afb_event_push(my_event, push_obj);
    return 0;
}
/**
 * push on_screen_message event
 *
 * #### Parameters
 *  - message: post message.
 *
 * #### Return
 * result
 *
 */
int HS_Client::on_screen_message(struct afb_req request, const char* message)
{
    if(!checkEvent(__FUNCTION__))
        return 0;

    HMI_NOTICE("homescreen-service","push %s event message [%s].", __FUNCTION__, message);
    struct json_object* push_obj = json_object_new_object();
    hs_add_object_to_json_object_str( push_obj, 4, _display_message, message,
    _type, __FUNCTION__);
    afb_event_push(my_event, push_obj);
    return 0;
}

/**
 * push on_screen_reply event
 *
 * #### Parameters
 *  - message: reply message.
 *
 * #### Return
 * result
 *
 */
int HS_Client::on_screen_reply(struct afb_req request, const char* message)
{
    if(!checkEvent(__FUNCTION__))
        return 0;

    HMI_NOTICE("homescreen-service","push %s event message [%s].", __FUNCTION__, message);
    struct json_object* push_obj = json_object_new_object();
    hs_add_object_to_json_object_str( push_obj, 4, _reply_message, message,
    _type, __FUNCTION__);
    afb_event_push(my_event, push_obj);
    return 0;
}

/**
 * subscribe event
 *
 * #### Parameters
 *  - event: homescreen event, tap_shortcut etc.
 *
 * #### Return
 * result
 *
 */
int HS_Client::subscribe(struct afb_req request, const char* event)
{
    int ret = 0;
    auto ip = event_list.find(std::string(event));
    if(ip == event_list.end()) {
        event_list[std::string(event)] = 0;
        ret = afb_req_subscribe(request, my_event);
    }
    return ret;
}

/**
 * unsubscribe event
 *
 * #### Parameters
 *  - event: homescreen event, tap_shortcut etc.
 *
 * #### Return
 * result
 *
 */
int HS_Client::unsubscribe(struct afb_req request, const char* event)
{
    int ret = 0;
    event_list.erase(std::string(event));
    if(event_list.empty()) {
        ret = afb_req_unsubscribe(request, my_event);
    }
    return ret;
}

/**
 * allocate restriction
 *
 * #### Parameters
 *  - area: display area.
 *
 * #### Return
 * result
 *
 */
int HS_Client::allocateRestriction(struct afb_req request, const char* area)
{
    if(!checkEvent(__FUNCTION__))
        return 0;
    
    HMI_NOTICE("homescreen-service","%s area=%s.", __FUNCTION__, area);
    struct json_object* push_obj = json_object_new_object();
    hs_add_object_to_json_object_str( push_obj, 4, _application_name, "restriction",
    _type, __FUNCTION__);
    json_object_object_add(push_obj, _args, json_tokener_parse(area));
    afb_event_push(my_event, push_obj);
    return 0;
}

/**
 * release restriction
 *
 * #### Parameters
 *  - area: display area.
 *
 * #### Return
 * result
 *
 */
int HS_Client::releaseRestriction(struct afb_req request, const char* area)
{
    if(!checkEvent(__FUNCTION__))
        return 0;

    HMI_NOTICE("homescreen-service","%s area=%s.", __FUNCTION__, area);
    struct json_object* push_obj = json_object_new_object();
    hs_add_object_to_json_object_str( push_obj, 4, _application_name, "restriction",
    _type, __FUNCTION__);
    json_object_object_add(push_obj, _args, json_tokener_parse(area));
    afb_event_push(my_event, push_obj);
    return 0;
}

/**
 * check if client subscribe event
 *
 * #### Parameters
 *  - event: homescreen event, tap_shortcut etc.
 *
 * #### Return
 * true: found
 * false: not found
 *
 */
bool HS_Client::checkEvent(const char* event)
{
    auto ip = event_list.find(std::string(event));
    if(ip == event_list.end())
        return false;
    else
        return true;
}

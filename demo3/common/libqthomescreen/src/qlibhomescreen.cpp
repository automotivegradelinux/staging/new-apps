/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "qlibhomescreen.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "hmi-debug.h"
using namespace std;

#define _POPUPREPLY "on_screen_reply"
#define _REQ_POPUP_MESSAGE "on_screen_message"
#define _TAPSHORTCUT "tap_shortcut"
#define _KEY_DATA "data"
#define _KEY_APPLICATION_DATA "application_name"
#define _KEY_REPLY_MESSAGE "reply_message"
#define _KEY_REQUEST_MESSAGE "display_message"

static QLibHomeScreen* myThis;

// Note: qlibhomescreen will be integrated to libqtappfw
/**
 * QLibHomeScreen construction function
 *
 * #### Parameters
 * - parent [in] : object parent.
 *
 * #### Return
 * - None
 *
 */
QLibHomeScreen::QLibHomeScreen(QObject *parent) :
    QObject(parent),
    mp_hs(NULL)
{
    HMI_DEBUG("qlibhomescreen", "called.");
}

/**
 * QLibHomeScreen destruction function
 *
 * #### Parameters
 * - None
 *
 * #### Return
 * - None
 *
 */
QLibHomeScreen::~QLibHomeScreen()
{
    HMI_DEBUG("qlibhomescreen", "called.");
    if (mp_hs != NULL) {
        delete mp_hs;
    }
}

/**
 * init function
 *
 * call libhomescreen init function to connect to binder by websocket
 *
 * #### Parameters
 * - prot  : port from application
 * - token : token from application
 *
 * #### Return
 * - None
 *
 */
void QLibHomeScreen::init(int port, const QString &token)
{
    HMI_DEBUG("qlibhomescreen", "called.");
    string ctoken = token.toStdString();
    mp_hs = new LibHomeScreen();
    mp_hs->init(port, ctoken.c_str());

    myThis = this;
}


/**
 * call on screen message
 *
 * use libhomescreen api to call onscreen message
 *
 * #### Parameters
 * - message : message contents
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 */
int QLibHomeScreen::onScreenMessage(const QString &message)
{
    HMI_DEBUG("qlibhomescreen", "called.");
    string str = message.toStdString();
    return mp_hs->onScreenMessage(str.c_str());
}

/**
 * subscribe event
 *
 * use libhomescreen api to subscribe homescreen event
 *
 * #### Parameters
 * - evetNave : homescreen event name
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 */
int QLibHomeScreen::subscribe(const QString &evetName)
{
    HMI_DEBUG("qlibhomescreen", "called.");
    string str = evetName.toStdString();
    return mp_hs->subscribe(str);
}

/**
 * unsubscribe event
 *
 * use libhomescreen api to unsubscribe homescreen event
 *
 * #### Parameters
 * - evetNave : homescreen event name
 *
 * #### Return
 * - Returns 0 on success or -1 in case of error.
 *
 */
int QLibHomeScreen::unsubscribe(const QString &evetName)
{
    HMI_DEBUG("qlibhomescreen", "called.");
    string str = evetName.toStdString();
    return mp_hs->unsubscribe(str);
}

/**
 * set homescreen event handler function
 *
 * #### Parameters
 * - et : homescreen event name
 * - f  : event handler function
 *
 * #### Return
 * - None.
 *
 */
void QLibHomeScreen::set_event_handler(enum QEventType et, handler_fun f)
{
    HMI_DEBUG("qlibhomescreen", "called.");
    LibHomeScreen::EventType hs_et = (LibHomeScreen::EventType)et;
    return this->mp_hs->set_event_handler(hs_et, std::move(f));
}

/**
 * tapShortcut function
 *
 * #### Parameters
 * - application_name : tapped application name
  *
 * #### Return
 * - None.
 *
 */
void QLibHomeScreen::tapShortcut(QString application_name)
{
    HMI_DEBUG("qlibhomescreen","tapShortcut %s", application_name.toStdString().c_str());
    this->mp_hs->tapShortcut(application_name.toStdString().c_str());
}

/**
 * show application by application id
 *
 * #### Parameters
 * - id  : application id
 * - json  : json parameters
 *
 * #### Return
 * - None.
 *
 */
void QLibHomeScreen::showWindow(QString application_name, json_object* json)
{
    mp_hs->showWindow(application_name.toStdString().c_str(), json);
}

/**
 * show application by application id, json string
 *
 * #### Parameters
 * - id  : application id
 * - json  : json parameters
 *
 * #### Return
 * - None.
 *
 */
void QLibHomeScreen::showWindow(QString application_name, QString json)
{
    if(json.isNull())
        this->tapShortcut(application_name);
    else
        mp_hs->showWindow(application_name.toStdString().c_str(), json_tokener_parse(json.toStdString().c_str()));
}


/**
 * allocate restiction App
 * 
 * use libhomescreen api to call restriction app to display
 *
 * #### Parameters
 * - area : restriction display area
 *
 * #### Resturn
 * - None.
 *
 */
void QLibHomeScreen::allocateRestriction(QString area)
{
    mp_hs->allocateRestriction(area.toStdString().c_str());
}

/**
 * allocate restiction App
 * 
 * use libhomescreen api to call restriction app to hide
 *
 * #### Parameters
 * - area : restriction display area
 *
 * #### Resturn
 * - None.
 *
 */
void QLibHomeScreen::releaseRestriction(QString area)
{
    mp_hs->releaseRestriction(area.toStdString().c_str());
}

/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBWINDOWMANAGER_H
#define LIBWINDOWMANAGER_H

#include <functional>
#include <vector>
#include <string>
#include <json-c/json.h>

class Rect {
  public:
    Rect() : _x(0), _y(0),_w(0), _h(0) {}
    Rect(unsigned x, unsigned y, unsigned w, unsigned h)
        : _x(x), _y(y),_w(w), _h(h) {}
    ~Rect() = default;
    unsigned left()   const { return _x;}
    unsigned top()    const { return _y;}
    unsigned width()  const { return _w;}
    unsigned height() const { return _h;}
    void set_left  (unsigned int x) { _x = x; }
    void set_top   (unsigned int y) { _y = y; }
    void set_width (unsigned int w) { _w = w; }
    void set_height(unsigned int h) { _h = h; }
  private:
    unsigned _x;
    unsigned _y;
    unsigned _w;
    unsigned _h;
};

struct Screen
{
    unsigned long width_dp;
    unsigned long height_dp;
    unsigned long width_mm;
    unsigned long height_mm;
    double scale = 1.0;
};

class WMHandler {
  public:
    WMHandler();
    ~WMHandler() = default;

    using visible_handler   = std::function<void(const char*, bool visible)>;
    using active_handler    = std::function<void(const char*, bool active)>;
    using sync_draw_handler = std::function<void(const char*, const char*, Rect)>;
    using flush_draw_handler= std::function<void(const char*)>;
    using screen_updated_handler = std::function<void(const std::vector<std::string>&)>;
    using car_event_handler= std::function<void(const char*)>;

    visible_handler on_visible;
    active_handler on_active;
    sync_draw_handler on_sync_draw;
    flush_draw_handler on_flush_draw;
    screen_updated_handler on_screen_updated;
    car_event_handler on_headlamp_off;
    car_event_handler on_headlamp_on;
    car_event_handler on_parking_brake_off;
    car_event_handler on_parking_brake_on;
    car_event_handler on_lightstatus_brake_off;
    car_event_handler on_lightstatus_brake_on;
    car_event_handler on_car_stop;
    car_event_handler on_car_run;
};

class LibWindowmanager {
public:
    LibWindowmanager();
    ~LibWindowmanager();

    LibWindowmanager(const LibWindowmanager &) = delete;
    LibWindowmanager &operator=(const LibWindowmanager &) = delete;

    using handler_fun = std::function<void(json_object *)>;

    /* DrawingArea name (usage: {layout}.{area}) */
    const std::string kDefaultArea = "normal.full";
    const std::string kStrLayoutNormal = "normal";
    const std::string kStrLayoutSplit = "split";
    const std::string kStrAreaFull = "full";
    const std::string kStrAreaMain = "main";
    const std::string kStrAreaSub = "sub";

    /* Key for json obejct */
    const char *kKeyDrawingName = "drawing_name";
    const char *kKeyDrawingArea = "drawing_area";
    const char *kKeyDrawingRect = "drawing_rect";
    const char *kKeyIviId = "ivi_id";

    enum EventType {
       Event_Active = 0,
       Event_Inactive,

       Event_Visible,
       Event_Invisible,

       Event_SyncDraw,
       Event_FlushDraw,

       Event_ScreenUpdated,

       Event_HeadlampOff,
       Event_HeadlampOn,

       Event_ParkingBrakeOff,
       Event_ParkingBrakeOn,

       Event_LightstatusBrakeOff,
       Event_LightstatusBrakeOn,

       Event_CarStop,
       Event_CarRun,

       Event_Error,

       Event_Val_Max = Event_Error
    };

    int init(int port, char const *token);
    int init(int port, const std::string &token);

    // WM API
    int requestSurface(const char* role);
    int requestSurfaceXDG(const char* role, unsigned ivi_id);
    int activateWindow(const char* role, const char* area);
    int activateWindow(const char* role);
    int deactivateWindow(const char* role);
    int endDraw(const char* role);
    struct Screen getScreenInfo();
    int getAreaInfo(const char* role, Rect *out_rect);
    void setEventHandler(const WMHandler& wmh);

    // Backward Compatible API
    int requestSurface(json_object *object);
    int requestSurfaceXDG(json_object *object);
    int activateWindow(json_object *object);
    int deactivateWindow(json_object *object);
    int endDraw(json_object *object);
    int getDisplayInfo(json_object *object);
    int getAreaInfo(json_object *in_obj, json_object *out_obj);
    int getAreaInfo(const char *label, json_object *out_obj);
    void set_event_handler(enum EventType et, handler_fun f);

    // These APIs are deprecated, please use new API
    THIS_FUNCTION_IS_DEPRECATED(int activateSurface(json_object *object));
    THIS_FUNCTION_IS_DEPRECATED(int deactivateSurface(json_object *object));
    class Impl;

private:
    Impl *const d;
};
#endif // LIBWINDOWMANAGER_H

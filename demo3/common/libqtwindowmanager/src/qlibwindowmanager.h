/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef QLIBWINDOWMANAGER_H
#define QLIBWINDOWMANAGER_H
#include <libwindowmanager.h>
#include <functional>
#include <QObject>
#include <string>

class AGLScreenInfo : public QObject
{
    Q_OBJECT

  public:
    AGLScreenInfo(double scale = 1.0) { _scale_factor = scale; };

    Q_INVOKABLE double scale_factor() const { return _scale_factor; };

  private:
    double _scale_factor;
};

class AGLScreenInfoPrivate
{
  public:
    unsigned long width_dp(void) const { return _width_dp; };
    unsigned long height_dp(void) const { return _height_dp; };
    unsigned long width_mm(void) const { return _width_mm; };
    unsigned long height_mm(void) const { return _height_mm; };
    double scale_factor(void) const { return _scale; };

    void set_width_dp(unsigned long w) { _width_dp = w; };
    void set_height_dp(unsigned long h) { _height_dp = h; };
    void set_width_mm(unsigned long w) { _width_mm = w; };
    void set_height_mm(unsigned long h) { _height_mm = h; };
    void set_scale_factor(double scale) { _scale = scale; };

  private:
    unsigned long _width_dp;
    unsigned long _height_dp;
    unsigned long _width_mm;
    unsigned long _height_mm;
    double _scale = 1.0;
};

class QLibWindowmanager : public QObject{
Q_OBJECT
public:
    explicit QLibWindowmanager(QObject *parent = nullptr);
    ~QLibWindowmanager();

    QLibWindowmanager(const QLibWindowmanager &) = delete;
    QLibWindowmanager &operator=(const QLibWindowmanager &) = delete;

public:
    using handler_fun = std::function<void(json_object *object)>;

    enum QEventType {
       Event_Active = LibWindowmanager::Event_Active,
       Event_Inactive,

       Event_Visible,
       Event_Invisible,

       Event_SyncDraw,
       Event_FlushDraw,

       Event_ScreenUpdated,

       Event_HeadlampOff,
       Event_HeadlampOn,

       Event_ParkingBrakeOff,
       Event_ParkingBrakeOn,

       Event_LightstatusBrakeOff,
       Event_LightstatusBrakeOn,

       Event_CarStop,
       Event_CarRun,

       Event_Error,

       Event_Val_Max = Event_Error
    };

    int init(int port, const QString &token);

    // WM API
    Q_INVOKABLE int requestSurface(const QString &role);
    Q_INVOKABLE int activateWindow(const QString &role);
    Q_INVOKABLE int activateWindow(const QString &role, const QString &drawing_area);
    Q_INVOKABLE int deactivateWindow(const QString &role);
    Q_INVOKABLE int endDraw(const QString &role);

    void set_event_handler(enum QEventType et, handler_fun f);

    double get_scale_factor() const { return screen_info->scale_factor(); };

    // These APIs are deprecated, please use new API
    THIS_FUNCTION_IS_DEPRECATED(Q_INVOKABLE int activateSurface(const QString &role));
    THIS_FUNCTION_IS_DEPRECATED(Q_INVOKABLE int activateSurface(const QString &role, const QString &drawing_area));
    THIS_FUNCTION_IS_DEPRECATED(Q_INVOKABLE int deactivateSurface(const QString &role));

public slots:
    void slotActivateWindow();

    // This API is deprecated, please use new API
    THIS_FUNCTION_IS_DEPRECATED(void slotActivateSurface());

private:
    LibWindowmanager* wm;
    std::string graphic_role;
    bool isActive;
    AGLScreenInfoPrivate* screen_info;
};
#endif // LIBWINDOWMANAGER_H

/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "qlibwindowmanager.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>

using namespace std;

int QLibWindowmanager::init(int port, const QString &token) {
    string ctoken = token.toStdString();
    int ret_init =  this->wm->init(port, ctoken);

    this->screen_info = new AGLScreenInfoPrivate;

    if(ret_init == 0) {
        struct Screen scrn = this->wm->getScreenInfo();
        this->screen_info->set_width_dp(scrn.width_dp);
        this->screen_info->set_height_dp(scrn.height_dp);
        this->screen_info->set_width_mm(scrn.width_mm);
        this->screen_info->set_height_mm(scrn.height_mm);
        this->screen_info->set_scale_factor(scrn.scale);
    }

    return ret_init;
}

int QLibWindowmanager::requestSurface(const QString &role) {
    this->graphic_role = role.toStdString();
    int surface_id = this->wm->requestSurface(this->graphic_role.c_str());
    if(surface_id < 0){
        qDebug("failed to get surfaceID");
        return -1;
    }
    else{
        qDebug("surfaceID is set to %d", surface_id);
        char buf[65];   // surface id is under 64bit(1.84E19,) so 65 is sufficient for buffer
        snprintf(buf, 65, "%d", surface_id);
        setenv("QT_IVI_SURFACE_ID", buf, 1);
        return 0;
    }
}

int QLibWindowmanager::activateWindow(const QString &role) {
    string srole = role.toStdString();
    // Request default drawing area "normal.full" in libwindowmanager
    return this->wm->activateWindow(srole.c_str());
}

int QLibWindowmanager::activateWindow(const QString &role, const QString &area) {
    string srole = role.toStdString();
    string sarea = area.toStdString();
    return this->wm->activateWindow(srole.c_str(), sarea.c_str());
}

int QLibWindowmanager::deactivateWindow(const QString &role) {
    string srole = role.toStdString();
    return this->wm->deactivateWindow(srole.c_str());
}

// This API is deprecated, please use new API
int QLibWindowmanager::activateSurface(const QString &role) {
    return this->activateWindow(role);
}

// This API is deprecated, please use new API
int QLibWindowmanager::activateSurface(const QString &role, const QString &area) {
    return this->activateWindow(role, area);
}

// This API is deprecated, please use new API
int QLibWindowmanager::deactivateSurface(const QString &role) {
    return this->deactivateWindow(role);
}

int QLibWindowmanager::endDraw(const QString &role) {
    string srole = role.toStdString();
    return this->wm->endDraw(srole.c_str());
    }

void QLibWindowmanager::set_event_handler(enum QEventType et,
                                  handler_fun f) {
    LibWindowmanager::EventType wet = (LibWindowmanager::EventType)et;
    return this->wm->set_event_handler(wet, std::move(f));
}

void QLibWindowmanager::slotActivateWindow(){
    // This is needed for first rendering when the app is launched
    if(!isActive){
        qDebug("Let's show %s", qPrintable(this->graphic_role.c_str()));
        isActive = true;
        this->activateWindow(this->graphic_role.c_str());
    }
}

// This API is deprecated, please use new API
void QLibWindowmanager::slotActivateSurface(){
    this->slotActivateWindow();
}

QLibWindowmanager::QLibWindowmanager(QObject *parent)
    :QObject(parent), isActive(false), screen_info(nullptr)
{
    wm = new LibWindowmanager();
}

QLibWindowmanager::~QLibWindowmanager()
{
    delete wm;
    delete screen_info;
}
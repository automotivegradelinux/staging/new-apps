/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef WINDOW_MANAGER_HPP
#define WINDOW_MANAGER_HPP

#include <atomic>
#include <memory>
#include <unordered_map>
#include <experimental/optional>
#include "result.hpp"
#include "pm_wrapper.hpp"
#include "util.hpp"
#include "request.hpp"
#include "wm_error.hpp"
#include "wm_layer_control.hpp"
#include "wm_connection.hpp"
#include "low_can_client.hpp"
extern "C"
{
#include <afb/afb-binding.h>
}

struct json_object;

namespace wm
{

using std::experimental::optional;

/* DrawingArea name used by "{layout}.{area}" */
extern const char kNameLayoutNormal[];
extern const char kNameLayoutSplit[];
extern const char kNameAreaFull[];
extern const char kNameAreaMain[];
extern const char kNameAreaSub[];

/* Key for json obejct */
extern const char kKeyDrawingName[];
extern const char kKeyDrawingArea[];
extern const char kKeyDrawingRect[];
extern const char kKeyX[];
extern const char kKeyY[];
extern const char kKeyWidth[];
extern const char kKeyHeigh[];
extern const char kKeyWidthPixel[];
extern const char kKeyHeightPixel[];
extern const char kKeyWidthMm[];
extern const char kKeyHeightMm[];
extern const char kKeyScale[];
extern const char kKeyIds[];

struct id_allocator
{
    unsigned next = 1;

    // Surfaces that where requested but not yet created
    std::unordered_map<unsigned, std::string> id2name;
    std::unordered_map<std::string, unsigned> name2id;

    id_allocator(id_allocator const &) = delete;
    id_allocator(id_allocator &&) = delete;
    id_allocator &operator=(id_allocator const &);
    id_allocator &operator=(id_allocator &&) = delete;

    // Insert and return a new ID
    unsigned generate_id(std::string const &name)
    {
        unsigned sid = this->next++;
        this->id2name[sid] = name;
        this->name2id[name] = sid;
        HMI_DEBUG("allocated new id %u with name %s", sid, name.c_str());
        return sid;
    }

    // Insert a new ID which defined outside
    void register_name_id(std::string const &name, unsigned sid)
    {
        this->id2name[sid] = name;
        this->name2id[name] = sid;
        HMI_DEBUG("register id %u with name %s", sid, name.c_str());
        return;
    }

    // Lookup by ID or by name
    optional<unsigned> lookup(std::string const &name) const
    {
        auto i = this->name2id.find(name);
        return i == this->name2id.end() ? nullopt : optional<unsigned>(i->second);
    }

    optional<std::string> lookup(unsigned id) const
    {
        auto i = this->id2name.find(id);
        return i == this->id2name.end() ? nullopt
                                        : optional<std::string>(i->second);
    }

    // Remove a surface id and name
    void remove_id(std::string const &name)
    {
        auto i = this->name2id.find(name);
        if (i != this->name2id.end())
        {
            this->id2name.erase(i->second);
            this->name2id.erase(i);
        }
    }

    void remove_id(unsigned id)
    {
        auto i = this->id2name.find(id);
        if (i != this->id2name.end())
        {
            this->name2id.erase(i->second);
            this->id2name.erase(i);
        }
    }
};

struct TmpClient
{
    std::string   appid;
    unsigned pid;
};

struct TmpService
{
    std::string appid;  // Used to search who create service surface
    std::string dest;   // Used to attach service to destination application
    std::string service;// The name of service surface
    std::string uuid;   // uuid
    TmpService(const std::string& app, const std::string& dst,
               const std::string& svc, const std::string& uuid)
    : appid(app), dest(dst), service(svc), uuid(uuid) {}
};

struct CarInfo
{
    CarInfo()
        : parking_brake_stt(true),
          accel_pedal_stt(false),
          accel_pedal_pos(0.0),
          running_stt(false),
          headlamp_stt(false),
          lightstatus_brake_stt(true)
    {};

    bool parking_brake_stt;
    bool accel_pedal_stt;
    double accel_pedal_pos;
    bool running_stt;
    bool headlamp_stt;
    bool lightstatus_brake_stt;
};

class WindowManager
{
  public:
    typedef std::unordered_map<uint32_t, struct rect> rect_map;
    typedef std::function<void(const char *err_msg)> reply_func;

    enum EventType
    {
        Event_Val_Min = 0,

        Event_Active = Event_Val_Min,
        Event_Inactive,

        Event_Visible,
        Event_Invisible,

        Event_SyncDraw,
        Event_FlushDraw,

        Event_ScreenUpdated,

        Event_HeadlampOff,
        Event_HeadlampOn,

        Event_ParkingBrakeOff,
        Event_ParkingBrakeOn,

        Event_LightstatusBrakeOff,
        Event_LightstatusBrakeOn,

        Event_CarStop,
        Event_CarRun,

        Event_Error,

        Event_Val_Max = Event_Error,
    };

    explicit WindowManager();
    ~WindowManager() = default;

    WindowManager(WindowManager const &) = delete;
    WindowManager &operator=(WindowManager const &) = delete;
    WindowManager(WindowManager &&) = delete;
    WindowManager &operator=(WindowManager &&) = delete;

    int init();

    result<int> api_request_surface(char const *appid, char const *role);
    char const *api_request_surface(char const *appid, char const *role, char const *ivi_id);
    bool api_set_role(char const *appid, char const *role);
    void api_activate_surface(char const *appid, char const *role, char const *drawing_area, const reply_func &reply);
    void api_activate_surface_for_slave(char const *appid, char const *drawing_name,
                                        char const *drawing_area, const reply_func &reply);
    void api_activate_surface_to_master(char const *appid, char const *drawing_name,
                                        char const *drawing_area, const reply_func &reply);
    void api_deactivate_surface(char const *appid, char const *role, const reply_func &reply);
    void api_deactivate_surface_for_slave(char const *appid, char const *drawing_name,
                                          const reply_func &reply);
    void api_deactivate_surface_to_master(char const *appid, char const *drawing_name,
                                          const reply_func &reply);
    void api_enddraw(char const *appid, char const *role);
    void api_enddraw_for_remote(char const *appid, char const *drawing_name);
    bool api_client_set_render_order(const char *appid, const std::vector<std::string> &render_order);
    std::string api_client_attach_service_surface(const char* appid, const char* dest, const char* service_surface);
    result<json_object *> api_get_display_info();
    result<json_object *> api_get_area_info(char const *role);
    result<json_object *> api_get_car_info(char const *label);
    void send_event(char const *evname);
    void send_event(char const *evname, char const *label);
    void send_event(char const *evname, char const *label, char const *area, int x, int y, int w, int h);

    // Events from the compositor we are interested in
    void surface_created(unsigned pid, unsigned surface_id);
    void surface_removed(unsigned surface_id);

    void removeClient(const std::string &appid);
    void exceptionProcessForTransition();
    const char* convertRoleOldToNew(char const *role);

    void analyzeReceivedEvent(const char *event, struct json_object *object);

    // Do not use this function
    void timerHandler();
    void startTransitionWrapper(std::vector<WMAction> &actions);
    void processError(WMError error);
    void processForRemoteRequest(json_object *data);
    std::string searchApp(unsigned pid, unsigned ppid, unsigned surface, json_object* resp);
    void storeSurface(const std::string& appid, unsigned ppid, unsigned surface);

    const std::vector<const char *> kListEventName{
        "active",
        "inactive",
        "visible",
        "invisible",
        "syncDraw",
        "flushDraw",
        "screenUpdated",
        "headlampOff",
        "headlampOn",
        "parkingBrakeOff",
        "parkingBrakeOn",
        "lightstatusBrakeOff",
        "lightstatusBrakeOn",
        "carStop",
        "carRun",
        "error"};
    std::map<const char *, struct afb_event> map_afb_event;

    WMConnection wmcon;

  private:
    // WM Events to clients
    void emit_activated(char const *label);
    void emit_deactivated(char const *label);
    void emit_syncdraw(char const *label, char const *area, int x, int y, int w, int h);
    void emit_syncdraw(const std::string &role, const std::string &area);
    void emit_flushdraw(char const *label);
    void emit_visible(char const *label, bool is_visible);
    void emit_invisible(char const *label);
    void emit_visible(char const *label);
    void emitHeadlampOff();
    void emitHeadlampOn();
    void emitParkingBrakeOff();
    void emitParkingBrakeOn();
    void emitLightstatusBrakeOff();
    void emitLightstatusBrakeOn();
    void emitCarStop();
    void emitCarRun();

    WMError setRequest(const std::string &appid, const std::string &role, const std::string &area,
                             Task task, unsigned *req_num);
    WMError setRequest(Task task, unsigned* req_num);
    WMError setRequestForSlave(const std::string& appid, const std::string &role,
                               const std::string &area, Task task, unsigned* req_num);
    WMError checkPolicy(unsigned req_num);
    WMError checkPolicyForSlave(unsigned req_num);
    WMError startTransition(unsigned req_num);
    void transitionCarState(TaskCarState task);

    WMError doEndDraw(unsigned req_num);
    void    emitScreenUpdated(unsigned req_num);

    void setTimer();
    void stopTimer();
    void processNextRequest();

    int loadOldRolesConfigFile();

    Task convertCanSignalToCarStateTask(const char *signal_name);
    void inputCarStateTask(Task task);

    const char *check_surface_exist(const char *role);

  private:
    std::unordered_map<std::string, std::string> roleold2new;
    std::unordered_map<std::string, std::string> rolenew2old;
    std::shared_ptr<LayerControl> lc;

    LowCanClient lcc;
    CarInfo crr_car_info;

    PMWrapper pmw;

    // ID allocation and proxy methods for lookup
    struct id_allocator id_alloc;
    // Surface are info (x, y, w, h)
    rect_map area_info;
    // FOR CES DEMO
    std::unordered_map<unsigned, struct TmpClient> tmp_surface2app;
    std::vector<struct TmpService> tmp_services;
    static const char* kDefaultOldRolesConfig;
};

} // namespace wm

#endif // WINDOW_MANAGER_HPP

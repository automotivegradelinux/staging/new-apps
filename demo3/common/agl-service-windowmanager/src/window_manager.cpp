/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fstream>
#include <regex>
#include <sstream>

#include "window_manager.hpp"
#include "json_helper.hpp"
#include "applist.hpp"

extern "C"
{
#include <systemd/sd-event.h>
}

using std::string;
using std::vector;

namespace wm
{

static const uint64_t kTimeOut = 3ULL; /* 3s */

/* DrawingArea name used by "{layout}.{area}" */
const char kNameLayoutNormal[] = "normal";
const char kNameLayoutSplit[]  = "split";
const char kNameAreaFull[]     = "full";
const char kNameAreaMain[]     = "main";
const char kNameAreaSub[]      = "sub";

/* Key for json obejct */
const char kKeyDrawingName[] = "drawing_name";
const char kKeyDrawingArea[] = "drawing_area";
const char kKeyDrawingRect[] = "drawing_rect";
const char kKeyX[]           = "x";
const char kKeyY[]           = "y";
const char kKeyWidth[]       = "width";
const char kKeyHeight[]      = "height";
const char kKeyWidthPixel[]  = "width_pixel";
const char kKeyHeightPixel[] = "height_pixel";
const char kKeyWidthMm[]     = "width_mm";
const char kKeyHeightMm[]    = "height_mm";
const char kKeyScale[]       = "scale";
const char kKeyIds[]         = "ids";

static const char kPathOldRolesConfigFile[] = "/etc/old_roles.json";

static sd_event_source *g_timer_ev_src = nullptr;
static AppList g_app_list;
static WindowManager *g_context;

struct AfbClosure {
public:
    AfbClosure(unsigned pid, unsigned ppid, unsigned surface)
        : pid(pid), ppid(ppid), surface(surface) {}
    ~AfbClosure() = default;
    unsigned pid;
    unsigned ppid;
    unsigned surface;
};

namespace
{

static int processTimerHandler(sd_event_source *s, uint64_t usec, void *userdata)
{
    HMI_NOTICE("Time out occurs because the client replys endDraw slow, so revert the request");
    reinterpret_cast<wm::WindowManager *>(userdata)->timerHandler();
    return 0;
}

static void onStateTransitioned(vector<WMAction> actions)
{
    g_context->startTransitionWrapper(actions);
}

static void onError()
{
    g_context->processError(WMError::LAYOUT_CHANGE_FAIL);
}

static void onReceiveRemoteRequest(json_object *data)
{
    g_context->processForRemoteRequest(data);
}
} // namespace

/**
 * WindowManager Impl
 */
WindowManager::WindowManager()
    : wmcon{},
      id_alloc{}
{
    const char *path = getenv("AFM_APP_INSTALL_DIR");
    if (!path)
    {
        HMI_ERROR("AFM_APP_INSTALL_DIR is not defined");
    }
    string root = path;

    // TODO: ECU name should be decide by config file
    // Get mode and decide ECU name
    string ecu_name = this->wmcon.getEcuName();

    this->lc = std::make_shared<LayerControl>(root, ecu_name);

    HMI_DEBUG("Layer Controller initialized");
}

int WindowManager::init()
{
    LayerControlCallbacks lmcb;
    lmcb.surfaceCreated = [&](unsigned pid, unsigned surface){
        this->surface_created(pid, surface);
        };
    lmcb.surfaceDestroyed = [&](unsigned surface){
        this->surface_removed(surface);
    };
    this->lc->init(lmcb);

    // TODO: application requests by old role,
    //       so create role map (old, new)
    // Load old_roles config file
    this->loadOldRolesConfigFile();

    // Initialize LowCanClient
    this->lcc.initialize();

    // Store my context for calling callback from PolicyManager
    g_context = this;

    // Initialize PMWrapper
    this->pmw.initialize(this->wmcon.getEcuName());

    // Register callback to PolicyManager
    this->pmw.registerCallback(onStateTransitioned, onError);

    // Initialize WMConnection
    this->wmcon.initialize();

    // Register callback to WMConnection
    this->wmcon.registerCallback(onReceiveRemoteRequest);

    // Make afb event
    for (int i = Event_Val_Min; i <= Event_Val_Max; i++)
    {
        map_afb_event[kListEventName[i]] = afb_daemon_make_event(kListEventName[i]);
    }

    const struct rect css_bg = this->lc->getAreaSize("fullscreen");
    Screen screen = this->lc->getScreenInfo();
    rectangle dp_bg(screen.width(), screen.height());

    dp_bg.set_aspect(static_cast<double>(css_bg.w) / css_bg.h);
    dp_bg.fit(screen.width(), screen.height());
    dp_bg.center(screen.width(), screen.height());
    HMI_DEBUG("SCALING: CSS BG(%dx%d) -> DDP %dx%d,(%dx%d)",
              css_bg.w, css_bg.h, dp_bg.left(), dp_bg.top(), dp_bg.width(), dp_bg.height());

    double scale = static_cast<double>(dp_bg.height()) / css_bg.h;
    this->lc->setupArea(dp_bg, scale);

    return 0; //init_layers();
}

result<int> WindowManager::api_request_surface(char const *appid, char const *drawing_name)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *role = this->convertRoleOldToNew(drawing_name);
    string l_name;
    string s_appid = appid;
    string s_role = role;

    if(!g_app_list.contains(s_appid))
    {
        // auto lid = this->layers.get_layer_id(string(role));
        unsigned l_id = this->lc->getNewLayerID(s_role, &l_name);
        if (l_id == 0)
        {
            /**
             * register drawing_name as fallback and make it displayed.
             */
            l_id = this->lc->getNewLayerID("fallback", &l_name);
            HMI_DEBUG("%s is not registered in layers.json, then fallback as normal app", role);
            if (l_id == 0)
            {
                return Err<int>("Designated role does not match any role, fallback is disabled");
            }
        }

        // TODO: remote layer size is fixed value
        if ("Remote" == l_name)
        {
            this->lc->createNewRemoteLayer(l_id);
        }
        else
        {
            this->lc->createNewLayer(l_id);
        }

        // add client into the db
        g_app_list.addClient(s_appid, l_id, s_role);
    }

    // generate surface ID for ivi-shell application

    auto rname = this->id_alloc.lookup(string(role));
    if (!rname)
    {
        // name does not exist yet, allocate surface id...
        auto id = int(this->id_alloc.generate_id(role));
        this->tmp_surface2app[id] = {s_appid, 0};

        // Work Around
        HMI_NOTICE("WORK AROUND: add surface on request surface");
        auto client = g_app_list.lookUpClient(s_appid);
        client->addSurface(id);
        ///////////////

        // Set role map of (new, old)
        this->rolenew2old[role] = string(drawing_name);

        return Ok<int>(id);
    }

    // Check currently registered drawing names if it is already there.
    return Err<int>("Surface already present");
}

char const *WindowManager::api_request_surface(char const *appid, char const *drawing_name,
                                     char const *ivi_id)
{
    unsigned sid = std::stol(ivi_id);

    HMI_DEBUG("This API(requestSurfaceXDG) is for XDG Application using runXDG");
    /*
     * IVI-shell doesn't send surface_size event via ivi-wm protocol
     * if the application is using XDG surface.
     * So WM has to set surface size with original size here
     */
    WMError ret = this->lc->setXDGSurfaceOriginSize(sid);
    if(ret != SUCCESS)
    {
        HMI_ERROR("%s", errorDescription(ret));
        HMI_WARNING("The main user of this API is runXDG");
        return "fail";
    }

    // TODO: application requests by old role,
    //       so convert role old to new
    const char *role = this->convertRoleOldToNew(drawing_name);
    string s_role = role;
    string s_appid = appid;
    string l_name;

    if(!g_app_list.contains(s_appid))
    {
        // auto lid = this->layers.get_layer_id(string(role));
        unsigned l_id = this->lc->getNewLayerID(s_role, &l_name);
        if (l_id == 0)
        {
            /**
             * register drawing_name as fallback and make it displayed.
             */
            l_id = this->lc->getNewLayerID("fallback", &l_name);
            HMI_DEBUG("%s is not registered in layers.json, then fallback as normal app", role);
            if (l_id == 0)
            {
                return "Designated role does not match any role, fallback is disabled";
            }
        }

        // TODO: remote layer size is fixed value
        if ("Remote" == l_name)
        {
            this->lc->createNewRemoteLayer(l_id);
        }
        else
        {
            this->lc->createNewLayer(l_id);
        }

        // add client into the db
        g_app_list.addClient(s_appid, l_id, s_role);
    }

    auto rname = this->id_alloc.lookup(s_role);
    if (rname)
    {
        return "Surface already present";
    }

    // register pair drawing_name and ivi_id
    this->id_alloc.register_name_id(role, sid);

    auto client = g_app_list.lookUpClient(s_appid);
    client->addSurface(sid);

    // Set role map of (new, old)
    this->rolenew2old[role] = string(drawing_name);

    return nullptr;
}

bool WindowManager::api_set_role(char const *appid, char const *drawing_name)
{
    bool ret = false;

    // TODO: application requests by old role,
    //       so convert role old to new
    const char *role = this->convertRoleOldToNew(drawing_name);
    string s_role = role;
    string s_appid = appid;
    string l_name;

    // Create WMClient
    if(!g_app_list.contains(s_appid))
    {
        // auto lid = this->layers.get_layer_id(string(role));
        unsigned l_id = this->lc->getNewLayerID(s_role, &l_name);
        if (l_id == 0)
        {
            /**
             * register drawing_name as fallback and make it displayed.
             */
            l_id = this->lc->getNewLayerID("fallback", &l_name);
            HMI_DEBUG("%s is not registered in layers.json, then fallback as normal app", role);
            if (l_id == 0)
            {
                HMI_ERROR("Designated role does not match any role, fallback is disabled");
                return ret;
            }
        }

        // TODO: remote layer size is fixed value
        if ("Remote" == l_name)
        {
            this->lc->createNewRemoteLayer(l_id);
        }
        else
        {
            this->lc->createNewLayer(l_id);
        }

        // add client into the db
        g_app_list.addClient(s_appid, l_id, s_role);
        // Set role map of (new, old)
        this->rolenew2old[role] = string(drawing_name);
    }

    // for(auto itr = this->tmp_surface2app.begin();
    //         itr != this->tmp_surface2app.end() ; ++itr)
    // {
    for(auto& x : this->tmp_surface2app)
    {
        if(x.second.appid == s_appid)
        {
            unsigned surface = x.first;
            auto client = g_app_list.lookUpClient(s_appid);
            client->addSurface(surface);
            this->tmp_surface2app.erase(surface);
            this->id_alloc.register_name_id(s_role, surface);
            break;
        }
    }

/*     if(0 != pid){
        // search floating surfaceID from pid if pid is designated.
        wm_err = g_app_list.popFloatingSurface(pid, &surface);
    }
    else{
        // get floating surface with appid. If WM queries appid from pid,
        // WM can bind surface and role with appid(not implemented yet)
        //wm_err = g_app_list.popFloatingSurface(id);
    }
    if(wm_err != WMError::SUCCESS){
        HMI_ERROR("No floating surface for app: %s", id.c_str());
        g_app_list.addFloatingClient(id, lid, role);
        HMI_NOTICE("%s : Waiting for surface creation", id.c_str());
        return ret;
    }

    ret = true;
    if (g_app_list.contains(id))
    {
        HMI_INFO("Add role: %s with surface: %d. Client %s has multi surfaces.",
                 role.c_str(), surface, id.c_str());
        auto client = g_app_list.lookUpClient(id);
        client->appendRole(role);
    }
    else{
        HMI_INFO("Create new client: %s, surface: %d into layer: %d with role: %s",
                 id.c_str(), surface, lid, role.c_str());
        g_app_list.addClient(id, lid, role);
    } */

    // register pair drawing_name and ivi_id

    return true;
}

void WindowManager::api_activate_surface(char const *appid, char const *drawing_name,
                               char const *drawing_area, const reply_func &reply)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    string id = appid;
    string role = c_role;
    string area = drawing_area;

    if(!g_app_list.contains(id))
    {
        reply("app doesn't request 'requestSurface' or 'setRole' yet");
        return;
    }
    auto client = g_app_list.lookUpClient(id);

    // unsigned srfc = client->surfaceID(role);
    // unsigned layer = client->layerID();

    // g_app_list.removeFloatingSurface(client->surfaceID());
    // g_app_list.removeFloatingSurface(client);

    Task task = Task::TASK_ALLOCATE;
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequest(id, role, area, task, &req_num);

    //vector<WMLayerState> current_states = this->lc->getCurrentStates();
    // ret = this->setRequest(id, role, area, task, current_states, &req_num);

    if(ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        reply("Failed to set request");
        return;
    }

    reply(nullptr);
    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
     * Do allocate tasks
     */
    ret = this->checkPolicy(req_num);

    if (ret != WMError::SUCCESS)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }
}

void WindowManager::api_activate_surface_for_slave(
                               char const *appid, char const *drawing_name,
                               char const *drawing_area, const reply_func &reply)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    string id = appid;
    string role = c_role;
    string area = drawing_area;

    if(!g_app_list.contains(id))
    {
        // Request surface of app in slave to register app information
        this->api_request_surface(appid, drawing_name);

        // Set role of app in slave to register app information
        this->api_set_role(appid, drawing_name);
    }
    auto client = g_app_list.lookUpClient(id);

    // unsigned srfc = client->surfaceID(role);
    // unsigned layer = client->layerID();

    // g_app_list.removeFloatingSurface(client->surfaceID());
    // g_app_list.removeFloatingSurface(client);

    Task task = Task::TASK_ALLOCATE;
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequestForSlave(id, role, area, task, &req_num);

    //vector<WMLayerState> current_states = this->lc->getCurrentStates();
    // ret = this->setRequest(id, role, area, task, current_states, &req_num);

    if(ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        reply("Failed to set request");
        return;
    }

    reply(nullptr);
    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
     * Do allocate tasks
     */
    ret = this->checkPolicyForSlave(req_num);

    if (ret != WMError::SUCCESS)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }
}

void WindowManager::api_activate_surface_to_master(
                               char const *appid, char const *drawing_name,
                               char const *drawing_area, const reply_func &reply)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    string id = appid;
    string role = c_role;
    string area = drawing_area;

    if(!g_app_list.contains(id))
    {
        reply("app doesn't request 'requestSurface' or 'setRole' yet");
        return;
    }
    auto client = g_app_list.lookUpClient(id);

    // unsigned srfc = client->surfaceID(role);
    // unsigned layer = client->layerID();

    // g_app_list.removeFloatingSurface(client->surfaceID());
    // g_app_list.removeFloatingSurface(client);

    Task task = Task::TASK_ALLOCATE;
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequest(id, role, area, task, &req_num);

    //vector<WMLayerState> current_states = this->lc->getCurrentStates();
    // ret = this->setRequest(id, role, area, task, current_states, &req_num);

    if(ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        reply("Failed to set request");
        return;
    }

    reply(nullptr);
    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
     * Do allocate tasks
     */
    int i_ret = this->wmcon.sendRequest("activateWindow", appid,
                                        drawing_name, drawing_area);
    if (0 > i_ret)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }

    this->setTimer();
}

void WindowManager::api_deactivate_surface(char const *appid, char const *drawing_name,
                                 const reply_func &reply)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    /*
    * Check Phase
    */
    string id = appid;
    string role = c_role;
    string area = ""; //drawing_area;
    Task task = Task::TASK_RELEASE;
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequest(id, role, area, task, &req_num);

    if (ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        reply("Failed to set request");
        return;
    }

    reply(nullptr);
    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
    * Do allocate tasks
    */
    ret = this->checkPolicy(req_num);

    if (ret != WMError::SUCCESS)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }
}

void WindowManager::api_deactivate_surface_for_slave(char const *appid, char const *drawing_name,
                                 const reply_func &reply)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    /*
    * Check Phase
    */
    string id = appid;
    string role = c_role;
    string area = "";//drawing_area;
    Task task = Task::TASK_RELEASE;
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequest(id, role, area, task, &req_num);

    if (ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        reply("Failed to set request");
        return;
    }

    reply(nullptr);
    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
    * Do allocate tasks
    */
    ret = this->checkPolicyForSlave(req_num);

    if (ret != WMError::SUCCESS)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }
}

void WindowManager::api_deactivate_surface_to_master(char const *appid, char const *drawing_name,
                                 const reply_func &reply)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    /*
    * Check Phase
    */
    string id = appid;
    string role = c_role;
    string area = "";//drawing_area;
    Task task = Task::TASK_RELEASE;
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequest(id, role, area, task, &req_num);

    if (ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        reply("Failed to set request");
        return;
    }

    reply(nullptr);
    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
    * Do allocate tasks
    */
    int i_ret = this->wmcon.sendRequest("deactivateWindow", appid,
                                        drawing_name, "");
    if (0 > i_ret)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }

    this->setTimer();
}

void WindowManager::api_enddraw(char const *appid, char const *drawing_name)
{
    // TODO: application requests by old role,
    //       so convert role old to new
    const char *c_role = this->convertRoleOldToNew(drawing_name);

    string id = appid;
    string role = c_role;
    unsigned current_req = g_app_list.currentRequestNumber();
    bool result = g_app_list.setEndDrawFinished(current_req, id, role);

    if (!result)
    {
        HMI_ERROR("%s is not in transition state", id.c_str());
        return;
    }

    if (g_app_list.endDrawFullfilled(current_req))
    {
        // do task for endDraw
        this->stopTimer();
        WMError ret = this->doEndDraw(current_req);

        if(ret != WMError::SUCCESS)
        {
            //this->emit_error();

            // Undo state of PolicyManager
            this->pmw.undoState();
            this->lc->undoUpdate();
        }
        this->emitScreenUpdated(current_req);
        HMI_SEQ_INFO(current_req, "Finish request status: %s", errorDescription(ret));

        g_app_list.removeRequest(current_req);

        this->processNextRequest();
    }
    else
    {
        HMI_SEQ_INFO(current_req, "Wait other App call endDraw");
        return;
    }
}

void WindowManager::api_enddraw_for_remote(char const *appid, char const *drawing_name)
{
    int ret = this->wmcon.sendRequest("endDraw", appid, drawing_name, "");
    if (0 > ret)
    {
        //this->emit_error()

        this->pmw.undoState();
        this->lc->undoUpdate();

        unsigned current_req = g_app_list.currentRequestNumber();
        g_app_list.removeRequest(current_req);
        this->processNextRequest();

        return;
    }

    this->api_enddraw(appid, drawing_name);
}

bool WindowManager::api_client_set_render_order(char const* appid, const vector<string>& render_order)
{
    bool ret = false;
    string id = appid;
    auto client = g_app_list.lookUpClient(id);
    if(client)
    {
        client->setRenderOrder(render_order);
    }
    return ret;
}

string WindowManager::api_client_attach_service_surface
    (const char* appid, const char* dest, const char* service_surface)
{
    string uuid, s_dest = dest;
    auto client = g_app_list.lookUpClient(s_dest);
    if(!client)
    {
        HMI_ERROR("Failed to look up destination [%s]", dest);
        return uuid;
    }
    uuid = client->attachTmpServiceSurface(appid, service_surface);
    this->tmp_services.emplace_back(TmpService{appid, dest, service_surface, uuid});
    return uuid;
}

result<json_object *> WindowManager::api_get_display_info()
{
    Screen screen = this->lc->getScreenInfo();

    json_object *object = json_object_new_object();
    json_object_object_add(object, kKeyWidthPixel, json_object_new_int(screen.width()));
    json_object_object_add(object, kKeyHeightPixel, json_object_new_int(screen.height()));
    // TODO: set size
    json_object_object_add(object, kKeyWidthMm, json_object_new_int(0));
    json_object_object_add(object, kKeyHeightMm, json_object_new_int(0));
    json_object_object_add(object, kKeyScale, json_object_new_double(this->lc->scale()));

    return Ok<json_object *>(object);
}

result<json_object *> WindowManager::api_get_area_info(char const *drawing_name)
{
    HMI_DEBUG("called");

    // TODO: application requests by old role,
    //       so convert role old to new
    const char *role = this->convertRoleOldToNew(drawing_name);

    // Check drawing name, surface/layer id
    auto const &surface_id = this->id_alloc.lookup(string(role));
    if (!surface_id)
    {
        return Err<json_object *>("Surface does not exist");
    }

    // Set area rectangle
    rect area_info = this->area_info[*surface_id];
    json_object *object = json_object_new_object();
    json_object_object_add(object, kKeyX, json_object_new_int(area_info.x));
    json_object_object_add(object, kKeyY, json_object_new_int(area_info.y));
    json_object_object_add(object, kKeyWidth, json_object_new_int(area_info.w));
    json_object_object_add(object, kKeyHeight, json_object_new_int(area_info.h));

    return Ok<json_object *>(object);
}

result<json_object *> WindowManager::api_get_car_info(char const *label)
{
    json_object *j_in  = nullptr;
    json_object *j_out = nullptr;

    if (0 == strcmp("parking_brake_status", label))
    {
        // Get parking brake status
        json_bool val = (this->crr_car_info.parking_brake_stt) ? TRUE : FALSE;
        j_in = json_object_new_boolean(val);
    }
    else if (0 == strcmp("accelerator.pedal.position", label))
    {
        // Get accelerator pedal position
        double val = this->crr_car_info.accel_pedal_pos;
        j_in = json_object_new_double(val);
    }
    else if (0 == strcmp("car_state", label))
    {
        // Get running state
        const char* val = (this->crr_car_info.running_stt) ? "run" : "stop";
        j_in = json_object_new_string(val);
    }
    else if (0 == strcmp("lightstatus.brake", label)) {
        // Get lightstatus brake status
        json_bool val = (this->crr_car_info.lightstatus_brake_stt) ? TRUE : FALSE;
        j_in = json_object_new_boolean(val);
    }
    else
    {
       return Err<json_object *>("Car info does not exist");
    }

    // Create output object
    j_out = json_object_new_object();
    json_object_object_add(j_out, "value", j_in);

    return Ok<json_object *>(j_out);
}

void WindowManager::send_event(char const *evname)
{
    HMI_DEBUG("%s: %s", __func__, evname);

    int ret = afb_event_push(this->map_afb_event[evname], nullptr);
    if (ret != 0)
    {
        HMI_DEBUG("afb_event_push: %m");
    }
}

void WindowManager::send_event(char const *evname, char const *label)
{
    HMI_DEBUG("%s: %s(%s)", __func__, evname, label);

    json_object *j = json_object_new_object();
    json_object_object_add(j, kKeyDrawingName, json_object_new_string(label));

    int ret = afb_event_push(this->map_afb_event[evname], j);
    if (ret != 0)
    {
        HMI_DEBUG("afb_event_push failed: %m");
    }
}

void WindowManager::send_event(char const *evname, char const *label, char const *area,
                     int x, int y, int w, int h)
{
    HMI_DEBUG("%s: %s(%s, %s) x:%d y:%d w:%d h:%d",
              __func__, evname, label, area, x, y, w, h);

    json_object *j_rect = json_object_new_object();
    json_object_object_add(j_rect, kKeyX, json_object_new_int(x));
    json_object_object_add(j_rect, kKeyY, json_object_new_int(y));
    json_object_object_add(j_rect, kKeyWidth, json_object_new_int(w));
    json_object_object_add(j_rect, kKeyHeight, json_object_new_int(h));

    json_object *j = json_object_new_object();
    json_object_object_add(j, kKeyDrawingName, json_object_new_string(label));
    json_object_object_add(j, kKeyDrawingArea, json_object_new_string(area));
    json_object_object_add(j, kKeyDrawingRect, j_rect);

    int ret = afb_event_push(this->map_afb_event[evname], j);
    if (ret != 0)
    {
        HMI_DEBUG("afb_event_push failed: %m");
    }
}

string WindowManager::searchApp(unsigned pid, unsigned ppid, unsigned surface, json_object* resp)
{
    // retrieve appid from pid from application manager
    string appid;
    // check appid then add it to the client
    HMI_INFO("Runners:%s", json_object_get_string(resp));
    int size = json_object_array_length(resp);
    HMI_INFO("pid %d, ppid %d, surface %d",pid, ppid, surface);
    for(int i = 0; i < size; i++)
    {
        json_object *j = json_object_array_get_idx(resp, i);
        int runid      = jh::getIntFromJson(j, "runid");
        const char* id = jh::getStringFromJson(j, "id");
        HMI_DEBUG("Appid %s, runid %d", id, runid);
        if(id && (runid == ppid))
        {
            string s_id = id;
            s_id.erase(s_id.find('@'));
            appid = s_id;
            HMI_INFO("App found %s", appid.c_str());
            break;
        }
    }
    if(appid.empty())
    {
        HMI_WARNING("Failed to retrieve id");
    }
    return appid;
}

void WindowManager::storeSurface(const string& appid, unsigned ppid, unsigned surface)
{
    auto elem = std::find_if(this->tmp_services.begin(), this->tmp_services.end(),
                [&appid](TmpService& ts){
                    return (ts.dest == appid );
                });

    this->lc->setXDGSurfaceOriginSize(surface);
    if(elem != this->tmp_services.end())
    {
        // attachApp
        auto client = g_app_list.lookUpClient(elem->dest);
        if(client == nullptr)
        {
            return;
        }
        HMI_INFO("Attach surface %d (service %s) to app %s", surface, elem->service.c_str(), elem->dest.c_str());
        client->attachServiceSurface(elem->service, surface);
    }
    else
    {
        // setRole
        auto client = g_app_list.lookUpClient(appid);
        if(client != nullptr)
        {
            client->addSurface(surface);
            this->id_alloc.register_name_id(client->role(), surface);
        }
        else
        {
            // Store tmp surface and appid for application
            // who requests setRole after creating shell surface
            this->tmp_surface2app.emplace(surface, TmpClient{appid, ppid});
        }
    }
}

/**
 * proxied events
 */
void WindowManager::surface_created(unsigned pid, unsigned surface_id)
{
    // requestSurface
    if(this->tmp_surface2app.count(surface_id) != 0)
    {
        string appid = this->tmp_surface2app[surface_id].appid;
        auto client = g_app_list.lookUpClient(appid);
        if(client != nullptr)
        {
            WMError ret = client->addSurface(surface_id);
            HMI_INFO("Add surface %d to \"%s\"", surface_id, appid.c_str());
            if(ret != WMError::SUCCESS)
            {
                HMI_ERROR("Failed to add surface to client %s", client->appID().c_str());
            }
        }
        this->tmp_surface2app.erase(surface_id);
    }
    else
    {
        HMI_NOTICE("Unknown surface %d", surface_id);
        // retrieve ppid
        std::ostringstream os;
        os << pid ;
        string path = "/proc/" + os.str() + "/stat";
        std::ifstream ifs(path.c_str());
        string str;
        unsigned ppid = 0;
        if(!ifs.fail() && std::getline(ifs, str))
        {
            std::sscanf(str.data(), "%*d %*s %*c %d", &ppid);
            HMI_INFO("Retrieve ppid %d", ppid);
        }
        else
        {
            HMI_ERROR("Failed to open /proc/%d/stat", pid);
            HMI_ERROR("File system may be different");
            return;
        }
        struct AfbClosure* c = new struct AfbClosure(pid, ppid, surface_id);
        // search pid from surfaceID
        afb_service_call("afm-main", "runners", json_object_new_object(),
            [](void* closure, int stat, json_object* resp){
                HMI_DEBUG("check %s", json_object_get_string(resp));
                struct AfbClosure* c = static_cast<struct AfbClosure*>(closure);
                HMI_DEBUG("check");
                if(stat != 0)
                {
                    HMI_ERROR("Failed to call runners");
                }
                else
                {
                    json_object* j;
                    json_object_object_get_ex(resp, "response", &j);
                    string appid = g_context->searchApp(c->pid, c->ppid, c->surface, j);
                    if(!appid.empty())
                    {
                        g_context->storeSurface(appid, c->ppid, c->surface);
                    }
                }
                json_object_put(resp);
                delete c;
            }, c);
    }
}

void WindowManager::surface_removed(unsigned surface_id)
{
    HMI_DEBUG("Delete surface_id %u", surface_id);
    this->id_alloc.remove_id(surface_id);
    // this->layers.remove_surface(surface_id);
    g_app_list.removeSurface(surface_id);
}

void WindowManager::removeClient(const string &appid)
{
    HMI_DEBUG("Remove clinet %s from list", appid.c_str());
    auto client = g_app_list.lookUpClient(appid);
    this->lc->terminateApp(client);
    g_app_list.removeClient(appid);
}

void WindowManager::exceptionProcessForTransition()
{
    unsigned req_num = g_app_list.currentRequestNumber();
    HMI_SEQ_NOTICE(req_num, "Process exception handling for request. Remove current request %d", req_num);
    g_app_list.removeRequest(req_num);
    HMI_SEQ_NOTICE(g_app_list.currentRequestNumber(), "Process next request if exists");
    this->processNextRequest();
}

void WindowManager::analyzeReceivedEvent(const char *event, struct json_object *object)
{
    HMI_DEBUG("event:%s", event);

    // If receive low can signal
    if (strstr(event, "low-can"))
    {
        // Analyze low can signal
        const char *signal_name = this->lcc.analyzeCanSignal(object);

        // Create task for car state and input it to PolicyManager
        Task task = this->convertCanSignalToCarStateTask(signal_name);
        if (Task::TASK_INVALID != task)
        {
            this->inputCarStateTask(task);
        }
    }
}

void WindowManager::timerHandler()
{
    unsigned req_num = g_app_list.currentRequestNumber();
    HMI_SEQ_DEBUG(req_num, "Timer expired remove Request");
    g_app_list.reqDump();
    g_app_list.removeRequest(req_num);
    this->processNextRequest();
}

void WindowManager::startTransitionWrapper(vector<WMAction> &actions)
{
    WMError ret = WMError::UNKNOWN;
    // req_num is guaranteed by Window Manager
    unsigned req_num = g_app_list.currentRequestNumber();
    Task task = Task::TASK_INVALID;

    if (actions.empty())
    {
        if (g_app_list.haveRequest())
        {
            HMI_SEQ_DEBUG(req_num, "There is no WMAction for this request");
            goto proc_remove_request;
        }
        else
        {
            HMI_SEQ_DEBUG(req_num, "There is no request");
            return;
        }
    }

    // Check weather there is the no request task
    //   [The no request task]
    //     - TaskCarState::RESTRICTION_MODE_OFF
    //     - TaskCarState::RESTRICTION_MODE_ON
    for (const auto &act : actions)
    {
        if (TaskCarState::RESTRICTION_MODE_OFF == act.car_state)
        {
            task = Task::TASK_RESTRICTION_MODE_OFF;
            break;
        }
        else if (TaskCarState::RESTRICTION_MODE_ON == act.car_state)
        {
            task = Task::TASK_RESTRICTION_MODE_ON;
            break;
        }
    }

    // If there is the request-less task, set request here
    if (Task::TASK_INVALID != task) {
        unsigned req_num;
        ret = this->setRequest(task, &req_num);

        if(ret != WMError::SUCCESS)
        {
            goto error;
        }
    }

    for (auto &act : actions)
    {
        if ("" != act.role)
        {
            bool found;
            auto const &surface_id = this->id_alloc.lookup(act.role.c_str());
            if(surface_id == nullopt)
            {
                HMI_SEQ_DEBUG(req_num, "There is not surface id for role:%s", act.role.c_str());
                continue;
            }

            std::string appid = g_app_list.getAppID(*surface_id, &found);
            if (!found)
            {
                if (TaskVisible::INVISIBLE == act.visible)
                {
                    HMI_SEQ_DEBUG(req_num, "role:%s is killed, so do not set this action", act.role.c_str());
                    continue;
                }
                else
                {
                    HMI_SEQ_ERROR(req_num, "appid of role:%s which is visible is not found", act.role.c_str());
                    ret = WMError::FAIL;
                    goto error;
                }
            }
            auto client = g_app_list.lookUpClient(appid);
            act.req_num = req_num;
            act.client = client;

            // If Window Manager is master and this action is for slave,
            // change TaskVisible
            if (this->wmcon.isMasterMode())
            {
                if (this->wmcon.isMasterArea(act.area.c_str()))
                {
                    HMI_DEBUG("Set TaskVisible::REQ_REMOTE_VISIBLE");
                    act.visible = TaskVisible::REQ_REMOTE_VISIBLE;
                }
                // TODO: Check whether role is tbtnavi to request remote invisible
                else if (("tbtnavi" == act.role) &&
                         (TaskVisible::INVISIBLE == act.visible))
                {
                    HMI_DEBUG("Set TaskVisible::REQ_REMOTE_INVISIBLE");
                    act.visible = TaskVisible::REQ_REMOTE_INVISIBLE;
                }
            }
        }

        ret = g_app_list.setAction(req_num, act);
        if (ret != WMError::SUCCESS)
        {
            HMI_SEQ_ERROR(req_num, "Setting action is failed");
            goto error;
        }
    }

    HMI_SEQ_DEBUG(req_num, "Start transition.");
    ret = this->startTransition(req_num);
    if (ret != WMError::SUCCESS)
    {
        if (ret == WMError::NO_LAYOUT_CHANGE)
        {
            goto proc_remove_request;
        }
        else
        {
            HMI_SEQ_ERROR(req_num, "Transition state is failed");
            goto error;
        }
    }

    return;

error:
    //this->emit_error()
    HMI_SEQ_ERROR(req_num, errorDescription(ret));
    this->pmw.undoState();

proc_remove_request:
    g_app_list.removeRequest(req_num);
    this->processNextRequest();
}

void WindowManager::processError(WMError error)
{
    unsigned req_num = g_app_list.currentRequestNumber();

    //this->emit_error()
    HMI_SEQ_ERROR(req_num, errorDescription(error));
    g_app_list.removeRequest(req_num);
    this->processNextRequest();
}

void WindowManager::processForRemoteRequest(json_object *data)
{
    const char *req          = jh::getStringFromJson(data, "req");
    const char *appid        = jh::getStringFromJson(data, "appid");
    const char *drawing_name = jh::getStringFromJson(data, "drawing_name");
    const char *drawing_area = jh::getStringFromJson(data, "drawing_area");

    if (!req || !drawing_name)
    {
        HMI_ERROR("Parse Error!!");
        return;
    }

    if (this->wmcon.isMasterMode())
    {
        if (!appid)
        {
            HMI_ERROR("Parse Error!!");
            return;
        }

        auto reply = [](const char *errmsg) {
            if (errmsg != nullptr)
            {
                HMI_ERROR(errmsg);
                return;
            }
        };

        if ("activateWindow" == std::string(req))
        {
            if (!drawing_area)
            {
                HMI_ERROR("Parse Error!!");
                return;
            }

            this->api_activate_surface_for_slave(
                appid, drawing_name, drawing_area, reply);
        }
        else if ("deactivateWindow" == std::string(req))
        {
            this->api_deactivate_surface_for_slave(
                appid, drawing_name, reply);
        }
        else if ("endDraw" == std::string(req))
        {
            this->api_enddraw(appid, drawing_name);
        }
    }
    else
    {
        if ("syncDraw" == std::string(req))
        {
            this->stopTimer();

            if (!appid || !drawing_area)
            {
                HMI_ERROR("Parse Error!!");
                return;
            }

            unsigned req_num = g_app_list.currentRequestNumber();
            auto client = g_app_list.lookUpClient(appid);

            // TODO: application requests by old role,
            //       so convert role old to new
            const char *c_role = this->convertRoleOldToNew(drawing_name);

            // Create action
            bool end_draw_finished = false;
            WMAction act
            {
                req_num,
                client,
                string(c_role),
                string(drawing_area),
                TaskVisible::REMOTE_VISIBLE,
                end_draw_finished,
                TaskCarState::NO_TASK
            };

            // Set action
            WMError ret = g_app_list.setAction(req_num, act);
            if (ret != WMError::SUCCESS)
            {
                HMI_SEQ_ERROR(req_num, "Setting action is failed");
                return;
            }

            this->emit_syncdraw(string(drawing_name), string(drawing_area));
            this->wmcon.startSyncDrawForRemote(appid);
            this->setTimer();
        }
        else if ("activated" == std::string(req))
        {
            this->emit_visible(drawing_name);
            this->emit_activated(drawing_name);
        }
        else if ("deactivated" == std::string(req))
        {
            this->stopTimer();

            if (!appid || !drawing_area)
            {
                HMI_ERROR("Parse Error!!");
                return;
            }

            unsigned req_num = g_app_list.currentRequestNumber();
            auto client = g_app_list.lookUpClient(appid);

            // TODO: application requests by old role,
            //       so convert role old to new
            const char *c_role = this->convertRoleOldToNew(drawing_name);

            // Create action
            bool end_draw_finished = true;
            WMAction act
            {
                req_num,
                client,
                string(c_role),
                "",
                TaskVisible::REMOTE_INVISIBLE,
                end_draw_finished,
                TaskCarState::NO_TASK
            };

            this->lc->visibilityChange(act);
            this->lc->renderLayers();
            this->lc->renderLayersRemote();

            this->emit_invisible(drawing_name);
            this->emit_deactivated(drawing_name);
            this->emitScreenUpdated(req_num);

            g_app_list.removeRequest(req_num);
            this->processNextRequest();
        }
        else if ("flushDraw" == std::string(req))
        {
            this->emit_flushdraw(drawing_name);
        }
    }
}

/*
 ******* Private Functions *******
 */

void WindowManager::emit_activated(char const *label)
{
    this->send_event(kListEventName[Event_Active], label);
}

void WindowManager::emit_deactivated(char const *label)
{
    this->send_event(kListEventName[Event_Inactive], label);
}

void WindowManager::emit_syncdraw(char const *label, char const *area, int x, int y, int w, int h)
{
    this->send_event(kListEventName[Event_SyncDraw], label, area, x, y, w, h);
}

void WindowManager::emit_syncdraw(const string &role, const string &area)
{
    rect rect = this->lc->getAreaSize(area);
    this->send_event(kListEventName[Event_SyncDraw],
                     role.c_str(), area.c_str(), rect.x, rect.y, rect.w, rect.h);
}

void WindowManager::emit_flushdraw(char const *label)
{
    this->send_event(kListEventName[Event_FlushDraw], label);
}

void WindowManager::emit_visible(char const *label, bool is_visible)
{
    this->send_event(is_visible ? kListEventName[Event_Visible] : kListEventName[Event_Invisible], label);
}

void WindowManager::emit_invisible(char const *label)
{
    return emit_visible(label, false);
}

void WindowManager::emit_visible(char const *label) { return emit_visible(label, true); }

void WindowManager::emitHeadlampOff()
{
    // Send HeadlampOff event for all application
    this->send_event(kListEventName[Event_HeadlampOff]);
}

void WindowManager::emitHeadlampOn()
{
    // Send HeadlampOn event for all application
    this->send_event(kListEventName[Event_HeadlampOn]);
}

void WindowManager::emitParkingBrakeOff()
{
    // Send ParkingBrakeOff event for all application
    this->send_event(kListEventName[Event_ParkingBrakeOff]);
}

void WindowManager::emitParkingBrakeOn()
{
    // Send ParkingBrakeOn event for all application
    this->send_event(kListEventName[Event_ParkingBrakeOn]);
}

void WindowManager::emitLightstatusBrakeOff()
{
    // Send LightstatusBrakeOff event for all application
    this->send_event(kListEventName[Event_LightstatusBrakeOff]);
}

void WindowManager::emitLightstatusBrakeOn()
{
    // Send LightstatusBrakeOn event for all application
    this->send_event(kListEventName[Event_LightstatusBrakeOn]);
}

void WindowManager::emitCarStop()
{
    // Send CarStop event for all application
    this->send_event(kListEventName[Event_CarStop]);
}

void WindowManager::emitCarRun()
{
    // Send CarRun event for all application
    this->send_event(kListEventName[Event_CarRun]);
}

WMError WindowManager::setRequest(const string& appid, const string &role, const string &area,
                            Task task, unsigned* req_num)
{
    if (!g_app_list.contains(appid))
    {
        return WMError::NOT_REGISTERED;
    }

    auto client = g_app_list.lookUpClient(appid);

    /*
     * Queueing Phase
     */
    unsigned current = g_app_list.currentRequestNumber();
    unsigned requested_num = g_app_list.getRequestNumber(appid);
    if (requested_num != 0)
    {
        HMI_SEQ_INFO(requested_num,
            "%s %s %s request is already queued", appid.c_str(), role.c_str(), area.c_str());
        return REQ_REJECTED;
    }

    WMRequest req = WMRequest(appid, role, area, task);
    unsigned new_req = g_app_list.addRequest(req);
    *req_num = new_req;
    g_app_list.reqDump();

    HMI_SEQ_DEBUG(current, "%s start sequence with %s, %s", appid.c_str(), role.c_str(), area.c_str());

    return WMError::SUCCESS;
}

WMError WindowManager::setRequest(Task task, unsigned* req_num)
{
    /*
     * Queueing Phase
     */
    unsigned current = g_app_list.currentRequestNumber();

    WMRequest req = WMRequest(task);
    unsigned new_req = g_app_list.addRequest(req);
    *req_num = new_req;
    g_app_list.reqDump();

    HMI_SEQ_DEBUG(current, "start sequence for task:%d", task);

    return WMError::SUCCESS;
}

WMError WindowManager::setRequestForSlave(const string& appid, const string &role, const string &area,
                            Task task, unsigned* req_num)
{
    /*
     * Queueing Phase
     */
    unsigned current = g_app_list.currentRequestNumber();
    unsigned requested_num = g_app_list.getRequestNumber(appid);
    if (requested_num != 0)
    {
        HMI_SEQ_INFO(requested_num,
            "%s %s %s request is already queued", appid.c_str(), role.c_str(), area.c_str());
        return REQ_REJECTED;
    }

    WMRequest req = WMRequest(appid, role, area, task);
    unsigned new_req = g_app_list.addRequest(req);
    *req_num = new_req;
    g_app_list.reqDump();

    HMI_SEQ_DEBUG(current, "%s start sequence with %s, %s", appid.c_str(), role.c_str(), area.c_str());

    return WMError::SUCCESS;
}

WMError WindowManager::checkPolicy(unsigned req_num)
{
    /*
    * Check Policy
    */
    // get current trigger
    bool found = false;
    WMError ret = WMError::LAYOUT_CHANGE_FAIL;
    auto trigger = g_app_list.getRequest(req_num, &found);
    if (!found)
    {
        ret = WMError::NO_ENTRY;
        return ret;
    }
    string req_area = trigger.area;

    if (trigger.task == Task::TASK_ALLOCATE)
    {
        const char *msg = this->check_surface_exist(trigger.role.c_str());

        if (msg)
        {
            HMI_SEQ_ERROR(req_num, msg);
            return ret;
        }
    }

    // Input event data to PolicyManager
    if (0 > this->pmw.setInputEventData(trigger.task, trigger.role, trigger.area))
    {
        HMI_SEQ_ERROR(req_num, "Failed to set input event data to PolicyManager");
        return ret;
    }

    // Execute state transition of PolicyManager
    if (0 > this->pmw.executeStateTransition())
    {
        HMI_SEQ_ERROR(req_num, "Failed to execute state transition of PolicyManager");
        return ret;
    }

    ret = WMError::SUCCESS;

    g_app_list.reqDump();

    return ret;
}

WMError WindowManager::checkPolicyForSlave(unsigned req_num)
{
    /*
    * Check Policy
    */
    // get current trigger
    bool found = false;
    WMError ret = WMError::LAYOUT_CHANGE_FAIL;
    auto trigger = g_app_list.getRequest(req_num, &found);
    if (!found)
    {
        ret = WMError::NO_ENTRY;
        return ret;
    }
    string req_area = trigger.area;

    // Input event data to PolicyManager
    if (0 > this->pmw.setInputEventData(trigger.task, trigger.role, trigger.area))
    {
        HMI_SEQ_ERROR(req_num, "Failed to set input event data to PolicyManager");
        return ret;
    }

    // Execute state transition of PolicyManager
    if (0 > this->pmw.executeStateTransition())
    {
        HMI_SEQ_ERROR(req_num, "Failed to execute state transition of PolicyManager");
        return ret;
    }

    ret = WMError::SUCCESS;

    g_app_list.reqDump();

    return ret;
}

WMError WindowManager::startTransition(unsigned req_num)
{
    bool sync_draw_happen = false;
    bool found = false;
    WMError ret = WMError::SUCCESS;
    auto actions = g_app_list.getActions(req_num, &found);
    if (!found)
    {
        ret = WMError::NO_ENTRY;
        HMI_SEQ_ERROR(req_num,
            "Window Manager bug :%s : Action is not set", errorDescription(ret));
        return ret;
    }

    g_app_list.reqDump();
    for (const auto &action : actions)
    {
        // TODO: application requests by old role,
        //       so convert role new to old for emitting event
        string old_role = this->rolenew2old[action.role];

        if (action.visible == TaskVisible::VISIBLE)
        {
            sync_draw_happen = true;

            this->emit_syncdraw(old_role, action.area);
            /* TODO: emit event for app not subscriber
               if(g_app_list.contains(y.appid))
               g_app_list.lookUpClient(y.appid)->emit_syncdraw(y.role, y.area); */
        }
        else if(action.visible == TaskVisible::REQ_REMOTE_VISIBLE)
        {
            // If this action is for slave, send to slave
            this->wmcon.sendRequest("syncDraw", action.client->appID().c_str(),
                                    old_role.c_str(), action.area.c_str());
        }
        else if (action.car_state != TaskCarState::NO_TASK)
        {
            this->transitionCarState(action.car_state);
        }
    }

    if (sync_draw_happen)
    {
        this->setTimer();
    }
    else
    {
        // deactivate only, no syncDraw
        // Make it deactivate here
        for (const auto &x : actions)
        {
            this->lc->visibilityChange(x);
            string old_role = this->rolenew2old[x.role];

            if (x.visible == TaskVisible::INVISIBLE)
            {
                emit_deactivated(old_role.c_str());
            }
            else if (x.visible == TaskVisible::REQ_REMOTE_INVISIBLE)
            {
                // If this action is for slave, send to slave
                int i_ret = this->wmcon.sendRequest("deactivated", x.client->appID().c_str(),
                                                    old_role.c_str(), "");
                if (0 > i_ret)
                {
                    ret = WMError::FAIL;
                }
            }

            /* if (g_app_list.contains(x.appid))
            {
                auto client = g_app_list.lookUpClient(x.appid);
                //this->deactivate(client->surfaceID(x.role));
            } */
        }
        this->lc->renderLayers();
        this->lc->renderLayersRemote();
        ret = WMError::NO_LAYOUT_CHANGE;
    }
    return ret;
}

void WindowManager::transitionCarState(TaskCarState task)
{
    if (TaskCarState::PARKING_BRAKE_OFF == task)
    {
        this->crr_car_info.parking_brake_stt = false;
        this->emitParkingBrakeOff();
    }
    else if (TaskCarState::PARKING_BRAKE_ON == task)
    {
        this->crr_car_info.parking_brake_stt = true;
        this->emitParkingBrakeOn();
    }
    else if (TaskCarState::ACCEL_PEDAL_OFF == task)
    {
        this->crr_car_info.accel_pedal_stt = false;
    }
    else if (TaskCarState::ACCEL_PEDAL_ON == task)
    {
        this->crr_car_info.accel_pedal_stt = true;
    }
    else if (TaskCarState::HEDLAMP_OFF == task)
    {
        this->crr_car_info.headlamp_stt = false;
        this->emitHeadlampOff();
    }
    else if (TaskCarState::HEDLAMP_ON == task)
    {
        this->crr_car_info.headlamp_stt = true;
        this->emitHeadlampOn();
    }
    else if (TaskCarState::LIGHTSTATUS_BRAKE_OFF == task)
    {
        this->crr_car_info.lightstatus_brake_stt = false;
        this->emitLightstatusBrakeOff();
    }
    else if (TaskCarState::LIGHTSTATUS_BRAKE_ON == task)
    {
        this->crr_car_info.lightstatus_brake_stt = true;
        this->emitLightstatusBrakeOn();
    }
    else if (TaskCarState::CAR_STOP == task)
    {
        this->crr_car_info.running_stt = false;
        this->emitCarStop();
    }
    else if (TaskCarState::CAR_RUN == task)
    {
        this->crr_car_info.running_stt = true;
        this->emitCarRun();
    }
}

WMError WindowManager::doEndDraw(unsigned req_num)
{
    // get actions
    bool found;
    auto actions = g_app_list.getActions(req_num, &found);
    WMError ret = WMError::SUCCESS;
    if (!found)
    {
        ret = WMError::NO_ENTRY;
        return ret;
    }

    HMI_SEQ_INFO(req_num, "do endDraw");

    // layout change and make it visible
    for (const auto &act : actions)
    {
        if(act.visible != TaskVisible::NO_CHANGE)
        {
            // layout change
            ret = this->lc->layoutChange(act);
            if(ret != WMError::SUCCESS)
            {
                HMI_SEQ_WARNING(req_num,
                    "Failed to manipulate surfaces while state change : %s", errorDescription(ret));
                return ret;
            }
            ret = this->lc->visibilityChange(act);

            // Emit active/deactive event
            string old_role = this->rolenew2old[act.role];
            if(act.visible == TaskVisible::VISIBLE)
            {
                emit_visible(old_role.c_str());
                emit_activated(old_role.c_str());
            }
            else if(act.visible == TaskVisible::REQ_REMOTE_VISIBLE)
            {
                // If this action is for slave, send to slave
                int i_ret = this->wmcon.sendRequest("activated", "", old_role.c_str(), "");
                if (0 > i_ret)
                {
                    ret = WMError::FAIL;
                }
            }
            else if(act.visible == TaskVisible::REQ_REMOTE_INVISIBLE)
            {
                // If this action is for slave, send to slave
                int i_ret = this->wmcon.sendRequest("deactivated", "", old_role.c_str(), "");
                if (0 > i_ret)
                {
                    ret = WMError::FAIL;
                }
            }
            else if((act.visible == TaskVisible::REMOTE_VISIBLE) ||
                    (act.visible == TaskVisible::REMOTE_INVISIBLE))
            {
                // nop because emit active/deactive by event from remote
            }
            else
            {
                emit_invisible(old_role.c_str());
                emit_deactivated(old_role.c_str());
            }

            if (ret != WMError::SUCCESS)
            {
                HMI_SEQ_WARNING(req_num,
                    "Failed to manipulate surfaces while state change : %s", errorDescription(ret));
                return ret;
            }
            HMI_SEQ_DEBUG(req_num, "visible %s", act.role.c_str());
        }
    }
    this->lc->renderLayers();
    this->lc->renderLayersRemote();

    HMI_SEQ_INFO(req_num, "emit flushDraw");

    for(const auto &act_flush : actions)
    {
        // TODO: application requests by old role,
        //       so convert role new to old for emitting event
        string old_role = this->rolenew2old[act_flush.role];

        if(act_flush.visible == TaskVisible::VISIBLE)
        {
            this->emit_flushdraw(old_role.c_str());
        }
        else if(act_flush.visible == TaskVisible::REQ_REMOTE_VISIBLE)
        {
            // If this action is for slave, send to slave
            this->wmcon.sendRequest("flushDraw", "", old_role.c_str(), "");
        }
    }

    return ret;
}

void WindowManager::emitScreenUpdated(unsigned req_num)
{
    // Get visible apps
    HMI_SEQ_DEBUG(req_num, "emit screen updated");
    bool found = false;
    auto actions = g_app_list.getActions(req_num, &found);

    HMI_DEBUG("@@@@@");
    // create json object
    json_object *j = json_object_new_object();
    json_object *jarray = json_object_new_array();

    for(const auto& action: actions)
    {
        if((action.visible == TaskVisible::VISIBLE) ||
           (action.visible == TaskVisible::REMOTE_VISIBLE))
        {
            json_object_array_add(jarray, json_object_new_string(action.client->appID().c_str()));
        }
    }
    json_object_object_add(j, kKeyIds, jarray);
    HMI_SEQ_INFO(req_num, "Visible app: %s", json_object_get_string(j));

    int ret = afb_event_push(
        this->map_afb_event[kListEventName[Event_ScreenUpdated]], j);
    if (ret != 0)
    {
        HMI_DEBUG("afb_event_push failed: %m");
    }
}

void WindowManager::setTimer()
{
    struct timespec ts;
    if (clock_gettime(CLOCK_BOOTTIME, &ts) != 0) {
        HMI_ERROR("Could't set time (clock_gettime() returns with error");
        return;
    }

    HMI_SEQ_DEBUG(g_app_list.currentRequestNumber(), "Timer set activate");
    if (g_timer_ev_src == nullptr)
    {
        // firsttime set into sd_event
        int ret = sd_event_add_time(afb_daemon_get_event_loop(), &g_timer_ev_src,
            CLOCK_BOOTTIME, (uint64_t)(ts.tv_sec + kTimeOut) * 1000000ULL, 1, processTimerHandler, this);
        if (ret < 0)
        {
            HMI_ERROR("Could't set timer");
        }
    }
    else
    {
        // update timer limitation after second time
        sd_event_source_set_time(g_timer_ev_src, (uint64_t)(ts.tv_sec + kTimeOut) * 1000000ULL);
        sd_event_source_set_enabled(g_timer_ev_src, SD_EVENT_ONESHOT);
    }
}

void WindowManager::stopTimer()
{
    unsigned req_num = g_app_list.currentRequestNumber();
    HMI_SEQ_DEBUG(req_num, "Timer stop");
    int rc = sd_event_source_set_enabled(g_timer_ev_src, SD_EVENT_OFF);
    if (rc < 0)
    {
        HMI_SEQ_ERROR(req_num, "Timer stop failed");
    }
}

void WindowManager::processNextRequest()
{
    g_app_list.next();
    g_app_list.reqDump();
    unsigned req_num = g_app_list.currentRequestNumber();
    if (g_app_list.haveRequest())
    {
        HMI_SEQ_DEBUG(req_num, "Process next request");
        WMError rc = checkPolicy(req_num);
        if (rc != WMError::SUCCESS)
        {
            HMI_SEQ_ERROR(req_num, errorDescription(rc));
        }
    }
    else
    {
        HMI_SEQ_DEBUG(req_num, "Nothing Request. Waiting Request");
    }
}

const char* WindowManager::convertRoleOldToNew(char const *old_role)
{
    const char *new_role = nullptr;

    for (auto const &on : this->roleold2new)
    {
        std::regex regex = std::regex(on.first);
        if (std::regex_match(old_role, regex))
        {
            // role is old. So convert to new.
            new_role = on.second.c_str();
            break;
        }
    }

    if (nullptr == new_role)
    {
        // role is new or fallback.
        new_role = old_role;
    }

    HMI_DEBUG("old:%s -> new:%s", old_role, new_role);

    return new_role;
}

int WindowManager::loadOldRolesConfigFile()
{
    // Get afm application installed dir
    char const *afm_app_install_dir = getenv("AFM_APP_INSTALL_DIR");
    HMI_DEBUG("afm_app_install_dir:%s", afm_app_install_dir);

    string file_name;
    if (!afm_app_install_dir)
    {
        HMI_ERROR("AFM_APP_INSTALL_DIR is not defined");
    }
    else
    {
        file_name = string(afm_app_install_dir) + string(kPathOldRolesConfigFile);
    }

    // Load old_rolea config file
    json_object* json_obj;
    int ret = jh::inputJsonFilie(file_name.c_str(), &json_obj);
    if (0 > ret)
    {
        HMI_ERROR("Could not open %s, so use default old_roles information", kPathOldRolesConfigFile);
        json_obj = json_tokener_parse(kDefaultOldRolesConfig);
    }
    HMI_DEBUG("json_obj dump:%s", json_object_get_string(json_obj));

    // Perse apps
    json_object* json_cfg;
    if (!json_object_object_get_ex(json_obj, "old_roles", &json_cfg))
    {
        HMI_ERROR("Parse Error!!");
        return -1;
    }

    int len = json_object_array_length(json_cfg);
    HMI_DEBUG("json_cfg len:%d", len);
    HMI_DEBUG("json_cfg dump:%s", json_object_get_string(json_cfg));

    for (int i=0; i<len; i++)
    {
        json_object* json_tmp = json_object_array_get_idx(json_cfg, i);

        const char* old_role = jh::getStringFromJson(json_tmp, "name");
        if (nullptr == old_role)
        {
            HMI_ERROR("Parse Error!!");
            return -1;
        }

        const char* new_role = jh::getStringFromJson(json_tmp, "new");
        if (nullptr == new_role)
        {
            HMI_ERROR("Parse Error!!");
            return -1;
        }

        this->roleold2new[old_role] = string(new_role);
    }

    // Check
    for(auto itr = this->roleold2new.begin();
      itr != this->roleold2new.end(); ++itr)
    {
        HMI_DEBUG(">>> role old:%s new:%s",
                  itr->first.c_str(), itr->second.c_str());
    }

    // Release json_object
    json_object_put(json_obj);

    return 0;
}

Task WindowManager::convertCanSignalToCarStateTask(const char *signal_name)
{
    wm::LowCanClient *lcc = &(this->lcc);
    Task task = Task::TASK_INVALID;

    // If car info is updated, set car state change event
    if (strstr(signal_name, lcc->kSignalName[lcc->SignalNoParkingBrake]))
    {
        HMI_DEBUG("Parking Brake state is changed");

        if (lcc->getCurrentParkingBrakeState())
        {
            task = wm::Task::TASK_PARKING_BRAKE_ON;
        }
        else
        {
            task = wm::Task::TASK_PARKING_BRAKE_OFF;
        }
    }
    else if (strstr(signal_name, lcc->kSignalName[lcc->SignalNoAccelPedalPos]))
    {
        // Update accel pedal position
        this->crr_car_info.accel_pedal_pos = lcc->getCurrentAccelPedalPosition();

        if (lcc->isChangedAccelPedalState())
        {
            HMI_DEBUG("Accelerator Pedal state is changed");

            if (lcc->getCurrentAccelPedalState())
            {
                task = wm::Task::TASK_ACCEL_PEDAL_ON;
            }
            else
            {
                task = wm::Task::TASK_ACCEL_PEDAL_OFF;
            }
        }
    }
    else if (strstr(signal_name, lcc->kSignalName[lcc->SignalNoHeadlame]))
    {
        HMI_DEBUG("Headlamp state is changed");

        if (lcc->getCurrentHeadlampState())
        {
            task = wm::Task::TASK_HEDLAMP_ON;
        }
        else
        {
            task = wm::Task::TASK_HEDLAMP_OFF;
        }
    }
    else if (strstr(signal_name, lcc->kSignalName[lcc->SignalNoLightstatusBrake]))
    {
        HMI_DEBUG("Lightstatus Brake state is changed");

        if (lcc->getCurrentLightstatusBrakeState())
        {
            task = wm::Task::TASK_LIGHTSTATUS_BRAKE_ON;
        }
        else
        {
            task = wm::Task::TASK_LIGHTSTATUS_BRAKE_OFF;
        }
    }
    return task;
}

void WindowManager::inputCarStateTask(Task task)
{
    unsigned req_num = 0;
    WMError ret = WMError::UNKNOWN;

    ret = this->setRequest(task, &req_num);

    if(ret != WMError::SUCCESS)
    {
        HMI_ERROR(errorDescription(ret));
        return;
    }

    if (req_num != g_app_list.currentRequestNumber())
    {
        // Add request, then invoked after the previous task is finished
        HMI_SEQ_DEBUG(req_num, "request is accepted");
        return;
    }

    /*
     * Do allocate tasks
     */
    ret = this->checkPolicy(req_num);

    if (ret != WMError::SUCCESS)
    {
        //this->emit_error()
        HMI_SEQ_ERROR(req_num, errorDescription(ret));
        g_app_list.removeRequest(req_num);
        this->processNextRequest();
    }
}

const char *WindowManager::check_surface_exist(const char *drawing_name)
{
    auto const &surface_id = this->id_alloc.lookup(string(drawing_name));
    if (!surface_id)
    {
        return "Surface does not exist";
    }

    /* if (!this->controller->surface_exists(*surface_id))
    {
        return "Surface does not exist in controller!";
    } */

    /* auto layer_id = this->layers.get_layer_id(*surface_id);

    if (!layer_id)
    {
        return "Surface is not on any layer!";
    } */

    HMI_DEBUG("surface %d is detected", *surface_id);
    return nullptr;
}

const char* WindowManager::kDefaultOldRolesConfig = "{ \
    \"old_roles\": [ \
        { \
            \"name\": \"HomeScreen\", \
            \"new\": \"homescreen\" \
        }, \
        { \
            \"name\": \"Music\", \
            \"new\": \"music\" \
        }, \
        { \
            \"name\": \"MediaPlayer\", \
            \"new\": \"music\" \
        }, \
        { \
            \"name\": \"Video\", \
            \"new\": \"video\" \
        }, \
        { \
            \"name\": \"VideoPlayer\", \
            \"new\": \"video\" \
        }, \
        { \
            \"name\": \"WebBrowser\", \
            \"new\": \"browser\" \
        }, \
        { \
            \"name\": \"Radio\", \
            \"new\": \"radio\" \
        }, \
        { \
            \"name\": \"Phone\", \
            \"new\": \"phone\" \
        }, \
        { \
            \"name\": \"Navigation\", \
            \"new\": \"map\" \
        }, \
        { \
            \"name\": \"HVAC\", \
            \"new\": \"hvac\" \
        }, \
        { \
            \"name\": \"Settings\", \
            \"new\": \"settings\" \
        }, \
        { \
            \"name\": \"Dashboard\", \
            \"new\": \"dashboard\" \
        }, \
        { \
            \"name\": \"POI\", \
            \"new\": \"poi\" \
        }, \
        { \
            \"name\": \"Mixer\", \
            \"new\": \"mixer\" \
        }, \
        { \
            \"name\": \"Restriction\", \
            \"new\": \"restriction\" \
        }, \
        { \
            \"name\": \"^OnScreen.*\", \
            \"new\": \"on_screen\" \
        } \
    ] \
}";

} // namespace wm

/*
 * Insert Copyright if needed.
 */

#ifndef WM_CONNECTION_HPP
#define WM_CONNECTION_HPP

#include <functional>

struct json_object;

namespace wm
{

class WMConnection
{
  public:
    WMConnection();
    ~WMConnection() = default;

    using ReceivedHandler = std::function<void(json_object* j_out)>;

    int initialize();
    void registerCallback(ReceivedHandler on_received);
    int sendRequest(char const *req, char const *appid,
                    char const *drawing_name, char const *drawing_area);
    bool isMasterMode();
    bool isMasterArea(const char* area);
    bool isConnecting();
    std::string parseMasterArea(const char* area);
    bool isSyncDrawingForRemote(const char* role);
    void startSyncDrawForRemote(const char* role);
    void finishSyncDrawForRemote(const char* role);
    int getMySocket();
    int getConnectedSocket();
    void setConnectedSocket(int connected_socket);
    std::string getEcuName();
    void callOnReceivedHandler(json_object *j_out);
    int connectToMaster();

    int receive(json_object** j_out);

  private:
    std::string mode;
    std::string ip;
    int port;
    int my_socket = -1;
    int connected_socket = -1;
    ReceivedHandler onReceived;
    std::string syndDrawingAppId;

    std::string ecu_name;

    int initializeMaster();
    int initializeSlave();
    int loadConnectionConfigFile();

    int send(json_object* j_in);
};

} // namespace wm

#endif // WM_CONNECTION_HPP


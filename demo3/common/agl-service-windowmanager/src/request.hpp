/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef WMREQUEST_HPP
#define WMREQUEST_HPP

#include <string>
#include <vector>
#include <memory>

namespace wm
{

class WMClient;

enum Task
{
    TASK_ALLOCATE,
    TASK_RELEASE,
    TASK_PARKING_BRAKE_OFF,
    TASK_PARKING_BRAKE_ON,
    TASK_ACCEL_PEDAL_OFF,
    TASK_ACCEL_PEDAL_ON,
    TASK_HEDLAMP_OFF,
    TASK_HEDLAMP_ON,
    TASK_LIGHTSTATUS_BRAKE_OFF,
    TASK_LIGHTSTATUS_BRAKE_ON,
    TASK_RESTRICTION_MODE_OFF,
    TASK_RESTRICTION_MODE_ON,
    TASK_INVALID
};

enum TaskVisible
{
    VISIBLE,
    INVISIBLE,
    REQ_REMOTE_VISIBLE,
    REQ_REMOTE_INVISIBLE,
    REMOTE_VISIBLE,
    REMOTE_INVISIBLE,
    NO_CHANGE
};

enum TaskCarState
{
    PARKING_BRAKE_OFF,
    PARKING_BRAKE_ON,
    ACCEL_PEDAL_OFF,
    ACCEL_PEDAL_ON,
    HEDLAMP_OFF,
    HEDLAMP_ON,
    LIGHTSTATUS_BRAKE_OFF,
    LIGHTSTATUS_BRAKE_ON,
    CAR_STOP,
    CAR_RUN,
    RESTRICTION_MODE_OFF,
    RESTRICTION_MODE_ON,
    NO_TASK,
};

struct WMTrigger
{
    std::string appid;
    std::string role;
    std::string area;
    Task task;
};

struct WMAction
{
    unsigned req_num;
    std::shared_ptr<WMClient> client;
    std::string role;
    std::string area;
    TaskVisible visible;
    bool end_draw_finished;
    TaskCarState car_state;
};

struct WMRequest
{
    WMRequest();
    explicit WMRequest(std::string appid, std::string role,
                       std::string area, Task task);
    explicit WMRequest(Task task);
    virtual ~WMRequest();
    WMRequest(const WMRequest &obj);

    unsigned req_num;
    struct WMTrigger trigger;
    std::vector<struct WMAction> sync_draw_req;
};

} // namespace wm

#endif //WMREQUEST_HPP

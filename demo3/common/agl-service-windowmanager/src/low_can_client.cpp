/*
 * Copyright (c) 2018 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "low_can_client.hpp"
#include "json_helper.hpp"
#include "util.hpp"

extern "C"
{
#include <afb/afb-binding.h>
}

namespace wm
{

LowCanClient::LowCanClient()
    : vehicle_speed(0),
      trans_gear_pos(0),
      headlamp_status(FALSE),
      parking_brake_status(TRUE),
      accel_pedal_pos(0),
      accel_pedal_stt(FALSE),
      lightstatus_brake_status(TRUE),
      is_changed_accel_pedal_stt(false)
{
}

void LowCanClient::initialize()
{
    int ret;

    // Require API "low-can"
    ret = afb_daemon_require_api_v2("low-can", 1);
    if (0 > ret)
    {
        HMI_INFO("Requirement API \"low-can\" failed");
        return;
    }

    // Subscribe low-level-can
    // low-can subscribe { "event": "vehicle.speed" }
    // low-can subscribe { "event": "transmission_gear_position" }
    // low-can subscribe { "event": "headlamp_status" }
    // low-can subscribe { "event": "parking_brake_status" }
    // low-can subscribe { "event": "accelerator.pedal.position" }
    // low-can subscribe { "event": "lightstatus.brake" }
    for (int i = SignalNoMin; i <= SignalNoMax; i++)
    {
        // Set Event
        json_object *json_obj = json_object_new_object();
        json_object_object_add(json_obj, "event",
                               json_object_new_string(this->kSignalName[i]));

        // Set filter
        if (0 != strcmp("", this->kFilterValue[i]))
        {
            json_object_object_add(json_obj, "filter",
                                   json_tokener_parse(this->kFilterValue[i]));
        }
        HMI_DEBUG("subscribe message:%s", json_object_get_string(json_obj));

        // Subscribe
        afb_service_call("low-can", "subscribe", json_obj,
                         [](void *closure, int status, json_object *result) {
                             HMI_DEBUG("subscribe result:%s", json_object_get_string(result));
                         },
                         nullptr);
    }

    return;
}

const char *LowCanClient::analyzeCanSignal(struct json_object *object)
{
    HMI_DEBUG("object:%s", json_object_get_string(object));

    const char *name = jh::getStringFromJson(object, "name");
    HMI_DEBUG("CAN signal name:%s", name);

    if (strstr(name, this->kSignalName[SignalNoVehicliSpeed]))
    {
        // Update vehicle speed
        this->vehicle_speed = jh::getIntFromJson(object, "value");
        HMI_DEBUG("Update vehicle speed:%d", this->vehicle_speed);
    }
    else if (strstr(name, this->kSignalName[SignalNoTransGearPos]))
    {
        // Update transmission gear position
        this->trans_gear_pos = jh::getIntFromJson(object, "value");
        HMI_DEBUG("Update transmission gear position:%d", this->trans_gear_pos);
    }
    else if (strstr(name, this->kSignalName[SignalNoHeadlame]))
    {
        // Update headlamp status
        this->headlamp_status = jh::getBoolFromJson(object, "value");
        HMI_DEBUG("Update headlamp status:%d", this->headlamp_status);
    }
    else if (strstr(name, this->kSignalName[SignalNoParkingBrake]))
    {
        // Update parking gear status
        this->parking_brake_status = jh::getBoolFromJson(object, "value");
        HMI_DEBUG("Update parking brake status:%d", this->parking_brake_status);
    }
    else if (strstr(name, this->kSignalName[SignalNoAccelPedalPos]))
    {
        // Clear flag for whether accel pedal state is changed
        this->is_changed_accel_pedal_stt = false;

        // Update accelerator pedal status
        this->accel_pedal_pos = jh::getDoubleFromJson(object, "value");
        HMI_DEBUG("Update accelerator pedal position:%lf", this->accel_pedal_pos);

        bool accel_pedal_stt;
        if (0 != this->accel_pedal_pos)
        {
            accel_pedal_stt = true;
        }
        else
        {
            accel_pedal_stt = false;
        }

        if (accel_pedal_stt != this->accel_pedal_stt)
        {
            this->is_changed_accel_pedal_stt = true;
            this->accel_pedal_stt = accel_pedal_stt;
        }
    }
    else if (strstr(name, this->kSignalName[SignalNoLightstatusBrake]))
    {
        // Update lightstatus brake status
        this->lightstatus_brake_status = jh::getBoolFromJson(object, "value");
        HMI_DEBUG("Update lightstatus brake status:%d", this->lightstatus_brake_status);
    }

    return name;
}

bool LowCanClient::isChangedAccelPedalState()
{
    return this->is_changed_accel_pedal_stt;
}

int LowCanClient::getCurrentTransGearState()
{
    return this->trans_gear_pos;
}

bool LowCanClient::getCurrentHeadlampState()
{
    return (bool)this->headlamp_status;
}

bool LowCanClient::getCurrentParkingBrakeState()
{
    return (bool)this->parking_brake_status;
}

double LowCanClient::getCurrentAccelPedalPosition()
{
    return this->accel_pedal_pos;
}

bool LowCanClient::getCurrentAccelPedalState()
{
    return this->accel_pedal_stt;
}

bool LowCanClient::getCurrentLightstatusBrakeState()
{
    return (bool)this->lightstatus_brake_status;
}

} // namespace wm

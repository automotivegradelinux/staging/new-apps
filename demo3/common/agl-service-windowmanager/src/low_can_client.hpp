/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TMCAGLWM_LOW_CAN_CLIENT_HPP
#define TMCAGLWM_LOW_CAN_CLIENT_HPP

#include <string>
#include <vector>
#include <json-c/json.h>

namespace wm
{

class LowCanClient
{

  public:
    explicit LowCanClient();
    ~LowCanClient() = default;

    enum SignalNo
    {
        SignalNoVehicliSpeed = 0,
        SignalNoTransGearPos,
        SignalNoHeadlame,
        SignalNoParkingBrake,
        SignalNoAccelPedalPos,
        SignalNoLightstatusBrake,

        SignalNum,

        SignalNoMin = SignalNoVehicliSpeed,
        SignalNoMax = SignalNum - 1,
    };

    const std::vector<const char *> kSignalName{
        "vehicle.speed",
        "transmission_gear_position",
        "headlamp_status",
        "parking_brake_status",
        "accelerator.pedal.position",
        "lightstatus.brake",
    };

    void initialize();
    const char *analyzeCanSignal(struct json_object *object);

    int getCurrentTransGearState();
    bool getCurrentHeadlampState();
    bool getCurrentParkingBrakeState();
    double getCurrentAccelPedalPosition();
    bool getCurrentAccelPedalState();
    bool getCurrentLightstatusBrakeState();

    bool isChangedAccelPedalState();

  private:
    // Disable copy and move
    LowCanClient(LowCanClient const &) = delete;
    LowCanClient &operator=(LowCanClient const &) = delete;
    LowCanClient(LowCanClient &&) = delete;
    LowCanClient &operator=(LowCanClient &&) = delete;

    enum TransGearPosVal
    {
        TransGearPosValD1 = 1,
        TransGearPosValD2,
        TransGearPosValD3,
        TransGearPosValD4,
        TransGearPosValD5,
        TransGearPosValD6,
        TransGearPosValD7,
        TransGearPosValD8,
        TransGearPosValR,
        TransGearPosValN,
    };

    const std::vector<const char *> kFilterValue{
        "", // vehicle.speed
        "", // transmission_gear_position
        "", // headlamp_status
        "", // parking_brake_status
        "", // accelerator.pedal.position
        "", // lightstatus.brake
    };

    int vehicle_speed;
    int trans_gear_pos;
    json_bool headlamp_status;
    json_bool parking_brake_status;
    double accel_pedal_pos;
    bool accel_pedal_stt;
    json_bool lightstatus_brake_status;

    bool is_changed_accel_pedal_stt;
};

} // namespace wm

#endif // TMCAGLWM_LOW_CAN_CLIENT_HPP

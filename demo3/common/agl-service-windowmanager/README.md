This is a WindowManager implementation for the AGL Project.
===========================================================

See http://docs.automotivelinux.org/docs/apis_services/en/dev/

**NOTE**
This Window Manager is master (meter cluster) mode by default.
If use slave (IVI) mode, please execute sclipt `change_to_slave_mode.sh` on target as follows:
```
$ scp ./change_to_slave_mode.sh root@<target_ip_addr>:~
$ ssh root@<target_ip_addr>
# sync
# ./change_to_slave_mode.sh
```

/*
 * Copyright (c) 2018 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include "stm.h"
#include "stm_inner.h"

const char* kStmEventName[] = {
    "none",
    "activate",
    "deactivate",
    "trans_gear_neutral",
    "trans_gear_not_neutral",
    "parking_brake_off",
    "parking_brake_on",
    "accel_pedal_off",
    "accel_pedal_on",
    "lamp_off",
    "lamp_on",
    "lightstatus_brake_off",
    "lightstatus_brake_on",
    "restriction_mode_off",
    "restriction_mode_on",
    "undo",
};

const char* kStmCategoryName[] = {
    "none",
    "homescreen",
    "map",
    "general",
    "splitable",
    "splitable_main",
    "splitable_sub",
    "pop_up",
    "system_alert",
    "restriction",
    "system",
    "software_keyboard",
    "tbt",
    "meter",
    "receiver",
    "debug",
};

const char* kStmAreaName[] = {
    "none",
    "fullscreen",
    "normal.full",
    "split.main",
    "split.sub",
    "on_screen",
    "restriction.normal",
    "restriction.split.main",
    "restriction.split.sub",
    "software_keyboard",
    "master.split.sub",
};

const char* kStmLayoutName[] = {
    "none",
    "pop_up",
    "system_alert",
    "map.normal",
    "map.split",
    "map.fullscreen",
    "splitable.normal",
    "splitable.split",
    "general.normal",
    "homescreen",
    "restriction.normal",
    "restriction.split.main",
    "restriction.split.sub",
    "system.normal",
    "software_keyboard",
    "tbt",
    "remote_tbt",
    "meter.normal",
    "meter_receiver",
    "meter_splitable",
    "splitable_receiver",
    "receiver.split",
    "debug.normal",
    "debug.split.main",
    "debug.split.sub",
    "debug.fullscreen",
};

const char* kStmLayerName[] = {
    "homescreen",
    "apps",
    "near_homescreen",
    "restriction",
    "on_screen",
    "remote",
};

const char* kStmCarElementName[] = {
    "trans_gear",
    "parking_brake",
    "accel_pedal",
    "running",
    "lamp",
    "lightstatus_brake",
    "restriction_mode",
};

const char* kStmTransGearStateName[] = {
    "neutral",
    "not_neutral"
};

const char* kStmParkingBrakeStateName[] = {
    "off",
    "on"
};

const char* kStmAccelPedalStateName[] = {
    "off",
    "on"
};

const char* kStmRunningSttNo2Name[] = {
    "stop",
    "run"
};

const char* kStmLampStateName[] = {
    "off",
    "on"
};

const char* kStmLightstatusBrakeStateName[] = {
    "off",
    "on"
};

const char* kStmRestrictionModeStateName[] = {
    "off",
    "on",
};

const char** kStmCarElementStateNameList[] = {
    kStmTransGearStateName,
    kStmParkingBrakeStateName,
    kStmAccelPedalStateName,
    kStmRunningSttNo2Name,
    kStmLampStateName,
    kStmLightstatusBrakeStateName,
    kStmRestrictionModeStateName,
};

void stmInitialize(const char *ecu_name) {
    stmInitializeInner(ecu_name);
}

int stmTransitionState(int event, StmState* state) {
    return stmTransitionStateInner(event, state);
}

void stmUndoState() {
    stmUndoStateInner();
}

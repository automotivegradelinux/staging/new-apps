#include "./StateTransitionor/ZST_include.h"

void stm_mst_rmt_start_activity_none() {
    g_stm_crr_state.layer[StmLayerNoRemote].state = StmLayoutNoNone;
    g_stm_crr_state.layer[StmLayerNoRemote].changed = STM_TRUE;
}

void stm_mst_rmt_start_activity_tbt() {
    g_stm_crr_state.layer[StmLayerNoRemote].state = StmLayoutNoRmtTbt;
    g_stm_crr_state.layer[StmLayerNoRemote].changed = STM_TRUE;
}

void stm_mst_rmt_initialize_valiable() {
	g_stm_prv_state.layer[StmLayerNoRemote].state = StmLayoutNoNone;
	g_stm_prv_state.layer[StmLayerNoRemote].changed = STM_FALSE;

	g_stm_crr_state.layer[StmLayerNoRemote].state = StmLayoutNoNone;
	g_stm_crr_state.layer[StmLayerNoRemote].changed = STM_FALSE;
}

void stm_mst_rmt_start_stm() {
	if (g_stm_event == StmEvtNoUndo) {
		// nop
	}
	else {
		g_stm_prv_state.layer[StmLayerNoRemote].state = g_stm_crr_state.layer[StmLayerNoRemote].state;
	}
	g_stm_crr_state.layer[StmLayerNoRemote].changed = STM_FALSE;
}

/************************************************************/
/*     ZACCEL_AccelPedalState_func.c                        */
/*     Function and variable source file                    */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../ZST_include.h"

/*************************************************************
    Function definition
*************************************************************/

/*
 * @name stm_aps_start_activity_accel_pedal_off
 */
void stm_aps_start_activity_accel_pedal_off() {
    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state = StmAccelPedalSttNoOff;
    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].changed = STM_TRUE;
}

/*
 * @name stm_aps_start_activity_accel_pedal_on
 */
void stm_aps_start_activity_accel_pedal_on() {
    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state = StmAccelPedalSttNoOn;
    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].changed = STM_TRUE;
}

/*
 * @name stm_aps_initialize_variable
 */
void stm_aps_initialize_variable() {
    g_stm_prv_state.car_element[StmCarElementNoAccelPedal].state = StmAccelPedalSttNoOff;
    g_stm_prv_state.car_element[StmCarElementNoAccelPedal].changed = STM_FALSE;

    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state = StmAccelPedalSttNoOff;
    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].changed = STM_FALSE;
}

/*
 * @name stm_aps_start_stm
 */
void stm_aps_start_stm() {
	g_stm_prv_state.car_element[StmCarElementNoAccelPedal].state = g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state;
    g_stm_crr_state.car_element[StmCarElementNoAccelPedal].changed = STM_FALSE;
}

/************************************************************/
/*     ZACCEL_AccelPedal.h                                  */
/*     AccelPedal State transition model header file        */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZACCEL_ACCELPEDAL_H
#define ZHEADER_ZACCEL_ACCELPEDAL_H

/*State management variable access define*/
#define ZACCEL_ACCELPEDAL ( 0U )
#define ZACCEL_ACCELPEDALS0 ( 0U )
#define ZACCEL_ACCELPEDALS1 ( 1U )
#define ZACCEL_ACCELPEDALSTATENOMAX ( 1U )

/*End state define*/
#define ZACCEL_ACCELPEDALEND ( 2U )
/*Terminate state define*/
#define ZACCEL_ACCELPEDALTERMINATE ( ZACCEL_ACCELPEDALEND + 1U )

/*State no define*/
#define ZACCEL_ACCELPEDALS0STATENO ( 0U )
#define ZACCEL_ACCELPEDALS1STATENO ( 1U )

/*State serial no define*/
#define ZACCEL_ACCELPEDALS0STATESERIALNO ( 0U )
#define ZACCEL_ACCELPEDALS1STATESERIALNO ( 1U )

/*Event no define*/
#define ZACCEL_ACCELPEDALE0EVENTNO ( 0U )
#define ZACCEL_ACCELPEDALE1EVENTNO ( 1U )

/*Event serial no define*/
#define ZACCEL_ACCELPEDALE0EVENTSERIALNO ( 0U )
#define ZACCEL_ACCELPEDALE1EVENTSERIALNO ( 1U )

/*Extern function*/
extern void stm_aps_event_call( void );
extern void stm_aps_initialize( void );
extern void ZACCEL_AccelPedalTerminate( void );

#endif

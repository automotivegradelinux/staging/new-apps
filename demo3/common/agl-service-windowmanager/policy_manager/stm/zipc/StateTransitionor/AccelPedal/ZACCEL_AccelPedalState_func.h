/************************************************************/
/*     ZACCEL_AccelPedalState_func.h                        */
/*     Function and variable header file                    */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZACCEL_ACCELPEDALSTATE_FUNC_H
#define ZHEADER_ZACCEL_ACCELPEDALSTATE_FUNC_H

extern void stm_aps_start_activity_accel_pedal_off();
extern void stm_aps_start_activity_accel_pedal_on();
extern void stm_aps_initialize_variable();
extern void stm_aps_start_stm();

#endif

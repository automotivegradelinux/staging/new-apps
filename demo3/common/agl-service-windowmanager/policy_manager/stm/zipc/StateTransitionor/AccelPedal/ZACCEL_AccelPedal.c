/************************************************************/
/*     ZACCEL_AccelPedal.c                                  */
/*     AccelPedal State transition model source file        */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../ZST_include.h"

/* State management variable */
static uint8_t ZACCEL_AccelPedalState[ZACCEL_ACCELPEDALSTATENOMAX];

static void ZACCEL_AccelPedals0e1( void );
static void ZACCEL_AccelPedals1e0( void );
static void ZACCEL_AccelPedals0Event( void );
static void ZACCEL_AccelPedals1Event( void );

/****************************************/
/* Action function                      */
/*   STM : AccelPedal                   */
/*   State : accel_pedal_off( No 0 )    */
/*   Event : evt_accel_pedal_on( No 1 ) */
/****************************************/
static void ZACCEL_AccelPedals0e1( void )
{
    ZACCEL_AccelPedalState[ZACCEL_ACCELPEDAL] = ( uint8_t )ZACCEL_ACCELPEDALS1;
    stm_aps_start_activity_accel_pedal_on();
}

/****************************************/
/* Action function                      */
/*   STM : AccelPedal                   */
/*   State : accel_pedal_on( No 1 )     */
/*   Event : evt_accel_pedal_off( No 0 )*/
/****************************************/
static void ZACCEL_AccelPedals1e0( void )
{
    ZACCEL_AccelPedalState[ZACCEL_ACCELPEDAL] = ( uint8_t )ZACCEL_ACCELPEDALS0;
    stm_aps_start_activity_accel_pedal_off();
}

/****************************************/
/* Event appraisal function             */
/*   STM : AccelPedal                   */
/*   State : accel_pedal_off( No 0 )    */
/****************************************/
static void ZACCEL_AccelPedals0Event( void )
{
    /*evt_accel_pedal_on*/
    if( g_stm_event == StmEvtNoAccelPedalOn )
    {
        ZACCEL_AccelPedals0e1();
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : AccelPedal                   */
/*   State : accel_pedal_on( No 1 )     */
/****************************************/
static void ZACCEL_AccelPedals1Event( void )
{
    /*evt_accel_pedal_off*/
    if( g_stm_event == StmEvtNoAccelPedalOff )
    {
        ZACCEL_AccelPedals1e0();
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event call function                  */
/*   STM : AccelPedal                   */
/****************************************/
void stm_aps_event_call( void )
{
    stm_aps_start_stm();
    switch( ZACCEL_AccelPedalState[ZACCEL_ACCELPEDAL] )
    {
    case ZACCEL_ACCELPEDALS0:
        ZACCEL_AccelPedals0Event();
        break;
    case ZACCEL_ACCELPEDALS1:
        ZACCEL_AccelPedals1Event();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* Initial function                     */
/*   STM : AccelPedal                   */
/****************************************/
void stm_aps_initialize( void )
{
    ZACCEL_AccelPedalState[ZACCEL_ACCELPEDAL] = ( uint8_t )ZACCEL_ACCELPEDALS0;
    stm_aps_start_activity_accel_pedal_off();
}

/****************************************/
/* Terminate function                   */
/*   STM : AccelPedal                   */
/****************************************/
void ZACCEL_AccelPedalTerminate( void )
{
    ZACCEL_AccelPedalState[ZACCEL_ACCELPEDAL] = ( uint8_t )ZACCEL_ACCELPEDALTERMINATE;
}


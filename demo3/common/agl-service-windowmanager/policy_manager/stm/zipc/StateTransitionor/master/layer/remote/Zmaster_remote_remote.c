/************************************************************/
/*     Zmaster_remote_remote.c                              */
/*     remote State transition model source file            */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../../../ZST_include.h"

/* State management variable */
static uint8_t Zmaster_remote_remoteState[ZMASTER_REMOTE_REMOTESTATENOMAX];

static void Zmaster_remote_remotes0e0( void );
static void Zmaster_remote_remotes0e2( void );
static void Zmaster_remote_remotes1e1( void );
static void Zmaster_remote_remotes1e3( void );
static void Zmaster_remote_remotes0Event( void );
static void Zmaster_remote_remotes1Event( void );

/****************************************/
/* Action function                      */
/*   STM : remote                       */
/*   State : none( No 0 )               */
/*   Event : ara_master_split_sub( No 0 ) */
/****************************************/
static void Zmaster_remote_remotes0e0( void )
{
    Zmaster_remote_remoteState[ZMASTER_REMOTE_REMOTE] = ( uint8_t )ZMASTER_REMOTE_REMOTES1;
    stm_mst_rmt_start_activity_tbt();
}

/****************************************/
/* Action function                      */
/*   STM : remote                       */
/*   State : none( No 0 )               */
/*   Event : stt_prv_layer_remote_none( No 2 ) */
/****************************************/
static void Zmaster_remote_remotes0e2( void )
{
    stm_mst_rmt_start_activity_none();
}

/****************************************/
/* Action function                      */
/*   STM : remote                       */
/*   State : tbt( No 1 )                */
/*   Event : ctg_tbt( No 1 )            */
/****************************************/
static void Zmaster_remote_remotes1e1( void )
{
    Zmaster_remote_remoteState[ZMASTER_REMOTE_REMOTE] = ( uint8_t )ZMASTER_REMOTE_REMOTES0;
    stm_mst_rmt_start_activity_none();
}

/****************************************/
/* Action function                      */
/*   STM : remote                       */
/*   State : tbt( No 1 )                */
/*   Event : stt_prv_layer_remote_tbt( No 3 ) */
/****************************************/
static void Zmaster_remote_remotes1e3( void )
{
    stm_mst_rmt_start_activity_tbt();
}

/****************************************/
/* Event appraisal function             */
/*   STM : remote                       */
/*   State : none( No 0 )               */
/****************************************/
static void Zmaster_remote_remotes0Event( void )
{
    /*evt_activate*/
    if( g_stm_event == StmEvtNoActivate )
    {
        /*ctg_tbt*/
        if( g_stm_category == StmCtgNoTbt )
        {
            /*ara_master_split_sub*/
            if( g_stm_area == StmAreaNoMasterSplitSub )
            {
                Zmaster_remote_remotes0e0();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    /*evt_undo*/
    else if( g_stm_event == StmEvtNoUndo )
    {
        /*stt_prv_layer_remote_none*/
        if( g_stm_prv_state.layer[StmLayerNoRemote].state == StmLayoutNoNone )
        {
            Zmaster_remote_remotes0e2();
        }
        /*stt_prv_layer_remote_tbt*/
        else if( g_stm_prv_state.layer[StmLayerNoRemote].state == StmLayoutNoRmtTbt )
        {
            Zmaster_remote_remotes0e0();
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : remote                       */
/*   State : tbt( No 1 )                */
/****************************************/
static void Zmaster_remote_remotes1Event( void )
{
    /*evt_deactivate*/
    if( g_stm_event == StmEvtNoDeactivate )
    {
        /*ctg_tbt*/
        if( g_stm_category == StmCtgNoTbt )
        {
            Zmaster_remote_remotes1e1();
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    /*evt_undo*/
    else if( g_stm_event == StmEvtNoUndo )
    {
        /*stt_prv_layer_remote_none*/
        if( g_stm_prv_state.layer[StmLayerNoRemote].state == StmLayoutNoNone )
        {
            Zmaster_remote_remotes1e1();
        }
        /*stt_prv_layer_remote_tbt*/
        else if( g_stm_prv_state.layer[StmLayerNoRemote].state == StmLayoutNoRmtTbt )
        {
            Zmaster_remote_remotes1e3();
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event call function                  */
/*   STM : remote                       */
/****************************************/
void stm_mst_rmt_event_call( void )
{
    stm_mst_rmt_start_stm();
    switch( Zmaster_remote_remoteState[ZMASTER_REMOTE_REMOTE] )
    {
    case ZMASTER_REMOTE_REMOTES0:
        Zmaster_remote_remotes0Event();
        break;
    case ZMASTER_REMOTE_REMOTES1:
        Zmaster_remote_remotes1Event();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* Initial function                     */
/*   STM : remote                       */
/****************************************/
void stm_mst_rmt_initialize( void )
{
    Zmaster_remote_remoteState[ZMASTER_REMOTE_REMOTE] = ( uint8_t )ZMASTER_REMOTE_REMOTES0;
    stm_mst_rmt_start_activity_none();
}

/****************************************/
/* Terminate function                   */
/*   STM : remote                       */
/****************************************/
void Zmaster_remote_remoteTerminate( void )
{
    Zmaster_remote_remoteState[ZMASTER_REMOTE_REMOTE] = ( uint8_t )ZMASTER_REMOTE_REMOTETERMINATE;
}


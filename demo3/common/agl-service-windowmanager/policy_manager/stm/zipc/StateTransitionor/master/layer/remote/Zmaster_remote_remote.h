/************************************************************/
/*     Zmaster_remote_remote.h                              */
/*     remote State transition model header file            */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZMASTER_REMOTE_REMOTE_H
#define ZHEADER_ZMASTER_REMOTE_REMOTE_H

/*State management variable access define*/
#define ZMASTER_REMOTE_REMOTE ( 0U )
#define ZMASTER_REMOTE_REMOTES0 ( 0U )
#define ZMASTER_REMOTE_REMOTES1 ( 1U )
#define ZMASTER_REMOTE_REMOTESTATENOMAX ( 1U )

/*End state define*/
#define ZMASTER_REMOTE_REMOTEEND ( 2U )
/*Terminate state define*/
#define ZMASTER_REMOTE_REMOTETERMINATE ( ZMASTER_REMOTE_REMOTEEND + 1U )

/*State no define*/
#define ZMASTER_REMOTE_REMOTES0STATENO ( 0U )
#define ZMASTER_REMOTE_REMOTES1STATENO ( 1U )

/*State serial no define*/
#define ZMASTER_REMOTE_REMOTES0STATESERIALNO ( 0U )
#define ZMASTER_REMOTE_REMOTES1STATESERIALNO ( 1U )

/*Event no define*/
#define ZMASTER_REMOTE_REMOTEE0EVENTNO ( 0U )
#define ZMASTER_REMOTE_REMOTEE1EVENTNO ( 1U )
#define ZMASTER_REMOTE_REMOTEE2EVENTNO ( 2U )
#define ZMASTER_REMOTE_REMOTEE3EVENTNO ( 3U )

/*Event serial no define*/
#define ZMASTER_REMOTE_REMOTEE0EVENTSERIALNO ( 0U )
#define ZMASTER_REMOTE_REMOTEE1EVENTSERIALNO ( 1U )
#define ZMASTER_REMOTE_REMOTEE2EVENTSERIALNO ( 2U )
#define ZMASTER_REMOTE_REMOTEE3EVENTSERIALNO ( 3U )

/*Extern function*/
extern void stm_mst_rmt_event_call( void );
extern void stm_mst_rmt_initialize( void );
extern void Zmaster_remote_remoteTerminate( void );

#endif

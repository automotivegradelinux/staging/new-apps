/************************************************************/
/*     Zmaster_apps_apps_main.c                             */
/*     apps_main State transition model source file         */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../../../ZST_include.h"

/* State management variable */
static uint8_t Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINSTATENOMAX];

static void Zmaster_apps_apps_mains0StateEntry( void );
static void Zmaster_apps_apps_mains1StateEntry( void );
static void Zmaster_apps_apps_mains0e1( void );
static void Zmaster_apps_apps_mains1e0( void );
static void Zmaster_apps_apps_car_stops0e0( void );
static void Zmaster_apps_apps_car_stops0e2( void );
static void Zmaster_apps_apps_car_stops0e3( void );
static void Zmaster_apps_apps_car_stops0e4( void );
static void Zmaster_apps_apps_car_stops0e11( void );
static void Zmaster_apps_apps_car_stops0e15( void );
static void Zmaster_apps_apps_car_runs0e0( void );
static void Zmaster_apps_apps_car_runs0e3( void );
static void Zmaster_apps_apps_mains0Event( void );
static void Zmaster_apps_apps_car_stops0Event( void );
static void Zmaster_apps_apps_car_stops1Event( void );
static void Zmaster_apps_apps_car_stops2Event( void );
static void Zmaster_apps_apps_car_stops3Event( void );
static void Zmaster_apps_apps_car_stops4Event( void );
static void Zmaster_apps_apps_car_stops5Event( void );
static void Zmaster_apps_apps_mains1Event( void );
static void Zmaster_apps_apps_car_runs0Event( void );
static void Zmaster_apps_apps_car_runs1Event( void );

/****************************************/
/* State start activity function        */
/*   STM : apps_main                    */
/*   State : lightstatus_brake_on( No 0 ) */
/****************************************/
static void Zmaster_apps_apps_mains0StateEntry( void )
{
    switch( Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] )
    {
    case ZMASTER_APPS_APPS_CAR_STOPS0:
        stm_mst_apl_start_activity_none();
        break;
    case ZMASTER_APPS_APPS_CAR_STOPS1:
        stm_mst_apl_start_activity_meter_receiver();
        break;
    case ZMASTER_APPS_APPS_CAR_STOPS2:
        stm_mst_apl_start_activity_meter_splitable();
        break;
    case ZMASTER_APPS_APPS_CAR_STOPS3:
        stm_mst_apl_start_activity_splitable_receiver();
        break;
    case ZMASTER_APPS_APPS_CAR_STOPS4:
        stm_mst_apl_start_activity_splitable_split();
        break;
    case ZMASTER_APPS_APPS_CAR_STOPS5:
        stm_mst_apl_start_activity_general();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* State start activity function        */
/*   STM : apps_main                    */
/*   State : lightstatus_brake_off( No 1 ) */
/****************************************/
static void Zmaster_apps_apps_mains1StateEntry( void )
{
    switch( Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS1F] )
    {
    case ZMASTER_APPS_APPS_CAR_RUNS0:
        stm_mst_apl_start_activity_meter_receiver();
        break;
    case ZMASTER_APPS_APPS_CAR_RUNS1:
        stm_mst_apl_start_activity_meter();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* Action function                      */
/*   STM : apps_main                    */
/*   State : lightstatus_brake_on( No 0 ) */
/*   Event : stt_lightstatus_brake_off( No 1 ) */
/****************************************/
static void Zmaster_apps_apps_mains0e1( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAIN] = ( uint8_t )ZMASTER_APPS_APPS_MAINS1;
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS1F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_RUNS0;
    Zmaster_apps_apps_mains1StateEntry();
}

/****************************************/
/* Action function                      */
/*   STM : apps_main                    */
/*   State : lightstatus_brake_off( No 1 ) */
/*   Event : stt_lightstatus_brake_on( No 0 ) */
/****************************************/
static void Zmaster_apps_apps_mains1e0( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAIN] = ( uint8_t )ZMASTER_APPS_APPS_MAINS0;
    Zmaster_apps_apps_mains0StateEntry();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/*   Event : ara_split_main( No 0 )     */
/****************************************/
static void Zmaster_apps_apps_car_stops0e0( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS1;
    stm_mst_apl_start_activity_meter_receiver();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/*   Event : ara_split_main( No 2 )     */
/****************************************/
static void Zmaster_apps_apps_car_stops0e2( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS3;
    stm_mst_apl_start_activity_splitable_receiver();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/*   Event : ara_split_sub( No 3 )      */
/****************************************/
static void Zmaster_apps_apps_car_stops0e3( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS2;
    stm_mst_apl_start_activity_meter_splitable();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/*   Event : ara_normal( No 4 )         */
/****************************************/
static void Zmaster_apps_apps_car_stops0e4( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS5;
    stm_mst_apl_start_activity_general();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/*   Event : stt_prv_layer_apps_none( No 11 ) */
/****************************************/
static void Zmaster_apps_apps_car_stops0e11( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS0;
    stm_mst_apl_start_activity_none();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/*   Event : stt_prv_layer_apps_splitable_split( No 15 ) */
/****************************************/
static void Zmaster_apps_apps_car_stops0e15( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS4;
    stm_mst_apl_start_activity_splitable_split();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_run                 */
/*   State : meter_receiver( No 0 )     */
/*   Event : ara_normal( No 0 )         */
/****************************************/
static void Zmaster_apps_apps_car_runs0e0( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS1F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_RUNS1;
    stm_mst_apl_start_activity_meter();
}

/****************************************/
/* Action function                      */
/*   STM : apps_car_run                 */
/*   State : meter_receiver( No 0 )     */
/*   Event : stt_prv_layer_apps_meter_receiver( No 3 ) */
/****************************************/
static void Zmaster_apps_apps_car_runs0e3( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS1F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_RUNS0;
    stm_mst_apl_start_activity_meter_receiver();
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_main                    */
/*   State : lightstatus_brake_on( No 0 ) */
/****************************************/
static void Zmaster_apps_apps_mains0Event( void )
{
    /*stt_lightstatus_brake_off*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOff )
    {
        stm_mst_apl_event_lightstatus_brake_off();
        Zmaster_apps_apps_mains0e1();
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_stop                */
/*   State : none( No 0 )               */
/****************************************/
static void Zmaster_apps_apps_car_stops0Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_receiver*/
            else if( g_stm_category == StmCtgNoReceiver )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_main*/
            else if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e2();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_general*/
            else if( g_stm_category == StmCtgNoGeneral )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_stops0e4();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_none*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoNone )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*stt_prv_layer_apps_meter_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_stops0e0();
            }
            /*stt_prv_layer_apps_meter_splitable*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrSpl )
            {
                Zmaster_apps_apps_car_stops0e3();
            }
            /*stt_prv_layer_apps_splitable_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplRcv )
            {
                Zmaster_apps_apps_car_stops0e2();
            }
            /*stt_prv_layer_apps_splitable_split*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplSpl )
            {
                Zmaster_apps_apps_car_stops0e15();
            }
            /*stt_prv_layer_apps_gen_nml*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoGenNml )
            {
                Zmaster_apps_apps_car_stops0e4();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_stop                */
/*   State : meter_receiver( No 1 )     */
/****************************************/
static void Zmaster_apps_apps_car_stops1Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_splitable_main*/
            if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e2();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_general*/
            else if( g_stm_category == StmCtgNoGeneral )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_stops0e4();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_homescreen*/
            else if( g_stm_category == StmCtgNoHomescreen )
            {
                /*ara_fullscreen*/
                if( g_stm_area == StmAreaNoFullscreen )
                {
                    Zmaster_apps_apps_car_stops0e11();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_deactivate*/
        else if( g_stm_event == StmEvtNoDeactivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*ctg_receiver*/
            else if( g_stm_category == StmCtgNoReceiver )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_none*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoNone )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*stt_prv_layer_apps_meter_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_stops0e0();
            }
            /*stt_prv_layer_apps_meter_splitable*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrSpl )
            {
                Zmaster_apps_apps_car_stops0e3();
            }
            /*stt_prv_layer_apps_splitable_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplRcv )
            {
                Zmaster_apps_apps_car_stops0e2();
            }
            /*stt_prv_layer_apps_splitable_split*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplSpl )
            {
                Zmaster_apps_apps_car_stops0e15();
            }
            /*stt_prv_layer_apps_gen_nml*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoGenNml )
            {
                Zmaster_apps_apps_car_stops0e4();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_stop                */
/*   State : meter_splitable( No 2 )    */
/****************************************/
static void Zmaster_apps_apps_car_stops2Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_receiver*/
            if( g_stm_category == StmCtgNoReceiver )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_main*/
            else if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e15();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_general*/
            else if( g_stm_category == StmCtgNoGeneral )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_stops0e4();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_homescreen*/
            else if( g_stm_category == StmCtgNoHomescreen )
            {
                /*ara_fullscreen*/
                if( g_stm_area == StmAreaNoFullscreen )
                {
                    Zmaster_apps_apps_car_stops0e11();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_deactivate*/
        else if( g_stm_event == StmEvtNoDeactivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_none*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoNone )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*stt_prv_layer_apps_meter_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_stops0e0();
            }
            /*stt_prv_layer_apps_meter_splitable*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrSpl )
            {
                Zmaster_apps_apps_car_stops0e3();
            }
            /*stt_prv_layer_apps_splitable_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplRcv )
            {
                Zmaster_apps_apps_car_stops0e2();
            }
            /*stt_prv_layer_apps_splitable_split*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplSpl )
            {
                Zmaster_apps_apps_car_stops0e15();
            }
            /*stt_prv_layer_apps_gen_nml*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoGenNml )
            {
                Zmaster_apps_apps_car_stops0e4();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_stop                */
/*   State : splitable_receiver( No 3 ) */
/****************************************/
static void Zmaster_apps_apps_car_stops3Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_main*/
            else if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e2();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e15();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_general*/
            else if( g_stm_category == StmCtgNoGeneral )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_stops0e4();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_homescreen*/
            else if( g_stm_category == StmCtgNoHomescreen )
            {
                /*ara_fullscreen*/
                if( g_stm_area == StmAreaNoFullscreen )
                {
                    Zmaster_apps_apps_car_stops0e11();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_deactivate*/
        else if( g_stm_event == StmEvtNoDeactivate )
        {
            /*ctg_receiver*/
            if( g_stm_category == StmCtgNoReceiver )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*ctg_splitable_main*/
            else if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_none*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoNone )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*stt_prv_layer_apps_meter_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_stops0e0();
            }
            /*stt_prv_layer_apps_meter_splitable*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrSpl )
            {
                Zmaster_apps_apps_car_stops0e3();
            }
            /*stt_prv_layer_apps_splitable_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplRcv )
            {
                Zmaster_apps_apps_car_stops0e2();
            }
            /*stt_prv_layer_apps_splitable_split*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplSpl )
            {
                Zmaster_apps_apps_car_stops0e15();
            }
            /*stt_prv_layer_apps_gen_nml*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoGenNml )
            {
                Zmaster_apps_apps_car_stops0e4();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_stop                */
/*   State : splitable_split( No 4 )    */
/****************************************/
static void Zmaster_apps_apps_car_stops4Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_receiver*/
            else if( g_stm_category == StmCtgNoReceiver )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e2();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_main*/
            else if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e15();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e15();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_general*/
            else if( g_stm_category == StmCtgNoGeneral )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_stops0e4();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_homescreen*/
            else if( g_stm_category == StmCtgNoHomescreen )
            {
                /*ara_fullscreen*/
                if( g_stm_area == StmAreaNoFullscreen )
                {
                    Zmaster_apps_apps_car_stops0e11();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_deactivate*/
        else if( g_stm_event == StmEvtNoDeactivate )
        {
            /*ctg_splitable_main*/
            if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e2();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_none*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoNone )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*stt_prv_layer_apps_meter_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_stops0e0();
            }
            /*stt_prv_layer_apps_meter_splitable*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrSpl )
            {
                Zmaster_apps_apps_car_stops0e3();
            }
            /*stt_prv_layer_apps_splitable_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplRcv )
            {
                Zmaster_apps_apps_car_stops0e2();
            }
            /*stt_prv_layer_apps_splitable_split*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplSpl )
            {
                Zmaster_apps_apps_car_stops0e15();
            }
            /*stt_prv_layer_apps_gen_nml*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoGenNml )
            {
                Zmaster_apps_apps_car_stops0e4();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_stop                */
/*   State : general( No 5 )            */
/****************************************/
static void Zmaster_apps_apps_car_stops5Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_receiver*/
            else if( g_stm_category == StmCtgNoReceiver )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_main*/
            else if( g_stm_category == StmCtgNoSplitableMain )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_stops0e2();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_splitable_sub*/
            else if( g_stm_category == StmCtgNoSplitableSub )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_stops0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_general*/
            else if( g_stm_category == StmCtgNoGeneral )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_stops0e4();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_homescreen*/
            else if( g_stm_category == StmCtgNoHomescreen )
            {
                /*ara_fullscreen*/
                if( g_stm_area == StmAreaNoFullscreen )
                {
                    Zmaster_apps_apps_car_stops0e11();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_deactivate*/
        else if( g_stm_event == StmEvtNoDeactivate )
        {
            /*ctg_general*/
            if( g_stm_category == StmCtgNoGeneral )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_none*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoNone )
            {
                Zmaster_apps_apps_car_stops0e11();
            }
            /*stt_prv_layer_apps_meter_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_stops0e0();
            }
            /*stt_prv_layer_apps_meter_splitable*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrSpl )
            {
                Zmaster_apps_apps_car_stops0e3();
            }
            /*stt_prv_layer_apps_splitable_receiver*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplRcv )
            {
                Zmaster_apps_apps_car_stops0e2();
            }
            /*stt_prv_layer_apps_splitable_split*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoSplSpl )
            {
                Zmaster_apps_apps_car_stops0e15();
            }
            /*stt_prv_layer_apps_gen_nml*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoGenNml )
            {
                Zmaster_apps_apps_car_stops0e4();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_main                    */
/*   State : lightstatus_brake_off( No 1 ) */
/****************************************/
static void Zmaster_apps_apps_mains1Event( void )
{
    /*stt_lightstatus_brake_on*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        stm_mst_apl_event_lightstatus_brake_on();
        Zmaster_apps_apps_mains1e0();
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_run                 */
/*   State : meter_receiver( No 0 )     */
/****************************************/
static void Zmaster_apps_apps_car_runs0Event( void )
{
    /*stt_lightstatus_brake_off*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOff )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                /*ara_normal*/
                if( g_stm_area == StmAreaNoNormal )
                {
                    Zmaster_apps_apps_car_runs0e0();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_meter_receiver*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_runs0e3();
            }
            /*stt_prv_layer_apps_meter*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrNml )
            {
                Zmaster_apps_apps_car_runs0e0();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : apps_car_run                 */
/*   State : meter( No 1 )              */
/****************************************/
static void Zmaster_apps_apps_car_runs1Event( void )
{
    /*stt_lightstatus_brake_off*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOff )
    {
        /*evt_activate*/
        if( g_stm_event == StmEvtNoActivate )
        {
            /*ctg_meter*/
            if( g_stm_category == StmCtgNoMeter )
            {
                /*ara_split_main*/
                if( g_stm_area == StmAreaNoSplitMain )
                {
                    Zmaster_apps_apps_car_runs0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            /*ctg_receiver*/
            else if( g_stm_category == StmCtgNoReceiver )
            {
                /*ara_split_sub*/
                if( g_stm_area == StmAreaNoSplitSub )
                {
                    Zmaster_apps_apps_car_runs0e3();
                }
                else
                {
                    /*Else and default design have not done.*/
                    /*Please confirm the STM and design else and default.*/
                }
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        /*evt_undo*/
        else if( g_stm_event == StmEvtNoUndo )
        {
            /*stt_prv_layer_apps_meter_receiver*/
            if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrRcv )
            {
                Zmaster_apps_apps_car_runs0e3();
            }
            /*stt_prv_layer_apps_meter*/
            else if( g_stm_prv_state.layer[StmLayerNoApps].state == StmLayoutNoMtrNml )
            {
                Zmaster_apps_apps_car_runs0e0();
            }
            else
            {
                /*Else and default design have not done.*/
                /*Please confirm the STM and design else and default.*/
            }
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event call function                  */
/*   STM : apps_main                    */
/****************************************/
void stm_master_apl_event_call( void )
{
    stm_mst_apl_start_stm();
    switch( Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAIN] )
    {
    case ZMASTER_APPS_APPS_MAINS0:
        switch( Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] )
        {
        case ZMASTER_APPS_APPS_CAR_STOPS0:
            Zmaster_apps_apps_car_stops0Event();
            break;
        case ZMASTER_APPS_APPS_CAR_STOPS1:
            Zmaster_apps_apps_car_stops1Event();
            break;
        case ZMASTER_APPS_APPS_CAR_STOPS2:
            Zmaster_apps_apps_car_stops2Event();
            break;
        case ZMASTER_APPS_APPS_CAR_STOPS3:
            Zmaster_apps_apps_car_stops3Event();
            break;
        case ZMASTER_APPS_APPS_CAR_STOPS4:
            Zmaster_apps_apps_car_stops4Event();
            break;
        case ZMASTER_APPS_APPS_CAR_STOPS5:
            Zmaster_apps_apps_car_stops5Event();
            break;
        default:
            /*Not accessible to this else (default).*/
            break;
        }
        Zmaster_apps_apps_mains0Event();
        break;
    case ZMASTER_APPS_APPS_MAINS1:
        switch( Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS1F] )
        {
        case ZMASTER_APPS_APPS_CAR_RUNS0:
            Zmaster_apps_apps_car_runs0Event();
            break;
        case ZMASTER_APPS_APPS_CAR_RUNS1:
            Zmaster_apps_apps_car_runs1Event();
            break;
        default:
            /*Not accessible to this else (default).*/
            break;
        }
        Zmaster_apps_apps_mains1Event();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* Initial function                     */
/*   STM : apps_main                    */
/****************************************/
void stm_master_apl_initialize( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAIN] = ( uint8_t )ZMASTER_APPS_APPS_MAINS0;
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS0F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_STOPS0;
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAINS1F] = ( uint8_t )ZMASTER_APPS_APPS_CAR_RUNS0;
    Zmaster_apps_apps_mains0StateEntry();
}

/****************************************/
/* Terminate function                   */
/*   STM : apps_main                    */
/****************************************/
void Zmaster_apps_apps_mainTerminate( void )
{
    Zmaster_apps_apps_mainState[ZMASTER_APPS_APPS_MAIN] = ( uint8_t )ZMASTER_APPS_APPS_MAINTERMINATE;
}


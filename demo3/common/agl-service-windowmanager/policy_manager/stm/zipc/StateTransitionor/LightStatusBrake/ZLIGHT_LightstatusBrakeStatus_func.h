/************************************************************/
/*     ZLIGHT_LightstatusBrakeStatus_func.h                 */
/*     Function and variable header file                    */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZLIGHT_LIGHTSTATUSBRAKESTATUS_FUNC_H
#define ZHEADER_ZLIGHT_LIGHTSTATUSBRAKESTATUS_FUNC_H

extern void stm_lbs_start_activity_lightstatus_brake_off();
extern void stm_lbs_start_activity_lightstatus_brake_on();
extern void stm_lbs_initialize_variable();
extern void stm_lbs_start_stm();

#endif

/************************************************************/
/*     ZLIGHT_LightstatusBrake.c                            */
/*     LightstatusBrake State transition model source file  */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../ZST_include.h"

/* State management variable */
static uint8_t ZLIGHT_LightstatusBrakeState[ZLIGHT_LIGHTSTATUSBRAKESTATENOMAX];

static void ZLIGHT_LightstatusBrakes0e1( void );
static void ZLIGHT_LightstatusBrakes1e0( void );
static void ZLIGHT_LightstatusBrakes0Event( void );
static void ZLIGHT_LightstatusBrakes1Event( void );

/****************************************/
/* Action function                      */
/*   STM : LightstatusBrake             */
/*   State : lightstatus_brake_on( No 0 ) */
/*   Event : evt_lightstatus_brake_off( No 1 ) */
/****************************************/
static void ZLIGHT_LightstatusBrakes0e1( void )
{
    ZLIGHT_LightstatusBrakeState[ZLIGHT_LIGHTSTATUSBRAKE] = ( uint8_t )ZLIGHT_LIGHTSTATUSBRAKES1;
    stm_lbs_start_activity_lightstatus_brake_off();
}

/****************************************/
/* Action function                      */
/*   STM : LightstatusBrake             */
/*   State : lightstatus_brake_off( No 1 ) */
/*   Event : evt_lightstatus_brake_on( No 0 ) */
/****************************************/
static void ZLIGHT_LightstatusBrakes1e0( void )
{
    ZLIGHT_LightstatusBrakeState[ZLIGHT_LIGHTSTATUSBRAKE] = ( uint8_t )ZLIGHT_LIGHTSTATUSBRAKES0;
    stm_lbs_start_activity_lightstatus_brake_on();
}

/****************************************/
/* Event appraisal function             */
/*   STM : LightstatusBrake             */
/*   State : lightstatus_brake_on( No 0 ) */
/****************************************/
static void ZLIGHT_LightstatusBrakes0Event( void )
{
    /*evt_lightstatus_brake_off*/
    if( g_stm_event == StmEvtNoLightstatusBrakeOff )
    {
        ZLIGHT_LightstatusBrakes0e1();
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : LightstatusBrake             */
/*   State : lightstatus_brake_off( No 1 ) */
/****************************************/
static void ZLIGHT_LightstatusBrakes1Event( void )
{
    /*evt_lightstatus_brake_on*/
    if( g_stm_event == StmEvtNoLightstatusBrakeOn )
    {
        ZLIGHT_LightstatusBrakes1e0();
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event call function                  */
/*   STM : LightstatusBrake             */
/****************************************/
void stm_lbs_event_call( void )
{
    stm_lbs_start_stm();
    switch( ZLIGHT_LightstatusBrakeState[ZLIGHT_LIGHTSTATUSBRAKE] )
    {
    case ZLIGHT_LIGHTSTATUSBRAKES0:
        ZLIGHT_LightstatusBrakes0Event();
        break;
    case ZLIGHT_LIGHTSTATUSBRAKES1:
        ZLIGHT_LightstatusBrakes1Event();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* Initial function                     */
/*   STM : LightstatusBrake             */
/****************************************/
void stm_lbs_initialize( void )
{
    ZLIGHT_LightstatusBrakeState[ZLIGHT_LIGHTSTATUSBRAKE] = ( uint8_t )ZLIGHT_LIGHTSTATUSBRAKES0;
    stm_lbs_start_activity_lightstatus_brake_on();
}

/****************************************/
/* Terminate function                   */
/*   STM : LightstatusBrake             */
/****************************************/
void ZLIGHT_LightstatusBrakeTerminate( void )
{
    ZLIGHT_LightstatusBrakeState[ZLIGHT_LIGHTSTATUSBRAKE] = ( uint8_t )ZLIGHT_LIGHTSTATUSBRAKETERMINATE;
}


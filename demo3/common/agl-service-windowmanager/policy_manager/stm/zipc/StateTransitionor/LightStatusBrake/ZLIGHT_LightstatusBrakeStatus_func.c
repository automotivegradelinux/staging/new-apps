/************************************************************/
/*     ZLIGHT_LightstatusBrakeStatus_func.c                 */
/*     Function and variable source file                    */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../ZST_include.h"

/*************************************************************
    Function definition
*************************************************************/

/*
 * @name stm_lbs_start_activity_lightstatus_brake_off
 */
void stm_lbs_start_activity_lightstatus_brake_off() {
    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state = StmLightstatusBrakeSttNoOff;
    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].changed = STM_TRUE;
}

/*
 * @name stm_lbs_start_activity_lightstatus_brake_on
 */
void stm_lbs_start_activity_lightstatus_brake_on() {
    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state = StmLightstatusBrakeSttNoOn;
    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].changed = STM_TRUE;
}

/*
 * @name stm_lbs_initialize_variable
 */
void stm_lbs_initialize_variable() {
    g_stm_prv_state.car_element[StmCarElementNoLightstatusBrake].state = StmLightstatusBrakeSttNoOn;
    g_stm_prv_state.car_element[StmCarElementNoLightstatusBrake].changed = STM_FALSE;

    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state = StmLightstatusBrakeSttNoOn;
    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].changed = STM_FALSE;
}

/*
 * @name stm_lbs_start_stm
 */
void stm_lbs_start_stm() {
	g_stm_prv_state.car_element[StmCarElementNoLightstatusBrake].state = g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state;
    g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].changed = STM_FALSE;
}

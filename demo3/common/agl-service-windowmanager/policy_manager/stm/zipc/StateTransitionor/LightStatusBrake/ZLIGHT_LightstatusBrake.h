/************************************************************/
/*     ZLIGHT_LightstatusBrake.h                            */
/*     LightstatusBrake State transition model header file  */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZLIGHT_LIGHTSTATUSBRAKE_H
#define ZHEADER_ZLIGHT_LIGHTSTATUSBRAKE_H

/*State management variable access define*/
#define ZLIGHT_LIGHTSTATUSBRAKE ( 0U )
#define ZLIGHT_LIGHTSTATUSBRAKES0 ( 0U )
#define ZLIGHT_LIGHTSTATUSBRAKES1 ( 1U )
#define ZLIGHT_LIGHTSTATUSBRAKESTATENOMAX ( 1U )

/*End state define*/
#define ZLIGHT_LIGHTSTATUSBRAKEEND ( 2U )
/*Terminate state define*/
#define ZLIGHT_LIGHTSTATUSBRAKETERMINATE ( ZLIGHT_LIGHTSTATUSBRAKEEND + 1U )

/*State no define*/
#define ZLIGHT_LIGHTSTATUSBRAKES0STATENO ( 0U )
#define ZLIGHT_LIGHTSTATUSBRAKES1STATENO ( 1U )

/*State serial no define*/
#define ZLIGHT_LIGHTSTATUSBRAKES0STATESERIALNO ( 0U )
#define ZLIGHT_LIGHTSTATUSBRAKES1STATESERIALNO ( 1U )

/*Event no define*/
#define ZLIGHT_LIGHTSTATUSBRAKEE0EVENTNO ( 0U )
#define ZLIGHT_LIGHTSTATUSBRAKEE1EVENTNO ( 1U )

/*Event serial no define*/
#define ZLIGHT_LIGHTSTATUSBRAKEE0EVENTSERIALNO ( 0U )
#define ZLIGHT_LIGHTSTATUSBRAKEE1EVENTSERIALNO ( 1U )

/*Extern function*/
extern void stm_lbs_event_call( void );
extern void stm_lbs_initialize( void );
extern void ZLIGHT_LightstatusBrakeTerminate( void );

#endif

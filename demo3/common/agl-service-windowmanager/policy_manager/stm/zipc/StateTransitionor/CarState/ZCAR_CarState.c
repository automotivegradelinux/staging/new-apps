/************************************************************/
/*     ZCAR_CarState.c                                      */
/*     CarState State transition model source file          */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../ZST_include.h"

/* State management variable */
static uint8_t ZCAR_CarStateState[ZCAR_CARSTATESTATENOMAX];

static void ZCAR_CarStates0e1( void );
static void ZCAR_CarStates1e0( void );
static void ZCAR_CarStates0Event( void );
static void ZCAR_CarStates1Event( void );

/****************************************/
/* Action function                      */
/*   STM : CarState                     */
/*   State : car_stop( No 0 )           */
/*   Event : stt_accel_pedal_on( No 1 ) */
/****************************************/
static void ZCAR_CarStates0e1( void )
{
    ZCAR_CarStateState[ZCAR_CARSTATE] = ( uint8_t )ZCAR_CARSTATES1;
    stm_rns_start_activity_car_run();
}

/****************************************/
/* Action function                      */
/*   STM : CarState                     */
/*   State : car_run( No 1 )            */
/*   Event : stt_accel_pedal_off( No 0 )*/
/****************************************/
static void ZCAR_CarStates1e0( void )
{
    ZCAR_CarStateState[ZCAR_CARSTATE] = ( uint8_t )ZCAR_CARSTATES0;
    stm_rns_start_activity_car_stop();
}

/****************************************/
/* Event appraisal function             */
/*   STM : CarState                     */
/*   State : car_stop( No 0 )           */
/****************************************/
static void ZCAR_CarStates0Event( void )
{
    /*stt_lightstatus_brake_off*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOff )
    {
        /*stt_accel_pedal_on*/
        if( g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state == StmAccelPedalSttNoOn )
        {
            ZCAR_CarStates0e1();
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event appraisal function             */
/*   STM : CarState                     */
/*   State : car_run( No 1 )            */
/****************************************/
static void ZCAR_CarStates1Event( void )
{
    /*stt_lightstatus_brake_off*/
    if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOff )
    {
        /*stt_accel_pedal_off*/
        if( g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state == StmAccelPedalSttNoOff )
        {
            ZCAR_CarStates1e0();
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    /*stt_lightstatus_brake_on*/
    else if( g_stm_crr_state.car_element[StmCarElementNoLightstatusBrake].state == StmLightstatusBrakeSttNoOn )
    {
        /*stt_accel_pedal_off*/
        if( g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state == StmAccelPedalSttNoOff )
        {
            ZCAR_CarStates1e0();
        }
        /*stt_accel_pedal_on*/
        else if( g_stm_crr_state.car_element[StmCarElementNoAccelPedal].state == StmAccelPedalSttNoOn )
        {
            ZCAR_CarStates1e0();
        }
        else
        {
            /*Else and default design have not done.*/
            /*Please confirm the STM and design else and default.*/
        }
    }
    else
    {
        /*Else and default design have not done.*/
        /*Please confirm the STM and design else and default.*/
    }
}

/****************************************/
/* Event call function                  */
/*   STM : CarState                     */
/****************************************/
void stm_rns_event_call( void )
{
    stm_rns_start_stm();
    switch( ZCAR_CarStateState[ZCAR_CARSTATE] )
    {
    case ZCAR_CARSTATES0:
        ZCAR_CarStates0Event();
        break;
    case ZCAR_CARSTATES1:
        ZCAR_CarStates1Event();
        break;
    default:
        /*Not accessible to this else (default).*/
        break;
    }
}

/****************************************/
/* Initial function                     */
/*   STM : CarState                     */
/****************************************/
void stm_rns_initialize( void )
{
    ZCAR_CarStateState[ZCAR_CARSTATE] = ( uint8_t )ZCAR_CARSTATES0;
    stm_rns_start_activity_car_stop();
}

/****************************************/
/* Terminate function                   */
/*   STM : CarState                     */
/****************************************/
void ZCAR_CarStateTerminate( void )
{
    ZCAR_CarStateState[ZCAR_CARSTATE] = ( uint8_t )ZCAR_CARSTATETERMINATE;
}


/************************************************************/
/*     ZCAR_CarState_func.c                                 */
/*     Function and variable source file                    */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#include "../ZST_include.h"

/*************************************************************
    Function definition
*************************************************************/

/*
 * @name stm_rns_start_activity_car_stop
 */
void stm_rns_start_activity_car_stop() {
    g_stm_crr_state.car_element[StmCarElementNoRunning].state = StmRunningNoStop;
    g_stm_crr_state.car_element[StmCarElementNoRunning].changed = STM_TRUE;
}

/*
 * @name stm_rns_start_activity_car_run
 */
void stm_rns_start_activity_car_run() {
    g_stm_crr_state.car_element[StmCarElementNoRunning].state = StmRunningNoRun;
    g_stm_crr_state.car_element[StmCarElementNoRunning].changed = STM_TRUE;
}

/*
 * @name stm_rns_initialize_variable
 */
void stm_rns_initialize_variable() {
	g_stm_prv_state.car_element[StmCarElementNoRunning].state = StmRunningNoStop;
	g_stm_prv_state.car_element[StmCarElementNoRunning].changed = STM_FALSE;

    g_stm_crr_state.car_element[StmCarElementNoRunning].state = StmRunningNoStop;
	g_stm_crr_state.car_element[StmCarElementNoRunning].changed = STM_FALSE;
}

/*
 * @name stm_rns_start_stm
 */
void stm_rns_start_stm() {
	g_stm_prv_state.car_element[StmCarElementNoRunning].state = g_stm_crr_state.car_element[StmCarElementNoRunning].state;
	g_stm_crr_state.car_element[StmCarElementNoRunning].changed = STM_FALSE;
}

/************************************************************/
/*     ZCAR_CarState.h                                      */
/*     CarState State transition model header file          */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZCAR_CARSTATE_H
#define ZHEADER_ZCAR_CARSTATE_H

/*State management variable access define*/
#define ZCAR_CARSTATE ( 0U )
#define ZCAR_CARSTATES0 ( 0U )
#define ZCAR_CARSTATES1 ( 1U )
#define ZCAR_CARSTATESTATENOMAX ( 1U )

/*End state define*/
#define ZCAR_CARSTATEEND ( 2U )
/*Terminate state define*/
#define ZCAR_CARSTATETERMINATE ( ZCAR_CARSTATEEND + 1U )

/*State no define*/
#define ZCAR_CARSTATES0STATENO ( 0U )
#define ZCAR_CARSTATES1STATENO ( 1U )

/*State serial no define*/
#define ZCAR_CARSTATES0STATESERIALNO ( 0U )
#define ZCAR_CARSTATES1STATESERIALNO ( 1U )

/*Event no define*/
#define ZCAR_CARSTATEE0EVENTNO ( 0U )
#define ZCAR_CARSTATEE1EVENTNO ( 1U )
#define ZCAR_CARSTATEE2EVENTNO ( 2U )
#define ZCAR_CARSTATEE3EVENTNO ( 3U )

/*Event serial no define*/
#define ZCAR_CARSTATEE0EVENTSERIALNO ( 0U )
#define ZCAR_CARSTATEE1EVENTSERIALNO ( 1U )
#define ZCAR_CARSTATEE2EVENTSERIALNO ( 2U )
#define ZCAR_CARSTATEE3EVENTSERIALNO ( 3U )

/*Extern function*/
extern void stm_rns_event_call( void );
extern void stm_rns_initialize( void );
extern void ZCAR_CarStateTerminate( void );

#endif

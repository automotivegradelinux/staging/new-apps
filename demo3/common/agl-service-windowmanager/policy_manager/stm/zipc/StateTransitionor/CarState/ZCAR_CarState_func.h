/************************************************************/
/*     ZCAR_CarState_func.h                                 */
/*     Function and variable header file                    */
/*     ZIPC Designer Version 1.2.0                          */
/************************************************************/
#ifndef ZHEADER_ZCAR_CARSTATE_FUNC_H
#define ZHEADER_ZCAR_CARSTATE_FUNC_H

extern void stm_rns_start_activity_car_stop();
extern void stm_rns_start_activity_car_run();
extern void stm_rns_initialize_variable();
extern void stm_rns_start_stm();

#endif

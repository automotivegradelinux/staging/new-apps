#include "./StateTransitionor/ZST_include.h"

void stm_mst_apl_start_activity_none() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoNone;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_start_activity_meter_receiver() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoMtrRcv;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_start_activity_meter_splitable() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoMtrSpl;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_start_activity_splitable_receiver() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoSplRcv;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_start_activity_splitable_split() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoSplSpl;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_start_activity_general() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoGenNml;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_start_activity_meter() {
    g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoMtrNml;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_event_lightstatus_brake_on() {
    g_stm_crr_state.layer[StmLayerNoApps].state = g_prv_apps_state_car_stop;
    g_stm_crr_state.layer[StmLayerNoApps].changed = STM_TRUE;
}

void stm_mst_apl_event_lightstatus_brake_off() {
    g_prv_apps_state_car_stop = g_stm_prv_state.layer[StmLayerNoApps].state;
}

void stm_master_apl_initialize_valiable() {
	g_stm_prv_state.layer[StmLayerNoApps].state = StmLayoutNoNone;
	g_stm_prv_state.layer[StmLayerNoApps].changed = STM_FALSE;

	g_stm_crr_state.layer[StmLayerNoApps].state = StmLayoutNoNone;
	g_stm_crr_state.layer[StmLayerNoApps].changed = STM_FALSE;
}

void stm_mst_apl_start_stm() {
	if (g_stm_event == StmEvtNoUndo) {
		// nop
	}
	else {
		g_stm_prv_state.layer[StmLayerNoApps].state = g_stm_crr_state.layer[StmLayerNoApps].state;
	}
	g_stm_crr_state.layer[StmLayerNoApps].changed = STM_FALSE;
}

import QtQuick 2.1

Text {
    verticalAlignment: Text.AlignVCenter
    font.family: "Lato"
    font.weight: Font.Light
    color: "#000000"
    font.pixelSize: 24
}

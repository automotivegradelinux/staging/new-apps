import QtQuick 2.6
import QtQuick.Controls 2.0

ApplicationWindow {
    id: root

    color: "#00000000"
   
    Label {
        id: message
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 20
        font.pixelSize: 75
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "white"
        text: "Only the video’s sound will be available while driving."
    }

    background : Image {
        id: backgroundImg
        anchors.fill: parent
        anchors.topMargin: 0
        anchors.bottomMargin: 0

        visible: true
        fillMode: Image.Stretch
        source: 'images/black_normal.png'

        state: "begin"
        states: [
            State {
                name: "begin"
                PropertyChanges { target: backgroundImg; opacity: 0.25 }
            },
            State {
                name: "end"
                PropertyChanges { target: backgroundImg; opacity: 0.75 }
            }
        ]

        transitions: [
            Transition {
                from: "begin"; to: "end"
                PropertyAnimation {target: backgroundImg; properties: "opacity"; duration: 2000}
            }
        ]
    }

    function showImage(area) {
        if (area === 'normal') {
            backgroundImg.source = 'images/black_normal.png'
        } else {
            backgroundImg.source = 'images/black_split.png'
        }
        backgroundImg.state = "end"
    }

    
    function hideImage() {
        backgroundImg.state = "begin"
    }
}

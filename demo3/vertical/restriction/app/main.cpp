
#include <QtQml/qqml.h>
#include <qlibwindowmanager.h>
#include <QQuickWindow>
#include <QtCore/QCommandLineParser>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QStandardPaths>
#include <QtCore/QUrlQuery>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include <libhomescreen.hpp>

int main(int argc, char* argv[]) {
    QString role = QString("Restriction");

    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("AGL");

    QQmlApplicationEngine engine;

    QCommandLineParser parser;
    parser.addPositionalArgument("port",
                                 app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret",
                                 app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();

    if (positionalArguments.length() == 2) {
        int port = positionalArguments.takeFirst().toInt();
        QString secret = positionalArguments.takeFirst();
        QUrl bindingAddress;
        bindingAddress.setScheme(QStringLiteral("ws"));
        bindingAddress.setHost(QStringLiteral("localhost"));
        bindingAddress.setPort(port);
        bindingAddress.setPath(QStringLiteral("/api"));
        QUrlQuery query;
        query.addQueryItem(QStringLiteral("token"), secret);
        bindingAddress.setQuery(query);

        std::string token = secret.toStdString();
        LibHomeScreen* hs = new LibHomeScreen();
        QLibWindowmanager* qwm = new QLibWindowmanager();

        if (qwm->init(port, secret) != 0) {
            exit(EXIT_FAILURE);
        }
        // Request a surface as described in layers.json windowmanager’s file
        if (qwm->requestSurface(role) != 0) {
            exit(EXIT_FAILURE);
        }
        // Create an event callback against an event type. Here a lambda is
        // called when SyncDraw event occurs
        qwm->set_event_handler(QLibWindowmanager::Event_SyncDraw,
                               [qwm, role](json_object* object) {
                                   fprintf(stderr, "Surface got syncDraw!\n");
                                   qwm->endDraw(role);
                               });

        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
        QObject* root = engine.rootObjects().first();

        // HomeScreen
        hs->init(port, token.c_str());

        // release restriction
        hs->set_event_handler(
            LibHomeScreen::Event_ReleaseRestriction,
            [qwm, role, root](json_object* object) {
                json_object* areaJ = nullptr;
                if (json_object_object_get_ex(object, "area", &areaJ)) {
                    QString area(QLatin1String(json_object_get_string(areaJ)));

                    QMetaObject::invokeMethod(root, "hideImage");

                    // qwm->releaseWR(role, area);
                    qwm->deactivateSurface(role);
                }
            });

        // allocate restriction
        hs->set_event_handler(
            LibHomeScreen::Event_AllocateRestriction,
            [qwm, role, root](json_object* object) {
                json_object* areaJ = nullptr;
                if (json_object_object_get_ex(object, "area", &areaJ)) {
                    QString area(QLatin1String(json_object_get_string(areaJ)));
                    qDebug()
                        << "Surface got Event_AllocateRestriction " << area;

                    QMetaObject::invokeMethod(root, "showImage",
                                              Q_ARG(QVariant, area));

                    // qwm->allocateWR(role, area);
                    qwm->activateSurface(role, area.prepend("restriction."));
                }
            });
    }

    return app.exec();
}
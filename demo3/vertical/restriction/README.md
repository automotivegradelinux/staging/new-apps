# CAN模拟走行规制说明手顺

## 5.0.2源码打wayland补丁并重新编译

```
$ cd meta-agl
$ patch -p1 < meta-agl-wayland-ivi-extension_2.0.2.patch
```

## 关联Service和App源码的下载地址和版本信息

1. [low-level-can-service (CommitID:700580)][lowcan]

    需要打开机自动启动补丁

    ```
    $ cd low-level-can-service
    $ patch -p1 < low-level-can-service.patch
    ```

2. [qlibwindowmanager][qlibwm]

3. [libwindowmanager][libwm]

4. [agl-service-windowmanager-2017][wm]

5. [agl-service-homescreen-2017 (CommitID:1a955ad4)][hs]

6. [libHomeScreen (CommitID:0240e70e)][libhs]

7. [restriction][restriction]

8. [videoplayer][videoplayer]

9. [homescreen-2017 (CommitID:b5865d4b)][hsapp]


## SD卡制作 (在PC上操作)

1. 烧写SD卡

2. 利用Ubuntu16.04中的Disks工具对SD卡空闲区域进行分区，并格式化为ext4格式, 最终生成分区名如: `/dev/sdb2`

3. 拷贝video文件到SD卡的media目录下

    ```
    $ sudo cp Camry_EBV.mp4 /media/$USER/root/media/
    ```

4. 拷贝地图数据到SD卡新分区

    ```
    $ sudo cp -r navi_data /media/$USER/data/
    ```

5. 拷贝相关service和app到SD卡

    ```
    $ sudo cp low-can-service.wgt /media/$USER/root/home/root/
    $ sudo cp windowmanager-service-2017.wgt /media/$USER/root/home/root/
    $ sudo cp libpolicy_manager.so /media/$USER/root/usr/lib/
    $ sudo cp libwindowmanager.so.0.1.0 /media/$USER/root/usr/lib/
    $ sudo cp libqtWindowmanagerWrapper.so.0.1.0 /media/$USER/root/usr/lib/
    $ sudo cp homescreen-service-2017.wgt /media/$USER/root/home/root/
    $ sudo cp libhomescreen.so.0.1 /media/$USER/root/usr/lib/
    $ sudo cp restriction.wgt /media/$USER/root/home/root/
    $ sudo cp videoplayer.wgt /media/$USER/root/home/root/
    $ sudo cp homescreen-2017.wgt /media/$USER/root/home/root/
    ```


## 启动配置 (在开发板上操作)

1. 安装app

    ```
    ssh root@${BOARDIP}
    # afm-util remove low-can-service@5.0
    # afm-util install low-can-service.wgt
    # afm-util remove windowmanager-service-2017@0.1
    # afm-util install windowmanager-service-2017.wgt
    # afm-util remove homescreen-service-2017@0.1
    # afm-util install homescreen-service-2017.wgt
    # afm-util install videoplayer.wgt
    # afm-util install restriction.wgt
    # afm-util remove homescreen-2017@0.1
    # afm-util install homescreen-2017.wgt
    ```

2. 配置dev-mapping

    ```
    # echo -e "[CANbus-mapping]\nhs=\"vcan0\"\nls=\"vcan1\"\n" > /etc/dev-mapping.conf
    ```

    ***注意： 在实车环境下，需要将vcan0和vcan1替换为实车的can设备节点***

3. 创建生成vcan设备脚本

    ```
    # echo -e "modprobe vcan\nip link add vcan0 type vcan\nip link set vcan0 up\nip link add vcan1 type vcan\nip link set vcan1 up" > ~/lowcan.sh
    # chmod +x ~/lowcan.sh
    ```

4. 自动挂载SD卡新分区

    ```
    # echo -e "/dev/mmcblk0p2       /home/root/ALS          auto       defaults,sync  0  0" >> /etc/fstab
    ```

5. 链接地图数据

    ```
    # ln -s /home/root/ALS/navi_data /home/navi_data
    ```

6. 修改lowcan启动的service文件

    ```
    # vi /var/local/lib/systemd/user/afm-service-low-can-service@5.0.service
    ...
    //找到此行
    ExecStartPre=/bin/mkdir -p %h/app-data/low-can-service
    //增加下面一行
    ExecStartPre=/bin/sh %h/lowcan.sh
    ...
    ```

    ***注意：afm-service-low-can-service@5.0.service文件版本号可能不同，根据实机上的文件进行修改即可***

7. 重启开发板


## 调试

1. 打开videoplayer

2. 在ssh终端发送can数据

    ```
    ssh root@${BOARDIP}
    # cansend vcan0 3BB#01
    # cansend vcan0 3BB#00
    ```

    ***说明：
    3BB#01 表示 Event_LightstatusBrakeOn
    3BB#00 表示 Event_LightstatusBrakeOff***


[lowcan]: https://oss-project.tmc-tokai.jp/gitlab/als2018/low-level-can-service/tree/master
[qlibwm]: https://gerrit.automotivelinux.org/gerrit/gitweb?p=staging/qlibwindowmanager.git;a=tree;h=696956618c85a6203464720721bdf3ad50bcdaa1;hb=696956618c85a6203464720721bdf3ad50bcdaa1
[libwm]: https://gerrit.automotivelinux.org/gerrit/gitweb?p=staging/libwindowmanager.git;a=tree;h=9bd3d91218f196da4246e87c8273548f04ced57e;hb=9bd3d91218f196da4246e87c8273548f04ced57e
[wm]: https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps/agl-service-windowmanager-2017.git;a=tree;h=a21dd2b748731f1bbe956da6d22a87cc1412de30;hb=a21dd2b748731f1bbe956da6d22a87cc1412de30
[hs]: https://oss-project.tmc-tokai.jp/gitlab/AppFW/agl-service-homescreen-2017/tree/ALS
[libhs]: https://oss-project.tmc-tokai.jp/gitlab/AppFW/libHomeScreen/tree/ALS
[restriction]: https://oss-project.tmc-tokai.jp/gitlab/als2018/restriction/tree/master
[videoplayer]: https://oss-project.tmc-tokai.jp/gitlab/als2018/videoplayer/tree/master
[hsapp]: https://oss-project.tmc-tokai.jp/gitlab/als2018/homescreen-2017/tree/master

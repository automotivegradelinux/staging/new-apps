# Readme for CES2019 demo#3
<br>There are two ECU connect with each other for CES2019 demo#3.

## horizontal (Meter cluster mode)
- homescreen
  <br> sandbox branch : [ces2019_horizontal](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps/homescreen.git;a=tree;h=refs/heads/sandbox/zheng_wenlong/ces2019_horizontal;hb=refs/heads/sandbox/zheng_wenlong/ces2019_horizontal)
- launcher
  <br> sandbox branch : [ces2019_horizontal](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Flauncher.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_horizontal)
- dashboard
  <br> sandbox branch : [ces2019_horizontal](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fdashboard.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_horizontal)
- hvac
  <br> sandbox branch : [ces2019_horizontal](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fhvac.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_horizontal)
- settings
  <br> sandbox branch : [ces2019_horizontal](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fsettings.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_horizontal)
- tachometer
  <br> sandbox branch : [ces2019_horizontal](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fals-meter-demo.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_horizontal)
- camapp
  <br> sandbox branch : [camapp](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fals-meter-demo.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fcamapp)
- waltham-server
  <br> Used for receive video from waltham transmitter. Needs waltham weston.
## vertical (IVI mode)
- homescreen
  <br> sandbox branch : [ces2019_vertical](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fhomescreen.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_vertical)
- launcher
  <br> sandbox branch : [ces2019_vertical](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Flauncher.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019_vertical)
- tbtnavi_demo
  <br> sandbox branch : [tbtnavi](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fnavigation.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Ftbtnavi)
- videoplayer
  <br> sandbox branch : [ces2019_vertical](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fvideoplayer.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Ftiansen%2Fces2019_vertical)
- restriction
  <br> When car is running don't display videoplayer screen but restriction text.
- ...

## common ones
- homescreen-service
  <br> sandbox branch : [ces2019](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fagl-service-homescreen.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019)
- libhomescreen
  <br> sandbox branch : [ces2019](https://gerrit.automotivelinux.org/gerrit/gitweb?p=src%2Flibhomescreen.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019)
- qlibhomescreen
  <br> sandbox branch : [ces2019](https://gerrit.automotivelinux.org/gerrit/gitweb?p=src%2Flibqthomescreen.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019)
- windowmanager-service
  <br> sandbox branch : [ces2019](https://gerrit.automotivelinux.org/gerrit/gitweb?p=apps%2Fagl-service-windowmanager-2017.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019)
- libwindowmanager
  <br> sandbox branch : [ces2019](https://gerrit.automotivelinux.org/gerrit/gitweb?p=src%2Flibwindowmanager.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019)
- libqtwindowmanager
  <br> sandbox branch : [ces2019](https://gerrit.automotivelinux.org/gerrit/gitweb?p=src%2Flibqtwindowmanager.git;a=shortlog;h=refs%2Fheads%2Fsandbox%2Fzheng_wenlong%2Fces2019)
- ...
**Overview**

This application receives transmitted data by waltham-transmitter.
This application is implemented based on waltham-server.
The original source code of waltham-server is in `./orig`.

**Download**

Download souce code and SDK from gitlab.

```
$ git clone http://oss-project.tmc-tokai.jp/gitlab/WindowManager/waltham-server.git
```

*NOTE*

This application uses waltham protocol, therefore waltham librarys is necessary.
The downloaded SDK includes the libraries.
If build this application, please use this SDK.

**Install SDK**

1. Install SDK which is in `./sdk`.
```
$ cd ./sdk
$ ./poky-agl-glibc-x86_64-agl-demo-platform-crosssdk-aarch64-toolchain-6.0.0.sh
```

1. Copy gstglconfig.h
This SDK has gstglconfig.h
but it exists in `<sdk_installed_path>/sysroots/aarch64-agl-linux/usr/lib`
and it occures build error.
Therefore copy it to `<sdk_installed_path>/sysroots/aarch64-agl-linux/usr/include`.
```
$ cd <sdk_installed_path>/sysroots/aarch64-agl-linux/
$ cp usr/lib/gstreamer-1.0/include/gst/gl/gstglconfig.h usr/include/gstreamer-1.0/gst/gl/
```

**Build application**

Source SDK envirment and make.
```
$ source <sdk_installed_path>/environment-setup-aarch64-agl-linux
$ cd waltham-server
$ mkdir build
$ cd build
$ cmake ..
$ make
$ make receiver
```

**Install wgt and config file**

```
[On host]
$ scp build/package/receiver.wgt root@<your_target_ip>
$ scp config/pipeline.cfg root@<your_target_ip>:/etc/xdg/weston
$ ssh root@<your_target_ip>

[On target]
# afm-util install receiver.wgt
# sync
```

**NOTE**

This application uses port 34400 by default.
If change it, please modify the followings:

- In `config/pipeline.cfg`, value of `port` at line 1.
- In `src/wth-server-main.cpp`, value of `tcp_port` at line 49.

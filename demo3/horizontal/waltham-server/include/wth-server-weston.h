/**
 * @licence app begin@
 *
 *
 *  TBD
 *
 *
 * @licence end@
 */
/*******************************************************************************
**                                                                            **
**  SRC-MODULE:                                                               **
**                                                                            **
**  TARGET    : linux                                                         **
**                                                                            **
**  PROJECT   : waltham-server                                                **
**                                                                            **
**  AUTHOR    :                                                               **
**                                                                            **
**                                                                            **
**                                                                            **
**  PURPOSE   : Header file declare macros, extern functions, data types etc  **
**  required to interface with weston compositor at server side               **
**                                                                            **
**  REMARKS   :                                                               **
**                                                                            **
**  PLATFORM DEPENDANT [yes/no]: yes                                          **
**                                                                            **
**  TO BE CHANGED BY USER [yes/no]: no                                        **
**                                                                            **
*******************************************************************************/

#ifndef WALTHAM_SHM_H_
#define WALTHAM_SHM_H_

#include "wth-server-waltham-comm.h"
/**
* wth_server_weston_shm_attach
*
* attach waltham surface data to weston surface
*
* @param names       struct window *window
*                    uint32_t data_sz
*                    void * data
*                    int32_t width
*                    int32_t height
*                    int32_t stride
*                    uint32_t format
* @param value       struct window *window - window info pointer
*                    uint32_t data_sz - buffer data size
*                    void * data - buffer pointer
*                    int32_t width - width
*                    int32_t height -height
*                    int32_t stride -stride information
*                    uint32_t format - buffer data format
* @return            none
*/
void wth_server_weston_shm_attach(struct window *window, uint32_t data_sz, void * data,
       int32_t width, int32_t height, int32_t stride, uint32_t format);

/**
* wth_server_weston_shm_damage
*
* Send weston surface damage
*
* @param names       struct window *window
*
* @param value       struct window *window - window info pointer
*
* @return            none
*/
void wth_server_weston_shm_damage(struct window *window);

/**
* wth_server_weston_shm_commit
*
* Commit weston surface data
*
* @param names       struct window *window
*
* @param value       struct window *window - window info pointer
*
* @return            none
*/
void wth_server_weston_shm_commit(struct window *window);

/**
* wth_server_weston_main
*
* This is the main function which will handle connection to the compositor at server side
*
* @param names        void *data
* @param value        struct window data
* @return             0 on success, -1 on error
*/
int wth_server_weston_main(void *data);


#endif

/**
 * @licence app begin@
 *
 *
 *  TBD
 *
 *
 * @licence end@
 */
/*******************************************************************************
**                                                                            **
**  SRC-MODULE:                                                               **
**                                                                            **
**  TARGET    : linux                                                         **
**                                                                            **
**  PROJECT   : waltham-server                                                **
**                                                                            **
**  AUTHOR    :                                                               **
**                                                                            **
**                                                                            **
**                                                                            **
**  PURPOSE   : This file is acts as interface to weston compositor at server **
**  side                                                                      **
**                                                                            **
**  REMARKS   :                                                               **
**                                                                            **
**  PLATFORM DEPENDANT [yes/no]: yes                                          **
**                                                                            **
**  TO BE CHANGED BY USER [yes/no]: no                                        **
**                                                                            **
*******************************************************************************/

#include <sys/mman.h>
#include <signal.h>
#include <sys/time.h>

#include "wth-server-weston.h"
#include "os-compatibility.h"
#include "ivi-application-client-protocol.h"
#include "bitmap.h"


static int running = 1;
extern bool get_verbosity(void);
extern int verbose;
/*
 * buffer callback
 */
static void
buffer_release(void *data, struct wl_buffer *buffer)
{
    struct shm_buffer *mybuf = data;

    wth_verbose("%s >>> \n",__func__);

    mybuf->busy = 0;

    wth_verbose(" <<< %s \n",__func__);
}

static const struct wl_buffer_listener buffer_listener = {
    buffer_release
};

static int
create_shm_buffer(struct display *display, struct shm_buffer *buffer,
          int width, int height, uint32_t format)
{
    struct wl_shm_pool *pool;
    int fd, size, stride;
    void *data;

    wth_verbose("%s >>> \n",__func__);
    stride = width * 4;
    size = stride * height;

    fd = os_create_anonymous_file(size);
    if (fd < 0) {
        wth_error("creating a buffer file for %d B failed: %m\n",
            size);
        return -1;
    }

    data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (data == MAP_FAILED) {
        wth_error("mmap failed: %m\n");
        close(fd);
        return -1;
    }

    pool = wl_shm_create_pool(display->shm, fd, size);
    buffer->buffer = wl_shm_pool_create_buffer(pool, 0,
                           width, height,
                           stride, format);
    wl_buffer_add_listener(buffer->buffer, &buffer_listener, buffer);
    wl_shm_pool_destroy(pool);
    close(fd);

    buffer->shm_data = data;

    wth_verbose(" <<< %s \n",__func__);
    return 0;
}

/*
 * ivi surface callback
 */
static void
handle_ivi_surface_configure(void *data, struct ivi_surface *ivi_surface,
                 int32_t width, int32_t height)
{
    /* Simple-shm is resizable */
}

static const struct ivi_surface_listener ivi_surface_listener = {
    handle_ivi_surface_configure,
};

static void
create_window(struct window *window, struct display *display, int width, int height)
{
    wth_verbose("%s >>> \n",__func__);

    window->callback = NULL;
    window->display = display;
    window->width = width;
    window->height = height;
    window->surface = wl_compositor_create_surface(display->compositor);
    window->window_frames = 0;
    window->window_benchmark_time = 0;

    wl_display_roundtrip(display->display);
    if (display->ivi_application ) {
        uint32_t id_ivisurf = window->id_ivisurf;
        window->ivi_surface =
            ivi_application_surface_create(display->ivi_application,
                               id_ivisurf, window->surface);
        if (window->ivi_surface == NULL) {
            wth_error("Failed to create ivi_client_surface\n");
            abort();
        }

        ivi_surface_add_listener(window->ivi_surface,
                     &ivi_surface_listener, window);

    } else {
        assert(0);
    }

    wth_verbose(" <<< %s \n",__func__);
    return;
}

static void
destroy_window(struct window *window)
{
    wth_verbose("%s >>> \n",__func__);

    if (window->callback)
        wl_callback_destroy(window->callback);

    if (window->buffers[0].buffer)
        wl_buffer_destroy(window->buffers[0].buffer);
    if (window->buffers[1].buffer)
        wl_buffer_destroy(window->buffers[1].buffer);

    wl_surface_destroy(window->surface);
    free(window);

    wth_verbose(" <<< %s \n",__func__);
}

static struct shm_buffer *
window_next_buffer(struct window *window)
{
    wth_verbose("%s >>> \n",__func__);

    struct shm_buffer *buffer;
    int ret = 0;

    if (!window->buffers[0].busy)
        buffer = &window->buffers[0];
    else if (!window->buffers[1].busy)
        buffer = &window->buffers[1];
    else {
        window->buffers[0].busy = 0;
        window->buffers[0].busy = 1;
        buffer = &window->buffers[0];
    }

    if (!buffer->buffer) {
        ret = create_shm_buffer(window->display, buffer,
                    window->width, window->height,
                    WL_SHM_FORMAT_XRGB8888);

        if (ret < 0)
            return NULL;

        /* paint the padding */
        memset(buffer->shm_data, 0xff,
               window->width * window->height * 4);
    }

    wth_verbose(" <<< %s \n",__func__);
    return buffer;
}

static void
paint_pixels(void *image, int padding, int width, int height, uint32_t time)
{
    wth_verbose("%s >>> \n",__func__);

    const int halfh = padding + (height - padding * 2) / 2;
    const int halfw = padding + (width  - padding * 2) / 2;
    int ir, or;
    uint32_t *pixel = image;
    int y;

    /* squared radii thresholds */
    or = (halfw < halfh ? halfw : halfh) - 8;
    ir = or - 32;
    or *= or;
    ir *= ir;

    pixel += padding * width;
    for (y = padding; y < height - padding; y++) {
        int x;
        int y2 = (y - halfh) * (y - halfh);

        pixel += padding;
        for (x = padding; x < width - padding; x++) {
            uint32_t v;

            /* squared distance from center */
            int r2 = (x - halfw) * (x - halfw) + y2;

            if (r2 < ir)
                v = (r2 / 32 + time / 64) * 0x0080401;
            else if (r2 < or)
                v = (y + time / 32) * 0x0080401;
            else
                v = (x + time / 16) * 0x0080401;
            v &= 0x00ffffff;

            /* cross if compositor uses X from XRGB as alpha */
            if (abs(x - y) > 6 && abs(x + y - height) > 6)
                v |= 0xff000000;

            *pixel++ = v;
        }

        pixel += padding;
    }
    wth_verbose(" <<< %s \n",__func__);
}

void
wth_server_weston_shm_attach(struct window *window, uint32_t data_sz, void * data,
       int32_t width, int32_t height, int32_t stride, uint32_t format)
{
    wth_verbose("%s >>> \n",__func__);

    struct timeval tv;
    uint32_t time;
    struct shm_buffer *buffer;
    static int ss_count = 0;

    gettimeofday(&tv, NULL);
    time = tv.tv_sec * 1000 + tv.tv_usec / 1000;

    buffer = window_next_buffer(window);
    if (!buffer) {
        wth_error("Both buffers busy at redraw().\n");
        return;
    }

    char *bitmap;
    bitmap = getenv("BITMAP");
    if (bitmap) {
        if (!strcmp(bitmap, "1")) {
            int len;
            const char *path = "/tmp/bitmap";
            len = strlen(path);
            char *filename;
            filename = (char *)malloc(len + 10);
            sprintf(filename, "%s%d.bmp", path, ss_count);
            wth_verbose("%s\n", filename);

            save_as_bitmap(filename, data, data_sz, width, height, format);
            ss_count++;
            free(filename);
        }
    }
    buffer->shm_data = data;
    window->width = width;
    window->height = height;
    paint_pixels(buffer->shm_data, 20, window->width, window->height, time);

    wl_surface_attach(window->surface, buffer->buffer, 0, 0);

    buffer->busy = 1;
    wth_verbose(" <<< %s \n",__func__);
}

void
wth_server_weston_shm_damage(struct window *window)
{
    wth_verbose("%s >>> \n",__func__);

    wl_surface_damage(window->surface,
              0, 0, window->width, window->height);

    wth_verbose(" <<< %s \n",__func__);
}

void
wth_server_weston_shm_commit(struct window *window)
{
    wth_verbose("%s >>> \n",__func__);

    static const uint32_t benchmark_interval = 5;
    struct timeval tv;
    uint32_t time;

    gettimeofday(&tv, NULL);
    time = tv.tv_sec * 1000 + tv.tv_usec / 1000;
    if (window->window_frames == 0)
        window->window_benchmark_time = time;
    if (time - window->window_benchmark_time > (benchmark_interval * 1000)) {
        wth_verbose("-------------------------------%d frames in %d seconds: %f fps\n",
        window->window_frames,
        benchmark_interval,
        (float) window->window_frames / benchmark_interval);

        window->window_benchmark_time = time;
        window->window_frames = 0;
    }

    wl_surface_commit(window->surface);

    wl_display_flush(window->display->display);

    if (window->wait)
        window->wait = 0;

    window->window_frames++;
    wth_verbose(" <<< %s \n",__func__);
}

/*
 * frame callback function - this function will display buffer to surface
 */

static const struct wl_callback_listener frame_listener;
static void
redraw(void *data, struct wl_callback *callback, uint32_t time)
{
    wth_verbose("%s >>> \n",__func__);

    struct window *window = data;
    struct shm_buffer *buffer;

    buffer = window_next_buffer(window);
    if (!buffer) {
        if(!callback)
        {
            wth_error("Failed to create the first buffer.\n");
        }
        else
        {
            wth_error("Both buffers busy at redraw(). Server bug?\n");
        }
        abort();
    }

    paint_pixels(buffer->shm_data, 20, window->width, window->height, time);

    wl_surface_attach(window->surface, buffer->buffer, 0, 0);
    wl_surface_damage(window->surface,
              20, 20, window->width - 40, window->height - 40);

    if (callback)
        wl_callback_destroy(callback);

    window->callback = wl_surface_frame(window->surface);
    wl_callback_add_listener(window->callback, &frame_listener, window);
    wl_surface_commit(window->surface);
    buffer->busy = 1;
    wth_verbose(" <<< %s \n",__func__);
}

static const struct wl_callback_listener frame_listener = {
    redraw
};

/*
 * shm callbcak function
 */

static void
shm_format(void *data, struct wl_shm *wl_shm, uint32_t format)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *d = data;

    if (format == WL_SHM_FORMAT_XRGB8888)
        d->has_xrgb = true;

    wth_verbose(" <<< %s \n",__func__);
}

struct wl_shm_listener shm_listener = {
    shm_format
};

/*
 * pointer callbcak functions
 */
static void
pointer_handle_enter(void *data, struct wl_pointer *wl_pointer,
             uint32_t serial, struct wl_surface *wl_surface,
             wl_fixed_t sx, wl_fixed_t sy)
{
    wth_verbose("%s >>> \n",__func__);

    wth_verbose("data [%p]\n", data);

    struct display *display = data;
    struct window *window = display->window;
    struct surface *surface = window->server_surf;
    struct seat *seat = window->server_seat;

    waltham_pointer_enter(window, serial, sx, sy);

    wth_verbose(" <<< %s \n",__func__);
}

static void
pointer_handle_leave(void *data, struct wl_pointer *pointer,
    uint32_t serial, struct wl_surface *surface)
{
    wth_verbose("%s >>> \n",__func__);

    wth_verbose("data [%p]\n", data);

    struct display *display = data;
    struct window *window = display->window;

    waltham_pointer_leave(window, serial);

    wth_verbose(" <<< %s \n",__func__);
}

static void
pointer_handle_motion(void *data, struct wl_pointer *pointer,
    uint32_t time, wl_fixed_t sx, wl_fixed_t sy)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_pointer_motion(window, time, sx, sy);

    wth_verbose(" <<< %s \n",__func__);
}

static void
pointer_handle_button(void *data, struct wl_pointer *wl_pointer,
                      uint32_t serial, uint32_t time, uint32_t button,
                      uint32_t state)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_pointer_button(window, serial, time, button, state);

    wth_verbose(" <<< %s \n",__func__);
}

static void
pointer_handle_axis(void *data, struct wl_pointer *wl_pointer,
            uint32_t time, uint32_t axis, wl_fixed_t value)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_pointer_axis(window, time, axis, value);

    wth_verbose(" <<< %s \n",__func__);
}

static const struct wl_pointer_listener pointer_listener = {
    pointer_handle_enter,
    pointer_handle_leave,
    pointer_handle_motion,
    pointer_handle_button,
    pointer_handle_axis,
};

/*
 * touch callbcak functions
 */

static void
touch_handle_down(void *data, struct wl_touch *touch, uint32_t serial,
          uint32_t time, struct wl_surface *surface, int32_t id,
          wl_fixed_t x_w, wl_fixed_t y_w)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    int x = (int)wl_fixed_to_double(x_w);
    int y = (int)wl_fixed_to_double(y_w);
    wth_verbose("%p x %d y %d\n",window, x, y);

    waltham_touch_down(window, serial, time, id, x_w, y_w);

    wth_verbose(" <<< %s \n",__func__);
}

static void
touch_handle_up(void *data, struct wl_touch *touch, uint32_t serial,
        uint32_t time, int32_t id)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_touch_up(window, serial, time, id);
}

static void
touch_handle_motion(void *data, struct wl_touch *touch, uint32_t time,
            int32_t id, wl_fixed_t x_w, wl_fixed_t y_w)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_touch_motion(window, time, id, x_w, y_w);

    wth_verbose(" <<< %s \n",__func__);
}

static void
touch_handle_frame(void *data, struct wl_touch *touch)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_touch_frame(window);

    wth_verbose(" <<< %s \n",__func__);
}

static void
touch_handle_cancel(void *data, struct wl_touch *touch)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;
    struct window *window = display->window;

    waltham_touch_cancel(window);

    wth_verbose(" <<< %s \n",__func__);
}

static const struct wl_touch_listener touch_listener = {
    touch_handle_down,
    touch_handle_up,
    touch_handle_motion,
    touch_handle_frame,
    touch_handle_cancel
};

/*
 * seat callback
 */
static void
seat_capabilities(void *data, struct wl_seat *wl_seat, enum wl_seat_capability caps)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display = data;

    wth_verbose("caps = %d\n", caps);

    if ((caps & WL_SEAT_CAPABILITY_POINTER) && !display->wl_pointer)
    {
        wth_verbose("WL_SEAT_CAPABILITY_POINTER\n");
        display->wl_pointer = wl_seat_get_pointer(wl_seat);
        wl_pointer_set_user_data(display->wl_pointer, display);
        wl_pointer_add_listener(display->wl_pointer, &pointer_listener, display);
        wl_display_roundtrip(display->display);
    } else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && display->wl_pointer) {
        wth_verbose("!WL_SEAT_CAPABILITY_POINTER\n");
        wl_pointer_destroy(display->wl_pointer);
        display->wl_pointer = NULL;
    }

    if ((caps & WL_SEAT_CAPABILITY_TOUCH) && !display->wl_touch)
    {
        wth_verbose("WL_SEAT_CAPABILITY_TOUCH\n");
        display->wl_touch = wl_seat_get_touch(wl_seat);
        wl_touch_set_user_data(display->wl_touch, display);
        wl_touch_add_listener(display->wl_touch, &touch_listener, display);
        wl_display_roundtrip(display->display);
    } else if (!(caps & WL_SEAT_CAPABILITY_TOUCH) && display->wl_touch) {
        wth_verbose("!WL_SEAT_CAPABILITY_TOUCH\n");
        wl_touch_destroy(display->wl_touch);
        display->wl_touch = NULL;
    }

    wth_verbose(" <<< %s \n",__func__);
}

static const struct wl_seat_listener seat_listener = {
    seat_capabilities,
    NULL
};

static void
add_seat(struct display *display, uint32_t id, uint32_t version)
{
    wth_verbose("%s >>> \n",__func__);

    display->wl_pointer = NULL;
    display->seat = wl_registry_bind(display->registry, id,
                      &wl_seat_interface, 1);
    wl_seat_add_listener(display->seat, &seat_listener, display);
    wth_verbose(" <<< %s \n",__func__);
}

/*
 *  registry callback
 */
static void
registry_handle_global(void *data, struct wl_registry *registry,
               uint32_t id, const char *interface, uint32_t version)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *d = data;

    if (strcmp(interface, "wl_compositor") == 0) {
        d->compositor =
            wl_registry_bind(registry,
                     id, &wl_compositor_interface, 1);
    } else if (strcmp(interface, "wl_shm") == 0) {
        d->shm = wl_registry_bind(registry,
                      id, &wl_shm_interface, 1);
        wl_shm_add_listener(d->shm, &shm_listener, d);
    } else if (strcmp(interface, "ivi_application") == 0) {
        d->ivi_application =
            wl_registry_bind(registry, id,
                     &ivi_application_interface, 1);
    } else if (strcmp(interface, "wl_seat") == 0) {
        add_seat(d, id, version);
    }
    wth_verbose(" <<< %s \n",__func__);
}

static void
registry_handle_global_remove(void *data, struct wl_registry *registry,
                  uint32_t name)
{
}

static const struct wl_registry_listener registry_listener = {
    registry_handle_global,
    registry_handle_global_remove
};

static struct display *
create_display(void)
{
    wth_verbose("%s >>> \n",__func__);

    struct display *display;

    display = malloc(sizeof *display);
    if (display == NULL) {
        wth_error("out of memory\n");
        exit(1);
    }
    display->display = wl_display_connect(NULL);
    assert(display->display);

    display->has_xrgb = false;
    display->registry = wl_display_get_registry(display->display);
    wl_registry_add_listener(display->registry,
                 &registry_listener, display);
    wl_display_roundtrip(display->display);
    if (display->shm == NULL) {
        wth_error("No wl_shm global\n");
        exit(1);
    }

    wl_display_roundtrip(display->display);

    if (!display->has_xrgb) {
        wth_error("WL_SHM_FORMAT_XRGB32 not available\n");
        exit(1);
    }

    wth_verbose(" <<< %s \n",__func__);
    return display;
}

static void
destroy_display(struct display *display)
{
    wth_verbose("%s >>> \n",__func__);

    if (display->shm)
        wl_shm_destroy(display->shm);

    if (display->compositor)
        wl_compositor_destroy(display->compositor);

    wl_registry_destroy(display->registry);
    wl_display_flush(display->display);
    wl_display_disconnect(display->display);
    free(display);

    wth_verbose(" <<< %s \n",__func__);
}

static void
signal_int(int signum)
{
    running = 0;
}

/**
* wth_server_weston_main
*
* This is the main function which will handle connection to the compositor at server side
*
* @param names        void *data
* @param value        struct window data
* @return             0 on success, -1 on error
*/
int
wth_server_weston_main(void *data)
{
    wth_verbose("%s >>> \n",__func__);

    struct window *window = (struct window *) data;
    struct sigaction sigint;
    struct display *display;
    int ret = 0;

    display = create_display();
    create_window(window, display, 400, 400);
    if (!window)
        return -1;
    wl_display_roundtrip(display->display);
    wth_verbose("data %p\n", data);
    wth_verbose("window %p\n", window);

    display->window = window;

    sigint.sa_handler = signal_int;
    sigemptyset(&sigint.sa_mask);
    sigint.sa_flags = SA_RESETHAND;
    sigaction(SIGINT, &sigint, NULL);

    /* Initialise damage to full surface, so the padding gets painted */
    wl_surface_damage(window->surface, 0, 0,
              window->width, window->height);

    redraw(window, NULL, 0);

    window->wait = 1;
    window->ready = true;
    while (window->wait)
    {
        ret = wl_display_dispatch(display->display);
        usleep(1);
    }
    wth_verbose("debug\n");
    while (running && ret != -1)
        ret = wl_display_dispatch(display->display);

    wth_verbose("wth_server_weston_main exiting\n");

    if (window->display->ivi_application) {
        ivi_surface_destroy(window->ivi_surface);
        ivi_application_destroy(window->display->ivi_application);
    }

    destroy_window(window);
    destroy_display(display);

    wth_verbose(" <<< %s \n",__func__);
    return 0;
}

Component Name:
============
waltham-server

Description
============
waltham-server component is a server side implementation for using
Waltham IPC library to obtain and process remote output received from
waltham-transmitter

This component is designed to be used for evaluating the functionalities of
waltham-transmitter plugin.

This component also acts as weston client application to display/handle various
requests from actual weston client at transmitter side.

Build Steps
===========
Currently built for HOST environment

1. Prerequisite before building
weston, wayland and waltham should be built and available in HOST machine
Refer below link to build on Ubuntu HOST machie
https://wayland.freedesktop.org/ubuntu16.04.html

2. In waltham-server directory, create build directory
# cd waltham-server
# mkdir build

3. Run cmake
# cmake ..

4. waltham-server binary should be availaible in build directory

Features supported
==================
1. Connection establishment with waltham-transmitter
2. Create surface sent by waltham-transmitter
3. Handle pointer events from weston at server side
4. Transmit pointer events from weston at server side to transmitter

Basic test steps
=================
1. Start weston at server side
# weston

2. Run waltham-server
#waltham-server -p <port_number> -v

3. Start weston with transmitter plugin at transmitter side
#weston

###############Connection established -Server side logs ################
set_sigint_handler >>>
 <<< set_sigint_handler
server_listen >>>
 <<< server_listen
watch_ctl >>>
 <<< watch_ctl
Waltham server listening on TCP port 34400...
server_mainloop >>>
server_flush_clients >>>
 <<< server_flush_clients
listen_socket_handle_data >>>
EPOLLIN evnet received.
server_accept_client >>>
client_create >>>
watch_ctl >>>
Client 0xb6c20930 connected.


Limitation/Workarounds if any
==============================
1. Currently server does not display framed data received from the transmitter,
instead frame data created in local buffer is used.
2. Pointer events are also tested with local buffer data.
